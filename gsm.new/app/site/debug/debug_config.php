<?

require_once "{$_SERVER['DOCUMENT_ROOT']}/debug/ErrorHook/MailNotifier.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/debug/ErrorHook/Listener.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/debug/ErrorHook/RemoveDupsWrapper.php";

if (!defined("PATH_SEPARATOR"))
    define("PATH_SEPARATOR", getenv("COMSPEC") ? ";" : ":");
ini_set("include_path", ini_get("include_path") . PATH_SEPARATOR . dirname(__FILE__));

$emails = array(
    'ivan@metadesign.ru',
    'misha@metadesign.ru'
);

if ($emails) {
    $errorsToMail = new Debug_ErrorHook_Listener();
    $errorsToMail->addNotifier(
        new Debug_ErrorHook_RemoveDupsWrapper(
            new Debug_ErrorHook_MailNotifier(
                implode(',', $emails),
                Debug_ErrorHook_TextNotifier::LOG_ALL
            ),
            "/tmp/errors", // lock directory
            60            // do not resend the same error within 60 seconds
        )
    );
}
?>