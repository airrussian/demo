<?php

$controlfile = $_SERVER['DOCUMENT_ROOT'] . "/sitemap-{$_SERVER['HTTP_HOST']}.xml";
if (file_exists($controlfile)) {
    header('Content-type: text/xml');
    echo file_get_contents($controlfile);
} else {
    header("Location: /404/"); exit();
}

