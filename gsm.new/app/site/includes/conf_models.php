<?php

/**
 * Список ORM моделей используемых на сайте
 */
$model_names = array(
    'Module',
    'Page',
    'PageType',
    'TextBlock',
    'User',
    'TempImage',
    'TempFile',
    'BuilderSetting',
    'FormsHandlerLogItem',
    'FeedbackTempFile',
    'FeedbackFile',  
    //----------------------------
    'NewsItem',
    'Certificate',
    
    array('ContactCity', 'ContactCities'),
    array('ContactStorage', 'ContactStoragies'),
    array('ContactEmployee', 'ContactEmployees'),
    
    'PriceList',    
    
    //'CatalogCategoryTemp',
    array('CatalogCategory', 'CatalogCategories'),
    array('Catalog2Category', 'Catalog2Categories'),
    array('CatalogEntry', 'CatalogEntries'),
    'CatalogGroup',
    'CatalogManuf',
    'CatalogModel',
    'CatalogPhoto',
    'CatalogBrand',
    'CatalogEntryGroupLink',
    'CatalogIncome',
    
    'CatalogEntryDatetimeValue',
    'CatalogEntryFloatValue',
    'CatalogEntryIntegerValue',    
    'CatalogEntryPriceValue',
    'CatalogEntryStringValue',
    'CatalogEntryTextValue',
    'CatalogEntryBooleanValue',     
    'CatalogField',
    'CatalogFieldSet',
    'CatalogFieldSetField',
    'CatalogFieldType',        
    'CatalogEntryManualRating',
    'CatalogEntryMem',
    'CatalogSeriesItem',
    
    'SiteUser',
    'SiteUserCart',
    'SiteUserPending',
    
    'SiteOrder',
    'SiteOrderItem',
    array('SiteOrderStatus', 'SiteOrderStatuses'),
    
    'Banner',
    'BannerPlace',
    'BannerPlaceBanner',
    
    'Preorder',    
    'Onorder',
    'OrdersEmail',
    
    //array('SCity', 'SCities'),
    'SCityLink',
    
    
    'Script',
    'ScriptLog',
    'ScriptLogMessage',
    
    'SiteOrderDeliveryMethod',
    'SiteOrderDeliveryPayMethod',
    
    'Test',

    'MessageQueueItem'
    
);
