<?php

class CatalogIncome extends NamiModel {

    static function definition() {
        return array(
            'entry' => new NamiFkDbField(array('model' => 'CatalogEntry', 'related' => 'incomes')),            
            'oneC_id' => new NamiCharDbField(array('maxlength' => 255, 'index' => true)),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M', 'index' => true)),
            'date_day' => new NamiDateTimeDbField(array('index' => true)),
            'city' => new NamiCharDbField(array('maxlength' => 255, 'index' => true)),         
          //  'qty' => new NamiIntegerDbField(),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }
    
    /*
     * Возвращает поступления товаров сгруппированых по категориями второго уровня
     */    
    static function getCategoryDate($date) {
        $city_prefix = ContactCity::GetCurrent()->prefix;                

        $entries_id = CatalogIncomes()->filter(['enabled' => true, 'date_day' => strtotime($date), 'city' => $city_prefix])->values("entry");

        $items = [];

        foreach ($entries_id as $entry_id) {
            $entry = CatalogEntries()->get(['id' => $entry_id]);
            if ($entry) {
                $category = ($entry->category->lvl > 3) ?
                        $category = CatalogCategories()->embraceParents($entry->category)->filterLevel(3)->first() :
                        $category = $entry->category;

                $items[$category->id]['entry'][] = $entry;
                $items[$category->id]['category'] = $category;
                $items[$category->id]['parent'] = CatalogCategories()->embraceParents($category)->filterLevel(2)->first();
            }
        }
        
        uasort($items, function($a, $b) {
            return ($a['category']->title < $b['category']->title) ? -1 : 1;
        });                
        
        return $items;
    }
    
    static function nextDate($date) {
        $city_prefix = ContactCity::GetCurrent()->prefix;
        return date("Y-m-d", CatalogIncomes()
                ->filter(['enabled' => true, 'date_day__lt' => strtotime($date), 'city' => $city_prefix])
                ->orderDesc("date_day")
                ->first()
                ->date_day->timestamp);
    }       
       
}
