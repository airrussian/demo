<?php

/**
 * Новости 
 */
class NewsItem extends NamiModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250, 'null' => false)),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y', 'index' => true, 'null' => false)),
            'announce' => new NamiTextDbField(),
            'text' => new NamiTextDbField(),
            'preview' => new NamiImageDbField(array(
                'path' => "/static/uploaded/images/article",
                'variants' => array(
                    'cms' => array('width' => 260, 'height' => 140, 'crop' => true),
                    'small' => array('width' => 200, 'height' => 130, 'crop' => true),
                ))
            ),
            'images' => new NamiArrayDbField(array(
                'type' => 'NamiImageDbField',
                'path' => '/static/uploaded/images/article',
                'variants' => array(
                    'cms' => array('width' => 120, 'height' => 120, 'crop' => true),
                    'small' => array('width' => 266, 'height' => 180, 'crop' => true),
                    'big' => array('width' => 1064, 'height' => 720),
                ))
            ),
            'oneC_id' => new NamiCharDbField(array('maxlength' => '300')),
            'send' => new NamiBoolDbField(array('default' => false)),
            'date_send' => new NamiDatetimeDbField(array('format' => '%d.%m.%Y %H:%M:%S', 'index' => true)),
            'city' => new NamiCharDbField(array('maxlength' => '300')),
            'favorite' => new NamiBoolDbField(array('default' => false, 'index' => true)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
            'uri' => new NamiCharDbField(array('maxlength' => 255, 'index' => true)),
            'meta_title' => new NamiCharDbField(array('maxlength' => 1000)),
            'meta_keywords' => new NamiTextDbField(),
            'meta_description' => new NamiTextDbField(),
        );
    }

    public $description = array(
        'title' => array('title' => 'Заголовок'),
        'date' => array('title' => 'Дата публикации'),
        'announce' => array('title' => 'Краткое описание'),
        'text' => array('title' => 'Описание', 'widget' => 'richtext'),
        'preview' => array('title' => 'Превью для статьи', 'info' => 'Рекомендуемый размер 200x130'),
        'images' => array('title' => 'Блок фотографий', 'widget' => 'images', 'info' => 'Рекомендуемый размер 1064x720'),
        'favorite' => array('title' => 'Важная новость', 'info' => 'Выводитьcя на главной'),
        'uri' => array('title' => 'Псевдоним страницы', 'info' => 'Можно не задавать, создается автоматически'),
        'city' => array('title' => 'Привязка к городу', 'widget' => 'chosen', 'choices' => 'ContactCity'),
        'meta' => array('title' => 'Ключевые слова, описание и заголовок (SEO)', 'widget' => 'seo_fields')
    );

    function afterSave($is_new) {
        
        if (!Meta::isPathName($this->uri)) {
            if ($this->uri)
                $this->uri = Meta::getPathName($this->uri);

            if (!Meta::isPathName($this->uri))
                $this->uri = Meta::getPathName($this->title);

            $this->hiddenSave();
        }
        
        $main_model_data = array(
            "id" => $this->id,
            "related_ids_new_value" => $this->city,
        );

        $link_model_data = array(
            "name" => "SCityLink",
            "main_model_field_name" => "news",
            "related_model_field_name" => "city",
        );

        LinksProcessor::check_links($main_model_data, $link_model_data);
    }

    function __full_uri() {
        return Builder::getAppUri("NewsApplication") . $this->uri . "/";
    }

    function beforeDelete() {
        $remove_params = array(
            "links_model_name" => "SCityLink",
            "link_field_name" => "news",
            "link_field_value" => $this->id,
        );

        LinksProcessor::remove_links($remove_params);
    }

}
