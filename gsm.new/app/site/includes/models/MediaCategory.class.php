<?

//категория фоточек и видосиков
class MediaCategory extends NamiSortableModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250, 'null' => false)),
            'name' => new NamiCharDbField(array('maxlength' => 250)),
            'enabled' => new NamiBoolDbField(array('default' => false, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название'),
        'name' => array('title' => 'Название для url'),
    );

    function __full_uri() {
        return Builder::getAppUri("MediaApplication") . $this->name . "/";
    }

    function beforeSave() {
        if (!Meta::isPathName($this->name)) {
            if ($this->name) {
                $this->name = Meta::getPathName($this->name);
            }

            if (!Meta::isPathName($this->name)) {
                $this->name = Meta::getPathName($this->title);
            }
        }
    }

    function afterSave($new) {
        if ($this->isDirty('name')) {
            if (MediaCategories()->filter(array('id__ne' => $this->id, 'name' => $this->name))->count() > 0) {
                $this->name .= $this->id;
                $this->hiddenSave();
            }
        }
    }

    function beforeDelete() {
        $has_items = MediaAlbums()->get(array("category" => $this->id));
        if ($has_items) {
            throw new Exception("Раздел нельзя удалить, т.к. в нем присутствуют альбомы");
        }
    }

}
