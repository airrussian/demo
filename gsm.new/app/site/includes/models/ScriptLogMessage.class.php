<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SiteLogMessage
 *
 * @author air
 */
class ScriptLogMessage extends NamiModel {

    static function definition() {
        return array(
            'log' => new NamiFkDbField(array('model' => 'ScriptLog')),
            'datetime' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M:%S', 'index' => true)),
            'message' => new NamiTextDbField(),
            'type' => new NamiCharDbField(array("maxlength" => "50", 'index' => true))
        );
    }

}
