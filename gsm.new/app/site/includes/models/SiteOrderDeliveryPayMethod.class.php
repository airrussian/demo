<?php

/**
 * Способы оплаты заказа, зависит от способа достаки
 */
class SiteOrderDeliveryPayMethod extends NamiSortableModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 255)),
            'name' => new NamiCharDbField(array('maxlength' => 255)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true))
        );
    }

    public $description = array(        
        'title' => array('title' => 'Способ оплаты'),
        'name' => array('title' => 'Название'),
    );
    
    public function __short_title() {
        return trim(preg_replace("~<span.+?span>~si", "", $this->title));
    }

}
