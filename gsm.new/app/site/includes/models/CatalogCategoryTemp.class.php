<?php

class CatalogCategoryTemp extends NamiNestedSetModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 255, 'default' => 'Новый раздел', 'index' => true)),
            'name' => new NamiCharDbField(array('localized' => false, 'maxlength' => 255, 'index' => true)),
            'uri' => new NamiCharDbField(array('localized' => false, 'maxlength' => 300, 'index' => true)),
            //'text' => new NamiTextDbField(),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
            'fieldset' => new NamiFkDbField(array('model' => 'CatalogFieldSet', 'related' => 'Catalog_categories', 'null' => true)),
            'oneC_id' => new NamiCharDbField(array('maxlength' => 255, 'localized' => false, 'index' => true)), # артикул
            'oneC_parent_id' => new NamiCharDbField(array('maxlength' => 255, 'localized' => false, 'index' => true)), # артикул
        );
    }

    public $description = array(
        'title' => array('title' => 'Название'),
        'name' => array('title' => 'Название на англ.'),
        //'text' => array('title' => 'Описание категории', 'widget' => 'richtext'),
        'fieldset' => array('title' => 'Набор полей', 'widget' => 'select', 'choices' => 'CatalogFieldSet', 'info' => 'Если не указано, набор будет взят из родительской(их) категории, если он там есть'),
    );

    function __full_uri() {
        return preg_replace("~/+~", "/", Builder::getAppUri('CatalogApplication') . $this->uri);
    }

    function beforeSave() {
        // Приводим в порядок name страницы
        if (!Meta::isPathName($this->name)) {
            // Попробуем сначала довести до ума исходный name
            if ($this->name) {
                $this->name = Meta::getPathName($this->name);
            }

            // Не получилось — сделаем на основе title
            if (!Meta::isPathName($this->name)) {
                $this->name = Meta::getPathName($this->title);
            }
        }
    }

    function afterSave($is_new) {
        //проверка на дубликаты среди соседей
        if ($this->isDirty('name', 'lvl') || $is_new) {
            if (CatalogCategories()->filterSiblings($this)->filter(array('id__ne' => $this->id, 'name' => $this->name))->count() > 0) {
                $this->name .= $this->id;
                $this->hiddenSave();
            }

            // Генерируем полный uri страницы
            if ($this->lvl > 1) {
                $pages_names = CatalogCategories()
                        ->filterParents($this)
                        ->embrace($this)
                        ->filterLevel(2, 0)
                        ->treeOrder()
                        ->values("name");

                $uri = "/" . join('/', $pages_names);
            } else {
                $uri = "/";
            }

            if ($this->uri != $uri) {
                $this->uri = $uri;
                $this->hiddenSave();
            }

            //проверяем дочерние элементы
            $sub_pages = CatalogCategories()->filterChildren($this)->filterLevel($this->lvl + 1)->all();
            if ($sub_pages) {
                foreach ($sub_pages as $sub_page) {
                    $sub_page->markDirty("name");
                    $sub_page->save();
                }
            }
        }
    }

    function beforeDelete() {
        $this_entries = CatalogEntries(array("category" => $this->id))->first();
        if ($this_entries) {
            throw new Exception("Невозможно удалить раздел «{$this->title}» ({$this->oneC_id}), в нем есть записи.");
        }
    }

}
