<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ScriptLog
 *
 * @author air
 */
class ScriptLog extends NamiModel {

    static function definition() {
        return array(
            'script' => new NamiFkDbField(array('model' => 'Script')),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M', 'index' => true)),
            'success' => new NamiBoolDbField(array("default" => 0)),
            'errors' => new NamiIntegerDbField(array("default" => 0)),
            'insert' => new NamiIntegerDbField(array("default" => 0)),
            'update' => new NamiIntegerDbField(array("default" => 0)),
            'delete' => new NamiIntegerDbField(array("default" => 0)),
        );
    }
    
    public function beforeDelete() {
        $messages = ScriptLogMessages()->filter(array("log" => $this->id))->all();
        foreach ($messages as $message) {
            $message->delete();
        }
    }

}
