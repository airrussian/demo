<?php

class CatalogSinglton {

    static private $instance;
    private $categoryTree;

    private function __construct() {
        // Выигрыш на 700 категориях, хранения в файле x2 (~0.2 из базы, и ~0.1 из файла)
        //$filename = $_SERVER['DOCUMENT_ROOT'] . '/static/cache/category.tree';
        /*if (file_exists($filename)) {
            $this->categoryTree = unserialize(file_get_contents($filename));
        } else { */
            $this->categoryTree = CatalogCategories()->treeOrder()->tree();
            /* file_put_contents($filename, serialize($this->categoryTree));
        }*/
    }

    public function category_filterLever($start, $finish, $categories = null) {
        $result = array();

        if (is_null($categories)) {
            $categories = $this->categoryTree;
            $result = $this->category_filterLever($start, $finish, $categories[0]->getChildren());
        } else {
            foreach ($categories as $category) {
                if (($category->lvl >= $start) && ($category->lvl <= $finish) && ($category->enabled == true)) {
                    $result[] = $category;
                    $result = array_merge($result, $this->category_filterLever($start, $finish, $category->getChildren()));
                }
            }
        }
        return $result;
    }

    public function category_tree() {
        return $this->categoryTree;
    }

    private function __clone() {
        
    }

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new CatalogSinglton();
        }
        return self::$instance;
    }

    public function category_getids() {
        $result = array();
        $cats = $this->category_filterLever(1, 10);
        foreach ($cats as $cat) {
            $result[] = $cat->id;
        }
        return $result;
    }


    public function category_parents($category, $categories = null) {
        $result = array();

        if (is_null($categories)) {
            $categories = $this->categoryTree[0]->getChildren();
        }
        foreach ($categories as $cat) {
            $r = $this->category_parents($category, $cat->getChildren());
            if (!empty($r)) {
                $result = array_merge(array($cat), $r);
                break;
            }
            if ($cat->id == $category->id) {
                $result[] = $category;
                break;
            }
        }
        return $result;
    }

}
