<?php

class PriceList extends NamiModel {
    
    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250)),                        
            'city' => new NamiFkDbField(array('model' => 'ContactCity')),
            'link' => new NamiCharDbField(array('maxlength' => 255)),
            'code' => new NamiTextDbField(),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название'),      
        'city' => array('title' => 'Город', 'widget' => 'select', 'choices' => 'ContactCity'),
        'link' => array('title' => 'Ссылка на прайс лист'),        
        'code' => array('title' => 'Код формы для рассылки'),                
    );
    
}