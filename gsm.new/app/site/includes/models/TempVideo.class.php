<?

class TempVideo extends NamiModel {

    static function definition() {
        return array(
            'video' => new NamiFlvDbField(array(
                'path' => '/static/uploaded/temp/movies',
                'width' => 580,
                'height' => 435,
                'force_convert' => false,
                )),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y', 'index' => true)),
        );
    }

}
