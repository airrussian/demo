<?php

class TownItem extends NamiModel
{
    static function definition()
    {
        return [
            'title'     => new NamiCharDbField(['maxlength' => 255]),
            'slug1C'    => new NamiCharDbField(['maxlength' => 10, 'index' => true]),
            'code'      => new NamiCharDbField(['maxlength' => 10, 'index' => true])
        ];
    }
}