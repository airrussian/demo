<?

/**
 *   Голосование
 */
class Poll extends NamiModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 500)),
            'enabled' => new NamiBoolDbField(array('default' => false)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название'),
    );

    function beforeDelete() {
        $this->votes->delete();
        $this->answers->delete();
    }

    static function get_poll() {
        $is_active_poll = true;
        $answers = null;

        $poll_filter = array(
            'enabled' => true,
        );

        //проголосованное сохраняется в куку
        //исключаем голосования которые уже были
        if (isset($_COOKIE['builder_polls'])) {
            $ids = explode(",", $_COOKIE['builder_polls']);
            if ($ids) {
                $poll_filter["id__notin"] = $ids;
            }
        }

        $poll = Polls()
            ->filter($poll_filter)
            ->orderRand()
            ->first();

        //если пользователь проголосовал за все
        // то прото показываем резльтат рандомного голосования
        if (!$poll && isset($poll_filter['id__notin'])) {
            unset($poll_filter['id__notin']);

            $poll = Polls()
                ->filter($poll_filter)
                ->orderRand()
                ->first();

            $is_active_poll = false;
        }

        if ($poll) {
            $answers = PollAnswers()
                ->filter(array(
                    "enabled" => true,
                    "poll" => $poll
                ))
                ->sortedOrder()
                ->all();
        }

        return array(
            "is_active_poll" => $is_active_poll,
            "poll" => $poll,
            "answers" => $answers
        );
    }

}
