<?

/**
  Страница сайта
 */
class Page extends NamiNestedSetModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 1000, 'default' => 'Новая страница')),
            'name' => new NamiCharDbField(array('maxlength' => 50, 'index' => true)),
            'uri' => new NamiCharDbField(array('maxlength' => 300, 'index' => true)),
            'text' => new NamiTextDbField(),
            'text_krsk' => new NamiTextDbField(),
            'text_irk' => new NamiTextDbField(),
            'text_omsk' => new NamiTextDbField(),
            'text_vlad' => new NamiTextDbField(),
            'text_ufa' => new NamiTextDbField(),
            'text_abakan' => new NamiTextDbField(),
            'text_ulanude' => new NamiTextDbField(),
            'text_mow' => new NamiTextDbField(),
	    'text_kzn' => new NamiTextDbField(),
            'type' => new NamiFkDbField(array('model' => 'PageType', 'null' => false, 'index' => true, 'default' => 1,)),
            'menu' => new NamiBoolDbField(array('default' => 1, 'index' => 'nav')),
            'enabled' => new NamiBoolDbField(array('default' => 0, 'index' => 'nav')),
            'meta_title' => new NamiCharDbField(array('maxlength' => 1000)),
            'meta_keywords' => new NamiTextDbField(),
            'meta_description' => new NamiTextDbField(),
            'meta_h1' => new NamiCharDbField(array('maxlength' => 1000)),
            //конторлы для админа
            'hide_drag_interface' => new NamiBoolDbField(array('index' => true, 'default' => false)),
            'hide_edit_interface' => new NamiBoolDbField(array('index' => true, 'default' => false)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название страницы'),
        'name' => array('title' => 'Название на англ.'),
        'cities_ids' => array('title' => 'Город', 'widget' => 'select', 'choices' => 'ContactCity', 'info' => 'Выбрав город, можно изменить текст страницы, который будет отображаться только для выбранного города'),
        'text' => array('title' => 'Текст страницы, общий', 'widget' => 'richtext', 'info' => 'Отображается на страницах городов, для которых нет текста'),
        'text_krsk' => array('title' => 'Текст страницы для города Красноярска', 'widget' => 'richtext'),
        'text_irk' => array('title' => 'Текст страницы для города Иркутска', 'widget' => 'richtext'),
        'text_omsk' => array('title' => 'Текст страницы для города Омска', 'widget' => 'richtext'),
        'text_vlad' => array('title' => 'Текст страницы для города Владивостока', 'widget' => 'richtext'),
        'text_ufa' => array('title' => 'Текст страницы для города Уфа', 'widget' => 'richtext'),
        'text_abakan' => array('title' => 'Текст страницы для города Абакан', 'widget' => 'richtext'),
        'text_ulanude' => array('title' => 'Текст страницы для города Улан-Удэ', 'widget' => 'richtext'),
        'text_mow' => array('title' => 'Текст страницы для города Москва', 'widget' => 'richtext'),
	'text_kzn' => array('title' => 'Текст страницы для города Казань', 'widget' => 'richtext'),
        'meta' => array('title' => 'Ключевые слова, описание и заголовок (SEO)', 'widget' => 'seo_fields'),
    );

    function beforeSave() {
        // Имя главной страницы - /
        if ($this->lvl == 1) {
            $this->name = '/';
            return true;
        }

        // Приводим в порядок name страницы
        if (!Meta::isPathName($this->name)) {
            // Попробуем сначала довести до ума исходный name
            if ($this->name) {
                $this->name = Meta::getPathName($this->name);
            }

            // Не получилось — сделаем на основе title
            if (!Meta::isPathName($this->name)) {
                $this->name = Meta::getPathName($this->title);
            }
        }
    }

    function afterSave($is_new) {
        //проверка на дубликаты среди соседей
        if ($this->isDirty('name', 'lvl') || $is_new) {
            if (Pages()->filterSiblings($this)->filter(array('id__ne' => $this->id, 'name' => $this->name))->count() > 0) {
                $this->name .= $this->id;
                $this->hiddenSave();
            }

            // Генерируем полный uri страницы
            if ($this->lvl > 1) {
                $pages_names = Pages()
                        ->filterParents($this)
                        ->embrace($this)
                        ->filterLevel(2, 0)
                        ->treeOrder()
                        ->values("name");

                $uri = '/' . join('/', $pages_names) . "/";
            } else {
                $uri = "/";
            }

            if ($this->uri != $uri) {
                $this->uri = $uri;
                $this->hiddenSave();
            }

            //проверяем дочерние элементы
            $sub_pages = Pages()->filterChildren($this)->filterLevel($this->lvl + 1)->all();
            if ($sub_pages) {
                foreach ($sub_pages as $sub_page) {
                    $sub_page->markDirty("name");
                    $sub_page->save();
                }
            }
        }
    }

    static public function replace(Page $page) {

        $city = ContactCity::GetCurrent()->title_where;
        $page->meta_title = str_replace("{{city}}", $city, $page->meta_title);
        $page->meta_keywords = str_replace("{{city}}", $city, $page->meta_keywords);
        $page->meta_description = str_replace("{{city}}", $city, $page->meta_description);
        $page->meta_h1 = str_replace("{{city}}", $city, $page->meta_h1);

        return $page;
    }

}
