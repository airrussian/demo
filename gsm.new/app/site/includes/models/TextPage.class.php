<?php

/**
  Текстовые страницы, привязаны по городам
 */
class TextPage extends NamiModel {

    static private $preview_length = 100;

    static function definition() {
        return array(
            'city' => new NamiCharDbField(),
            'name' => new NamiCharDbField(array('maxlength' => 255, 'null' => false, 'index' => true)),
            'text' => new NamiTextDbField(),
            'preview' => new NamiCharDbField(array('maxlength' => self::$preview_length)),
            'enabled' => new NamiBoolDbField(array('default' => 1, 'index' => true)),
            'rich' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название страницы'),
        'name' => array('title' => 'Название на англ.'),
        'text' => array('title' => 'Текст страницы', 'widget' => 'richtext'),
        'meta' => array('title' => 'Ключевые слова, описание и заголовок (SEO)', 'widget' => 'seo_fields'),
        'cities_ids' => array('title' => 'Привязка к городу', 'widget' => 'chosen', 'choices' => 'ContactCity'),
    );

}
