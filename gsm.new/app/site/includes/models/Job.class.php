<?

class Job extends NamiSortableModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 200)),
            'employee' => new NamiFkDbField(array('model' => 'Employee')),
            'department' => new NamiFkDbField(array('model' => 'Department')),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'employee' => array('title' => 'Сотрудник', 'widget' => 'select', 'choices' => 'Employee'),
    );

    function beforeSave() {
        $this->title = $this->employee->name . " - " . $this->employee->post;
    }

}
