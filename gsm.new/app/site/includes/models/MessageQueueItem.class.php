<?php

class MessageQueueItem extends NamiModel
{
    static function definition()
    {
        return [
            'title'         => new NamiCharDbField(['maxlength' => 255]),
            'body'          => new NamiTextDbField(),
            'addresses'     => new NamiTextDbField(),
            'fromSend'      => new NamiTextDbField(),
            'dateCreate'    => new NamiDatetimeDbField(['default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M', 'index' => true]),
            'isProccessing' => new NamiBoolDbField(['default' => false, 'index' => true]),
            'isError'       => new NamiBoolDbField(['default' => false, 'index' => true]),
            'dateSend'      => new NamiDatetimeDbField(['default' => null, 'format' => '%d.%m.%Y %H:%M', 'index' => true]),
            'error'         => new NamiTextDbField(),
        ];
    }

}