<?

class SliderItem extends NamiSortableModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 200)),
            'url' => new NamiCharDbField(array('maxlength' => 200)),
            'image' => new NamiImageDbField(array('path' => "/static/uploaded/images/slider",
                'variants' => array(
//                    'slide' => array('width' => 700, 'height' => 360, 'crop' => true, 'jpegquality' => 100, 'pngcompression' => 0),
                    'cms' => array('width' => 120, 'height' => 120, 'crop' => true),
                ))),
            'text' => new NamiTextDbField(),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
//            'category' => new NamiFkDbField(array('model' => 'SliderCategory', 'related' => 'slider_items', 'null' => false)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Заголовок'),
        'url' => array('title' => 'Ссылка'),
        'image' => array('title' => 'Изображение', 'info' => 'рекомендуемый размер 100x100 пикс.'),
        'text' => array('title' => 'Описание')
    );

}
