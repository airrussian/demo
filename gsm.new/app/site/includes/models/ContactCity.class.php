<?php

class ContactCity extends NamiSortableModel {

    static $socials_name = array(
        'vk' => 'VKontakte',
        'in' => 'Instagram',
        'fb' => 'Facebook',
        'yo' => 'Youtube'
    );

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250)),
            'title_where' => new NamiCharDbField(['maxlength' => 255]),
            'domain' => new NamiCharDbField(array('maxlength' => 250, 'index' => true)),
            'prefix' => new NamiCharDbField(array('maxlength' => 250, 'index' => true)),
            'header' => new NamiCharDbField(array('maxlength' => 255)),
            'requisites' => new NamiTextDbField(),
            'text' => new NamiTextDbField(),
            'footertext' => new NamiTextDbField(),
            'socials' => new NamiTextDbField(),
            'widgetVK' => new NamiTextDbField(),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
            'meta_title' => new NamiCharDbField(array('maxlength' => 1000)),
            'meta_keywords' => new NamiTextDbField(),
            'meta_description' => new NamiTextDbField(),
            'feedbackemail' => new NamiCharDbField(array('maxlength' => 255)),
	        'apikey' => new NamiCharDbField(array('maxlength' => 255)),
            'feedback_sendToEmail_predlojenie' => new NamiCharDbField(['maxlength' => 255]),
            'feedback_sendToEmail_otzyv' => new NamiCharDbField(['maxlength' => 255]),
            'order_sendToEmail' => new NamiCharDbField(['maxlength' => 255]),
            'order_sendFromEmail' => new NamiCharDbField(['maxlength' => 255]),
            'order_sendFromEmailPassword' => new NamiCharDbField(['maxlength' => 255])
        );
    }

    public $description = array(
        'title' => array('title' => 'Город'),
        'title_where' => ['title' => 'Название города в родительном падеже'],
        'domain' => array('title' => 'Домен'),
        'prefix' => array('title' => 'Префикс'),
        'header' => array('title' => 'Заголовок'),
        'requisites' => array('title' => 'Реквизиты', 'widget' => 'richtext'),
        'text' => array('title' => 'текст, описание', 'widget' => 'richtext'),
        'socials' => array('title' => 'Ссылки на соц.сети'),
        'footertext' => ['title' => 'Текст в подвале сайта'],
//        'widgetVK' => array('title' => 'Код виджета VK', 'info' => 'Редактирование данного блока на свой страх и риск'),
        'meta' => array('title' => 'Ключевые слова, описание и заголовок (SEO)', 'widget' => 'seo_fields'),
//        'feedbackemail' => array('title' => 'Email адреса для формы "обратная связь"'),
	    'apikey' => array('title' => 'Ключ для карт'),
        'feedback_sendToEmail_predlojenie' => ['title' => 'Email для получения вопросов по товару и доставке', 'info' => 'Для указания нескольких адресов в качестве разделителя используйте запятую'],
        'feedback_sendToEmail_otzyv' => ['title' => 'Email для получения отзывов и предложений', 'info' => 'Для указания нескольких адресов в качестве разделителя используйте запятую'],
        'order_sendToEmail' => ['title' => 'Email для получения оповещения о заказа', 'info' => 'Для указания нескольких адресов в качестве разделителя используйте запятую'],
        'order_sendFromEmail' => ['title' => 'Email с которого происходит отправка оповещений о заказе пользователю', 'info' => 'Email отправки может быть только один'],
        'order_sendFromEmailPassword' => ['title' => 'Пароль от Email с которого происходит отправка оповещения о заказе', 'info' => 'Необходим для успешной авторизации']
    );

    public function __order_sendFromEmailArray() {
        return [
            'name' => null,
            'email' => $this->order_sendFromEmail,
            'password' => $this->order_sendFromEmailPassword
        ];
    }

    /**
     * Возвращает названия город которые указаны в $ids
     * @param type $ids
     * @return type
     */
    static public function getNameCities($ids) {
        $array = array();

        $ids = json_decode($ids);

        $items = ContactCities()->filter(array('id__in' => $ids))->all();
        foreach ($items as $item) {
            $array[] = $item->title;
        }

        return $array;
    }

    static function GetNameByIds($ids) {
        return ContactCities()->filter(array('id__in' => $ids))->values('title');
    }

    static function GetCurrent() {
        return ContactCities()->get(array('domain' => self::GetDomain()));
    }
    
    static function getCurrentCity() {
        self::GetCurrent();
    }

    static function GetDomain() {
        list($dn1, $dn2) = array_reverse(explode(".", $_SERVER['HTTP_HOST']));
        return "$dn2.$dn1";
    }

    static function GetNameByPrefix($prefix) {
        if (!$prefix) $prefix = 'krsk';
        return self::GetByPrefix($prefix)->title;
    }

    static function GetAll($sorted = false) {
        $result = ContactCities(['enabled' => true]);
        
        switch ($sorted) {
            case 'title':
                $result = $result->order("title");
                break;
            case 'order': 
                $result = $result->sortedOrder();
                break;
        }
        
        $result = $result->all();
        return $result;
    }

    static function GetByPrefix($prefix) {
        $prefix = mb_strtolower($prefix) ?? 'krsk';
        return ContactCities()->get(array('prefix' => $prefix));
    }
    
    static function GetByName($name) {
        return ContactCities()->get(array('title' => $name));
    }
    
    public function __a_city() {
        return "a_" . ($this->prefix ?? 'krsk');
    }
    
//    public function __title_where() {
//        return $this->title_where;
//    }

}
