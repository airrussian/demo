<?php

class Certificate extends NamiSortableModel {
    
    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 200, 'default' => 'Новый раздел', 'null' => false)),
            'image' => new NamiImageDbField(array(
                'path' => "/static/uploaded/images/article",
                'variants' => array(
                    'cms' => array('width' => 260, 'height' => 140, 'crop' => true),
                    'small' => array('width' => 360, 'height' => 276),
                    'big' => array('width' => 1360, 'height' => 1276),
                ))
            ),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'заголовок'),
        'image' => array('title' => 'изображение', 'info' => 'файл, формат png/jpg'),        
    );
    
}