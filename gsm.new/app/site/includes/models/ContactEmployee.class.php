<?

/**
 * Сотрудник для модуля staff.
 * поле title - для админки
 */
class ContactEmployee extends NamiSortableModel {

    static function definition() {
        return array(
            'storage' => new NamiFkDbField(array('model' => 'ContactStorage')),
            'title' => new NamiCharDbField(array('maxlength' => 250)),
            'image' => new NamiImageDbField(array('path' => "/static/uploaded/images/employee",
                'variants' => array(                    
                    'preview' => array('width' => 170, 'height' => 170),
                    'cms' => array('width' => 120, 'height' => 120, 'crop' => true),
                ))),
            'name' => new NamiCharDbField(array('maxlength' => 255, 'null' => false)),
            'email' => new NamiCharDbField(array('maxlength' => 255, 'null' => false)),
            'post' => new NamiCharDbField(array('maxlength' => 100, 'null' => false)),
            'phone' => new NamiCharDbField(array('maxlength' => 100)),
            'phonedesc' => new NamiCharDbField(array('maxlength' => 255)),
            'icq' => new NamiCharDbField(array('maxlength' => 20)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'image' => array('title' => 'Фото', 'info' => 'Рекомендуемые размеры: 170x170 пикс. Формат изображения: png/jpg'),
        'name' => array('title' => 'Имя'),
        'email' => array('title' => 'E-mail'),
        'post' => array('title' => 'Должность, обязаность'),
        'phone' => array('title' => 'Телефон'),
        'phonedesc' => array('title' => 'Описание телефона'),
        'icq' => array('title' => 'icq')
    );

    function beforeSave() {
        $this->title = $this->name . " - " . $this->post;
    }

}
