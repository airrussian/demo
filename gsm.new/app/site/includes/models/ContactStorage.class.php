<?php

class ContactStorage extends NamiSortableModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250)),
            'city' => new NamiFkDbField(array('model' => 'ContactCity')),
            'abriveature' => new NamiCharDbField(array('maxlength' => 250)),
            'system' => new NamiBoolDbField(array('default' => true, 'index' => false)),
            'prefix' => new NamiCharDbField(array('maxlength' => 250, 'index' => true)),
            'main' => new NamiBoolDbField(array('default' => false)),
            'text' => new NamiTextDbField(),
            'address' => new NamiCharDbField(array('maxlength' => 500)),
            'address_text' => new NamiTextDbField(),
            'modework' => new NamiTextDbField(),
            'telephone' => new NamiArrayDbField(array('type' => 'NamiTextDbField')),
            'yandexmap_point' => new NamiCharDbField(array('maxlength' => 100, 'null' => false)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название'),
        'abriveature' => array('title' => 'Аббревиатура склада', 'info' => 'Используется при выводе товаров списком'),
        'prefix' => array('title' => 'Префикс', 'info' => 'Используется для сопоставление с выгрузкой'),
        'system' => array('title' => 'Не выводить на странице контактов'),
        'main' => array('title' => 'Основной склад город'),
        'text' => array('title' => 'Описание'),
        'address' => array('title' => 'Адрес'),
        'address_text' => array('title' => 'Адрес, пояснение'),
        'modework' => array('title' => 'Режим работы'),
        'telephone' => array('title' => 'Телефоны'),
        'yandexmap_point' => array('title' => 'YandexMap координаты метки и масштаб карты', 'widget' => 'yandex_map'),
    );
    
    static $storages = array();

    static function getNameByPrefix($prefix, $city) {
        if (!isset(self::$storages[$city])) {
            self::getAllCity($city);
        }
        
        if (isset(self::$storages[$city][$prefix])) {
            Meta::vd(self::$storages[$city][$prefix]->title);
            return self::$storages[$city][$prefix]->title;
        } else {
            return false;
        }
    }

    static function GetMain($city) {
        if (!isset(self::$storages[$city->prefix])) {
            self::getAllCity($city);
        }
        foreach (self::$storages[$city->prefix] as $storage) {
            if ($storage->main) {
                return $storage;
            }
        }
        
        return false;
    }

    static function getAllCity($city) {
        if (!isset(self::$storages[$city->prefix])) {                        
            $storages = ContactStoragies()->filter(array('city' => $city, 'enabled' => true))->sortedOrder()->all();
            foreach ($storages as $storage) {
                self::$storages[$city->prefix][$storage->prefix] = $storage;                
            }
        } 
        
        return self::$storages[$city->prefix];
    }
    
    static function GetByPrefix($prefix, $city) {        
        return ContactStoragies()->get(array('prefix' => $prefix, 'city' => $city, 'enabled' => true));
    }

}
