<?php

class SiteOrderDeliveryMethod extends NamiSortableModel {
    
    static $field_json_schema = [
        "delivery" => ["Город", "Стоимость", "Бесплатная, от", "Идентификатор 1С"],
        "pickup" => ["Город", "Адрес", "КОД"]
    ];        
    
     static function definition() {
        return [
            'title' => new NamiCharDbField(['maxlength' => 255]),
            'name' => new NamiCharDbField(['maxlength' => 255, 'index' => true]),
            'text' => new NamiTextDbField(),
            'enabled' => new NamiBoolDbField(['default' => true, 'index' => true]),
            'delivery' => new NamiTextDbField(),
            'pickup' => new NamiTextDbField(),
            'price' => new NamiPriceDbField(),
            'free' => new NamiPriceDbField(), 
            'pay' => new NamiCharDbField(['maxlength' => 100]),
        ];
    }

    public $description = [
        'title' => ['title' => 'Способ доставки'],
        'name' => ['title' => 'Название'],
        'text' => ['title' => 'Пояснение/Описание'],      
        'delivery' => ['title' => 'Стоимость доставки по городам', 'widget' => 'json'],
        'pickup' => ['title' => 'Самовывоз', 'widget' => 'json'],
        'price' => ['title' => 'Стоимость доставки'],
        'free' => ['title' => 'Бесплатная доставка, от'],
        'pay' => ['title' => 'Способы оплаты', 'widget' => 'chosen', 'choices' => 'SiteOrderDeliveryPayMethod']
    ];
    
    
    static function getPrice($totalprice, $name, $city) {
        $deliveryMethod = SiteOrderDeliveryMethods()->get(["name" => $name]);
        $tocities = json_decode($deliveryMethod->delivery, true);   
        
        if ($delivery = self::findCity($tocities, $city)) {
            if ($totalprice < $delivery['free']) {
                $return = $delivery['price']; 
            } else {
                $return = 0;
            }
        } else {
            if ($totalprice < $deliveryMethod->free) {
                $return = $deliveryMethod->price;
            } else {
                $return = 0;
            }
        }
        
        return $return;
        
    }
    
    static function findCity($tocities, $city) {
        $cityname = ContactCity::GetByPrefix($city);
        foreach ($tocities as $tocity) {
            if ($tocity[0] == $cityname->title) {
                return ['price' => $tocity[1], 'free' => $tocity[2], 'code' => $tocity[3]];
            }
        }        
        return false;
    }
    
    static function getCode($name, $city) {                
        $deliveryMethod = SiteOrderDeliveryMethods()->get(["name" => $name]);        
        $tocities = json_decode($deliveryMethod->delivery, true);                
        if ($delivery = self::findCity($tocities, $city)) {            
            return trim($delivery['code']);
        }
        
        return false;
    }
    
}