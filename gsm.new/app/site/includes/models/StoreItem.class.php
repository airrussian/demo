<?php

class StoreItem extends NamiModel
{
    static function definition()
    {
        return [
            'town'      => new NamiFkDbField(['model' => 'TownItem', 'index' => true]),
            'title'     => new NamiCharDbField(['maxlength' => 255]),
            'slug1C'    => new NamiCharDbField(['maxlength' => 10, 'index' => true])
        ];
    }
}