<?

class Faq extends NamiModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250)),
//            'category' => new NamiFkDbField(array('model' => 'FaqCategory')),
            'name' => new NamiCharDbField(array('maxlength' => 250)),
            'email' => new NamiCharDbField(array('maxlength' => 250)),
            'question' => new NamiTextDbField(array('localized' => false)),
            'answer' => new NamiTextDbField(array('localized' => false)),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M', 'index' => true)),
            'is_new' => new NamiBoolDbField(array('default' => true, 'index' => true)),
            'enabled' => new NamiBoolDbField(array('default' => false, 'index' => true)),
        );
    }

    public $description = array(
        'name' => array('title' => 'Имя'),
        'email' => array('title' => 'Адрес эл.почты'),
        'question' => array('title' => 'Текст вопроса'),
        'answer' => array('title' => 'Текст ответа', 'widget' => 'richtext'),
        'date' => array('title' => 'Дата', 'widget' => 'datetime'),
    );

    function afterSave($is_new) {
        if (!$is_new) {
            $this->is_new = false;
            $this->hiddenSave();
        }
    }

    function beforeSave() {
        $this->title = Meta::cut_text($this->question, 80);
    }

}

?>