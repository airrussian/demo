<?

//Бренд
class CatalogBrand extends NamiModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250, 'null' => false, 'localized' => false)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }
    
    public $description = array(
        'title' => array('title' => 'Название'),        
    );

}