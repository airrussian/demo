<?php

class SiteUserCart extends NamiModel {

    static function definition() {
        return array(
            'user' => new NamiFkDbField(array('model' => 'SiteUser', 'index' => true)),
            'city' => new NamiFkDbField(array('model' => 'ContactCity', 'index' => true)),
            'cart_data' => new NamiTextDbField(),
        );
    }

}
