<?

/**
 *   Ответ голосования
 */
class PollAnswer extends NamiSortableModel {

    static function definition() {
        return array(
            'poll' => new NamiFkDbField(array('model' => 'Poll', 'related' => 'answers')),
            'title' => new NamiCharDbField(array('maxlength' => 500)),
            'info' => new NamiCharDbField(array('maxlength' => 500)),
            'votes' => new NamiIntegerDbField(array('default' => 0, 'index' => true)),
            'votes_percent' => new NamiIntegerDbField(array('default' => 0, 'index' => true)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Текст варианта ответа'),
    );

    function afterSave() {
        //перераспределение процентоного соотношения голосов
        if ($this->isDirty("votes")) {
            $total_votes = 0;
            $answers = PollAnswers()
                ->filter(array(
                    "enabled" => true,
                    "poll" => $this->poll
                ))
                ->all();

            foreach ($answers as $answer) {
                $total_votes += $answer->votes;
            }

            foreach ($answers as $answer) {
                $answer->votes_percent = (100 / $total_votes) * $answer->votes;
                $answer->Save();
            }
        }

        if ($this->isDirty("votes_percent")) {
            $this->info = "количество голосов: " . $this->votes . " (" . $this->votes_percent . "%)";
            $this->hiddenSave();
        }
    }

}
