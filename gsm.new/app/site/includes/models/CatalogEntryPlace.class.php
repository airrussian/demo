<?php

class CatalogEntryPlace extends NamiModel
{
    static function definition()
    {
        return [
            'entry'     => new NamiFkDbField(['model' => 'CatalogEntry', 'index' => true]),
            'town'      => new NamiFkDbField(['model' => 'TownItem', 'index' => true]),
            'cnt'       => new NamiIntegerDbField(['default' => 0])
        ];
    }
}