<?php

/**
 * Class SiteUserTypePrice
 *
 * Типы пользователей (Цены)
 */

class SiteUserTypePrice extends NamiModel
{
    static function definition()
    {
        return [
            'title'     => new NamiCharDbField(['maxlength' => 255]),
            'slug1C'    => new NamiCharDbField(['maxlength' => 10, 'index' => true])
        ];
    }

    private $sync_items = [
        ['title' => 'Розничные цены',       'slug1C' => 'rozn'],
        ['title' => 'Мелкооптовые цены',    'slug1C' => 'm_opt'],
        ['title' => 'Оптовые цены',         'slug1C' => 'opt'],
    ];
}
