<?php

class SiteUserPending extends NamiModel {

    static function definition() {
        return array(
            'user' => new NamiFkDbField(array('model' => 'SiteUser', 'index' => true)),
            'city' => new NamiFkDbField(array('model' => 'ContactCity', 'index' => true)),
            'pending_data' => new NamiTextDbField(),
        );
    }

}
