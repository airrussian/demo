<?

//линк тега и товара
class SimpleCatalogEntryTagLink extends NamiModel {

    static function definition() {
        return array(
            'entry' => new NamiFkDbField(array('model' => 'SimpleCatalogEntry')),
            'tag' => new NamiFkDbField(array('model' => 'SimpleCatalogTag')),
        );
    }

}
