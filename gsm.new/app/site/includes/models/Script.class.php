<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Script
 *
 * @author air
 */
class Script extends NamiModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 50, 'index' => true)),
            'name' => new NamiCharDbField(array("maxlength" => 100)),
            'lastRun' => new NamiDatetimeDbField(array('format' => '%d.%m.%Y %H:%M', 'index' => true)),
            'isRun' => new NamiBoolDbField(array("default" => false, 'index' => true)),            
            'class' => new NamiCharDbField(array("maxlength" => 100)),
            'seek' => new NamiIntegerDbField(),
            'params' => new NamiTextDbField(),
        );
    }
    
    public $description = array(
        'title' => array('title' => 'Название'),                      
        'name' => array("title" => 'Имя вызова'),
        'class' => array('title' => 'Название класса скрипта', 'info' => "ImagesLoader"),                        
    );           

}
