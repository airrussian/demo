<?php

class CatalogSeriesItem extends NamiSortableModel {

    static function definition() {
        return array(
            'category' => new NamiFkDbField(array('model' => 'CatalogCategory', 'index' => true)),
            'title' => new NamiCharDbField(array('maxlength' => 255)),
            'name' => new NamiCharDbField(array('maxlength' => 255, 'index' => true)),
            'image' => new NamiImageDbField(array(
                'path' => "/static/uploaded/images/catalog/series",
                'variants' => array(
                    'preview' => array('width' => 260, 'height' => 180, 'crop' => true),
                    'cms' => array('width' => 100, 'height' => 100, 'spacefill' => true),
                ))
            ),
            'enabled' => new NamiBoolDbField(['default' => false]),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название серии'),      
        'name' => array('title' => 'Название серии в выгрузке', 'info' => 'Данное значение редактировать в ручную не рекомендуется'),
        'image' => array('title' => 'Изображение', 'info' => 'Рекомендуемый размер 260x180 пикс')
    );
    
    public function __full_uri() {
        $entry = CatalogEntries()->filter(['enabled' => true, 'series' => $this])->orderDesc("rating")->first();
        return $entry->full_uri; 
    }

    public function get_full_uri($item) {
        $entry = CatalogEntries()->filter(['enabled' => true, 'series' => $this, 'category' => $item->category])->orderDesc("rating")->first();
        return $entry ? $entry->full_uri : $this->__full_uri();
    }
    
    public function beforeSave() {
        if (!$this->title) {
            $this->title = $this->name;
        }
    }

}
