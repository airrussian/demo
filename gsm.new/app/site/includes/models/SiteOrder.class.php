<?

class SiteOrder extends NamiModel {

    static function definition() {
        return array(
            'user' => new NamiFkDbField(array('model' => 'SiteUser', 'index' => true)),
            'name' => new NamiCharDbField(array('maxlength' => 255, 'index' => true)),
            'phone' => new NamiCharDbField(array('maxlength' => 255)),
            'email' => new NamiCharDbField(array('maxlength' => 255)),
            'address' => new NamiCharDbField(array('maxlength' => 250)),
            'comment' => new NamiTextDbField(),
            'delivery_type' => new NamiCharDbField(array('maxlength' => 255)),
            'delivery_code' => new NamiCharDbField(array('maxlength' => 8)),
            'delivery_price' => new NamiPriceDbField(array('null' => true)),
            'city' => new NamiCharDbField(array('maxlength' => 255)),
            'storage' => new NamiCharDbField(array('maxlength' => 255)),
            'pay_type' => new NamiCharDbField(array('maxlength' => 255)),
            'online_id' => new NamiIntegerDbField(array('index' => true)),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M', 'index' => true)),
            'lastUpdate' => new NamiDatetimeDbField(array('format' => '%d.%m.%Y %H:%M', 'index' => true)),
            'status' => new NamiFkDbField(array('model' => 'SiteOrderStatus', 'default' => 1)),
            'qty' => new NamiIntegerDbField(),            
            'images' => new NamiTextDbField(),
            'items_price' => new NamiPriceDbField(array('null' => true)),
            'total_price' => new NamiPriceDbField(array('null' => true)),
        );
    }

    public $description = array(
        'name' => array('title' => 'Имя'),
        'phone' => array('title' => 'Телефон'),
        'address' => array('title' => 'Адрес'),
        'pay_type' => array('title' => 'Тип оплаты'),
        'city' => array('title' => 'Город заказа'),
        'storage' => array('title' => 'Склад'),
        'delivery_type' => array('title' => 'Способ доставки'),
        'delivery_price' => array('title' => 'Стоимость доставки'),
        'comment' => array('title' => 'Комментарий'),
        'date' => array('title' => 'Дата'),
        'status' => array('title' => 'Статус', 'widget' => 'select', 'choices' => 'SiteOrderStatus'),        
        'items_price' => array('title' => 'Стоимость товаров'),
        'total_price' => array('title' => 'Общая сумма заказа'),
    );

    function beforeDelete() {
        $order_items = SiteOrderItems(array('order' => $this->id))->all();

        if ($order_items) {
            foreach ($order_items as $i) {
                $i->delete();
            }
        }
    }

}
