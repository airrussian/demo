<?

//Список емейлов, на которые будет отправляться письма о заказах по крону
class OrdersEmail extends NamiModel
{

    static function definition()
    {
        return array(
            'order_id' => new NamiIntegerDbField(['maxlength' => 11]),
            'curcity'  => new NamiIntegerDbField(['maxlength' => 5]),
            'enabled'  => new NamiBoolDbField(array('default' => true)),
        );
    }
}