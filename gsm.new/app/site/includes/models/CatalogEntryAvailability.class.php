<?php

/**
 * Class CatalogEntryAvailability
 * @author Alexey Voronenko
 *
 * Наличие товаров на складах
 */

class CatalogEntryAvailability extends NamiModel {

    static function definition()
    {
        return [
            'entry' => new NamiFkDbField(['model' => 'CatalogEntry', 'index' => true]),
            'store' => new NamiFkDbField(['model' => 'StoreItem', 'index' => true]),
            'cnt'   => new NamiIntegerDbField(['default' => 0])
        ];
    }

}