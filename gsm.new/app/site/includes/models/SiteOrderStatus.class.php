<?

//Статус заказа
//статус «новый» должен иметь id — 1
class SiteOrderStatus extends NamiModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250, 'default' => 'Новый заголовок', 'null' => false)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
            //блокирует редактирование элемента для не супир админа
            'system' => new NamiBoolDbField(array('default' => false, 'index' => true)),
            //спец отметки для упрощения выборки.
            'name' => new NamiCharDbField(array('maxlength' => 250)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Статус'),
    );

}

/*
INSERT INTO `siteorderstatus` (`id`, `title`, `enabled`, `system`, `name`) VALUES
(1, 'Новый', 1, 1, 'new'),
(2, 'Эл.платеж. Ожидание оплаты', 1, 1, 'epay.new'),
(3, 'Эл.платеж. Оплачено', 1, 1, 'epay.success'),
(4, 'Эл.платеж. Отменен', 1, 1, 'epay.fail'),
(5, 'Выполнен', 1, 0, 'success'),
(6, 'Отменен', 1, 0, 'fail');
*/