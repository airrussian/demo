<?

class CatalogPhoto extends NamiSortableModel {

    protected $branchingFields = array('entry');

    static function definition() {
        return array(
            'entry' => new NamiFkDbField(array('model' => 'CatalogEntry', 'related' => 'photos')),
            'entry_id' => new NamiIntegerDbField(),
            'title' => new NamiCharDbField(array('maxlength' => 255, 'localized' => false)),
            'image' => new NamiImageDbField(
                array('path' => "/static/uploaded/images/catalog",
                    'variants' => array(
                        'small' => array('width' => 50, 'height' => 50, 'spacefill' => true, 'padding' => 3),
                        'medium' => array('width' => 215, 'height' => 125, 'spacefill' => true),
                        'large' => array('width' => 640, 'enlarge' => false),
                        'cms' => array('width' => 100, 'height' => 100, 'spacefill' => true),
                    ))),
            'moderate' => new NamiBoolDbField(array('default' => false)),
        );
    }

    function beforeSave() {
        if ($this->entry) {
            $this->entry_id = $this->entry->id;
            self::check_entry_img($this->entry->id);
        }
    }

    function beforeDelete() {
        if ($this->entry) {
            self::check_entry_img($this->entry->id);
        }
    }

    static function check_entry_img($entry_id) {
        $entry = CatalogEntries()->get($entry_id);

        if ($entry) {
            $entry->need_img_export = true;
            $entry->hiddenSave();
        }
    }

}
