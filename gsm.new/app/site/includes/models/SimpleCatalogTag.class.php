<?

class SimpleCatalogTag extends NamiSortableModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 200, 'null' => false)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название'),
    );

    function beforeDelete() {
        $remove_params = array(
            "links_model_name" => "SimpleCatalogEntryTagLink",
            "link_field_name" => "tag",
            "link_field_value" => $this->id,
        );

        LinksProcessor::remove_links($remove_params);
    }

    function __full_uri() {
        return Builder::getAppUri("SimpleCatalogApplication") . "by-tag/" . $this->id . "/";
    }

}
