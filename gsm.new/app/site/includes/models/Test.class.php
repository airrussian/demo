<?

/**
  Текстовый блок сайта
 */
class Test extends NamiModel {

    static function definition() {
        return array(
            'apple' => new NamiTextDbField(),
            'acer' => new NamiTextDbField(),
            'count' => new NamiIntegerDbField(),
            'rand' => new NamiIntegerDbField(),
            'randpage' => new NamiTextDbField(),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%i', 'index' => true)),
        );
    }

}
