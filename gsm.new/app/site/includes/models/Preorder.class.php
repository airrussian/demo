<?php

class Preorder extends NamiModel {

    protected $branchingFields = array('entries');

    static function definition() {
        return array(            
            'entry' => new NamiFkDbField(array('model' => 'CatalogEntry', 'related' => 'preorders')),
            'name' => new NamiCharDbField(array('maxlength' => 250)),
            'city' => new NamiCharDbField(array('maxlength' => 250)),
            'email' => new NamiCharDbField(array('maxlength' => 250)),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M')),
            'lastcheck' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'index' => true)),
            'done' => new NamiBoolDbField(array('default' => false, 'index' => 'nav')),
            'user' => new NamiFkDbField(array('model' => 'SiteUser', 'index' => true)),
        );
    }

    public $description = array(
        'name' => array('title' => 'ФИО'),
        'city' => array('title' => 'Город'),        
        'email' => array('title' => 'E-mail'),        
        'done' => array('title' => 'Пользователь проинформирован о поступлении товара на склад')
    );

}
