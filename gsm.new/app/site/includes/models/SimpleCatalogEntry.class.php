<?

class SimpleCatalogEntry extends NamiModel {

    static function definition() {
        return array(
            'category' => new NamiFkDbField(array('model' => 'SimpleCatalogCategory', 'null' => false)),
            'title' => new NamiCharDbField(array('maxlength' => 400, 'null' => false)),
            'price' => new NamiPriceDbField(),
            'descr' => new NamiTextDbField(),
            'images' => new NamiArrayDbField(array(
                'type' => 'NamiImageDbField',
                'path' => '/static/uploaded/images_array/catalogentry',
                'variants' => array(
//                    'large' => array('width' => 800, 'height' => 600),
                    'cms' => array('width' => 120, 'height' => 120, 'crop' => true),
                ))
            ),
            'resaved' => new NamiBoolDbField(array('default' => false, 'index' => true)),
            'enabled' => new NamiBoolDbField(array('default' => false, 'index' => true)),
            'meta_title' => new NamiCharDbField(array('maxlength' => 1000)),
            'meta_keywords' => new NamiTextDbField(),
            'meta_description' => new NamiTextDbField(),
//            'tags_ids' => new NamiCharDbField(array('maxlength' => 250)),
//            'entries_ids' => new NamiCharDbField(array('maxlength' => 250)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название товара'),
        'price' => array('title' => 'Цена'),
        'descr' => array('title' => 'Описание', 'widget' => 'richtext'),
        'images' => array('title' => 'Изображения. Рекомендуемый размер 100x100 пикс.', 'widget' => 'images'),
        'meta' => array('title' => 'Ключевые слова, описание и заголовок (SEO)', 'widget' => 'seo_fields'),
//        'tags_ids' => array('title' => 'Теги', 'widget' => 'chosen', 'choices' => 'SimpleCatalogTag'),
//        виджет выбора товаров из каталога по категориям. последний параметр — не отображать в списке редактируемый элемент.
//        'entries_ids' => array('title' => 'Товары', 'widget' => 'catalog_item_ids', 'not_use_own_id' => false),
    );

    function __full_uri() {
        return Builder::getAppUri('SimpleCatalogApplication') . $this->category->uri . $this->id . "/";
    }

    function afterSave() {
        if ($this->price === NULL) {
            $this->price = (string) 0;
            $this->hiddenSave();
        }
    }

//    function afterSave() {
//        $main_model_data = array(
//            "id" => $this->id,
//            "related_ids_new_value" => $this->tags_ids,
//        );
//
//        $link_model_data = array(
//            "name" => "SimpleCatalogEntryTagLink",
//            "main_model_field_name" => "entry",
//            "related_model_field_name" => "tag",
//        );
//
//        LinksProcessor::check_links($main_model_data, $link_model_data);
//    }
//
//    function beforeDelete() {
//        $remove_params = array(
//            "links_model_name" => "SimpleCatalogEntryTagLink",
//            "link_field_name" => "entry",
//            "link_field_value" => $this->id,
//        );
//
//        LinksProcessor::remove_links($remove_params);
//    }
}
