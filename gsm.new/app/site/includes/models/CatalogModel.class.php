<?

//модель
class CatalogModel extends NamiModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250, 'null' => false, 'localized' => false)),
            'brand' => new NamiFkDbField(array('model' => 'CatalogBrand')),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

}