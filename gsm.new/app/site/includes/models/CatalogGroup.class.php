<?

//Группа
class CatalogGroup extends NamiModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250, 'null' => false, 'localized' => false)),
            'model' => new NamiFkDbField(array('model' => 'CatalogModel')),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название'),
        'model' => array('title' => 'Модель', 'widget' => 'select'),
    );

    function beforeDelete() {
        $links = CatalogEntryGroupLinks(array("group" => $this->id))->all();
        if ($links) {
            $entries = array();
            foreach ($links as $link) {
                $entries[] = $link->entry;
                $link->delete();
            }

            if (count($entries) > 0) {
                foreach ($entries as $entry) {
                    $new_groups_ids = explode(",", $entry->groups_ids);
                    $new_groups_ids = array_diff($new_groups_ids, array(1 => $this->id));
                    if (count($new_groups_ids) > 0) {
                        $entry->groups_ids = implode(",", $new_groups_ids);
                    } else {
                        $entry->groups_ids = "";
                    }
                    $entry->hiddenSave();
                }
            }
        }
    }
}