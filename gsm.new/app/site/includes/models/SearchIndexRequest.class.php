<?

class SearchIndexRequest extends NamiModel {

    static function definition() {
        return array(
            'uri' => new NamiCharDbField(array('maxlength' => 500, 'index' => true,)),
            'added' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y', 'index' => true,)),
        );
    }

}
