<?

//линк группы и товара
class CatalogEntryGroupLink extends NamiModel {

    static function definition() {
        return array(
            'entry' => new NamiFkDbField(array('model' => 'CatalogEntry')),
            'group' => new NamiFkDbField(array('model' => 'CatalogGroup')),
        );
    }

}