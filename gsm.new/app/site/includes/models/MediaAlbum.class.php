<?

//альбомчик
class MediaAlbum extends NamiModel {

    static function definition() {
        return array(
            'category' => new NamiFkDbField(array('model' => 'MediaCategory')),
            'title' => new NamiCharDbField(array('maxlength' => 250, 'null' => false)),
            'name' => new NamiCharDbField(array('maxlength' => 250)),
            'text' => new NamiTextDbField(),
            'images_count' => new NamiIntegerDbField(),
            'videos_count' => new NamiIntegerDbField(),
            'cover' => new NamiImageDbField(array(
                'path' => "/static/uploaded/images/media/covers",
                'variants' => array(
//                    'preview' => array('width' => 360, 'height' => 225, 'crop' => true),
                    'cms' => array('width' => 120, 'height' => 120, 'crop' => true),
                ))
            ),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y', 'index' => true, 'null' => false)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название'),
        'name' => array('title' => 'Название для url'),
        'text' => array('title' => 'Описание', 'widget' => 'richtext'),
        'cover' => array('title' => 'Обложка альбома', 'info' => 'рекомендуемый размер 100x100 пикс.'),
        'date' => array('title' => 'Дата'),
    );

    function __full_uri() {
        return $this->category->full_uri . $this->id . "/";
    }

    function beforeSave() {
        if (!Meta::isPathName($this->name)) {
            if ($this->name) {
                $this->name = Meta::getPathName($this->name);
            }

            if (!Meta::isPathName($this->name)) {
                $this->name = Meta::getPathName($this->title);
            }
        }
    }

    function afterSave($new) {
        if ($this->isDirty('name')) {
            $has_albums = MediaAlbums()
                    ->filter(array(
                        'id__ne' => $this->id,
                        'name' => $this->name,
                        'category' => $this->category
                    ))
                    ->first();

            if ($has_albums) {
                $this->name .= $this->id;
                $this->hiddenSave();
            }
        }
    }

    function beforeDelete() {
        $items = MediaItems()
                ->filter(array(
                    "album" => $this->id,
                ))
                ->all();

        foreach ($items as $item) {
            $item->delete();
        }
    }

}
