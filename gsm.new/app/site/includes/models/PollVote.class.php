<?

/**
 *   Ip адреса, участвовавшие в голосовании
 */
class PollVote extends NamiModel {

    static $forwarded_length = 200;

    static function definition() {
        return array(
            'poll' => new NamiFkDbField(array('model' => 'Poll', 'related' => 'votes')),
            'answer' => new NamiFkDbField(array('model' => 'PollAnswer', 'related' => 'vote_addrs')),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M')),
            'remote_addr' => new NamiCharDbField(array('maxlength' => 32, 'index' => 'addr')),
            'forwarded' => new NamiCharDbField(array('maxlength' => self::$forwarded_length, 'index' => 'addr')),
        );
    }

    static function create_new_vote($poll_answer, $poll_id) {
        //АДМИНЫ ВКЛЮЧИТЕ ФИЛЬТРЫ НА НАКРУТКУ!!!111ONE
        $remote_addr = array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : '';
        $forwarded = array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
        $forwarded = substr($forwarded, 0, PollVote::$forwarded_length);
        $voted = PollVotes()
            ->filter(array('remote_addr' => $remote_addr, 'forwarded' => $forwarded, 'poll' => $poll_id))
            ->first();

        if (!$voted) {
            PollVotes()
                ->create(array(
                    'answer' => $poll_answer,
                    'remote_addr' => $remote_addr,
                    'forwarded' => $forwarded,
                    'poll' => $poll_id
            ));

            $poll_answer->votes = $poll_answer->votes + 1;
            $poll_answer->save();
        }

        $new_ids = array();
        if (isset($_COOKIE['builder_polls'])) {
            $new_ids = explode(",", $_COOKIE['builder_polls']);
        }
        $new_ids[] = $poll_answer->poll->id;

        setcookie("builder_polls", implode(",", $new_ids), strtotime('now + 1 year'), '/');
    }

}

/*
REMOTE_ADDR
HTTP_X_FORWARDED_FOR
*/