<?

class SiteOrderItem extends NamiModel {

    static function definition() {
        return array(
            'order' => new NamiFkDbField(array('model' => 'SiteOrder')),
            'entry' => new NamiFkDbField(array('model' => 'CatalogEntry', 'related' => 'entries')),
            'oneC_id' => new NamiCharDbField(array('maxlength' => 8)),
            'qty' => new NamiFloatDbField(),
            'price' => new NamiPriceDbField(),
        );
    }   

}
