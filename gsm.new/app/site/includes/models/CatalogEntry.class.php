<?php

class CatalogEntry extends CatalogEntryModel {

    protected $branchingFields = array('category');

    static function definition() {
        return array(
            'category' => new NamiFkDbField(array('model' => 'CatalogCategory', 'related' => 'entries')),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true, 'localized' => false,)),
            //------------------------ поля обновляемые из 1с --------
            'oneC_category_id' => new NamiCharDbField(array('maxlength' => 255, 'localized' => false, 'index' => true)),
            'title' => new NamiCharDbField(array('maxlength' => 1000, 'localized' => false, 'fulltext' => true)),
            'oneC_id' => new NamiCharDbField(array('maxlength' => 255, 'localized' => false, 'index' => true)),
            //цены на товар
            'price_opt' => new NamiPriceDbField(array('index' => true)),
            'price_rozn' => new NamiPriceDbField(array('index' => true)),
            'price_m_opt' => new NamiPriceDbField(array('index' => true)),
            // Наличие в городах, поля для выборки и сортировки 
            'a_krsk' => new NamiIntegerDbField(array('index' => true)),
            'a_irk' => new NamiIntegerDbField(array('index' => true)),
            'a_vlad' => new NamiIntegerDbField(array('index' => true)),
            'a_ufa' => new NamiIntegerDbField(array('index' => true)),
            'a_omsk' => new NamiIntegerDbField(array('index' => true)),
            'a_abakan' => new NamiIntegerDbField(array('index' => true)),
            'a_ulanude' => new NamiIntegerDbField(array('index' => true)),
            'a_mow' => new NamiIntegerDbField(array('index' => true)),
            'a_kzn' => new NamiIntegerDbField(array('index' => true)),
            'a_tym' => new NamiIntegerDbField(array('index' => true)),
            // Наличие в магазинах, пеле содержит подробную информацию
            'qty' => new NamiTextDbField(),
            // Место хранения товара в города JSON массив
            'place' => new NamiTextDbField(),
            //
            'recomended' => new NamiBoolDbField(array('default' => false, 'index' => true)),
            'sale' => new NamiBoolDbField(array('default' => false, 'index' => true)),
            'onorder' => new NamiBoolDbField(array('default' => false, 'index' => true)),
            'key_words' => new NamiTextDbField(array('fulltext' => true, 'localized' => false)),
            //---------------------------------------          
            'text' => new NamiTextDbField(array('localized' => false, 'fulltext' => true)),
            'new' => new NamiBoolDbField(array('default' => false)),
            //список ид групп для товара
            'groups_ids' => new NamiCharDbField(array('maxlength' => 1000, 'localized' => false)),
            //экспорт фото
            'need_img_export' => new NamiBoolDbField(array('default' => true, 'index' => true)),
            'images' => new NamiArrayDbField(array(
                'type' => 'NamiImageDbField',
                'path' => '/static/uploaded/images/catalog/entry',
                'variants' => array(
                    'cms' => array('width' => 120, 'height' => 120, 'crop' => true),
                    'mini' => array('width' => 60, 'height' => 60, 'crop' => true),
                    'small' => array('width' => 213, 'height' => 166, 'crop' => true),
                    'big' => array('width' => 570),
                ))
            ),
            // "рейтинг" товара см. /bin/update_rating.php
            'rating' => new NamiIntegerDbField(array('default' => 0)),
            'guarantee' => new NamiCharDbField(array('maxlength' => 255)),
            'cache_availability' => new NamiTextDbField(),
            'series' => new NamiFkDbField(['model' => 'CatalogSeriesItem', 'index' => true]),
            'seriesname' => new NamiCharDbField(['maxlength' => 255]),
            'color' => new NamiCharDbField(['maxlength' => 255]),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название'),
        'price_opt' => array('title' => 'Цена, оптовая'),
        'price_rozn' => array('title' => 'Цена, розничная'),
        'price_m_opt' => array('title' => 'Цена, мелкооптовая'),
        // 
        'a_krsk' => array('title' => 'Наличие в Красноярске'),
        'a_irk' => array('title' => 'Наличие в Иркутске'),
        'a_vlad' => array('title' => 'Наличие во Владивостоке'),
        'a_ufa' => array('title' => 'Наличие в Уфе'),
        'a_omsk' => array('title' => 'Наличие в Омске'),
        'a_abakan' => array('title' => 'Наличие в Абакане'),
        'a_kzn' => array('title' => 'Наличие в Казани'),
        'a_tym' => array('title' => 'Наличие в Тюмени'),
        'qty' => array('title' => 'Общая информация о количестве'),
        'place' => array('title' => 'Место хранения'),
        'oneC_id' => array('title' => 'Артикул'),
        'groups_ids' => array('title' => 'Товарная группа'),
        'text' => array('title' => 'Описание', 'widget' => 'richtext'),
        'recomended' => array('title' => 'Рекомендованный'),
        'sale' => array('title' => 'Распродажа'),
        'new' => array('title' => 'Новинка'),
        'images' => array('title' => 'Блок фотографий', 'widget' => 'images', 'info' => 'Рекомендуемый размер 1028x800'),
        'guarantee' => array('title' => 'Гарантия'),
    );

    function __full_uri() {
        return $this->category->full_uri . $this->id . "/";
    }

    function __relative_link() {
        $uri = Builder::getAppUri('CatalogApplication') . "{$this->category->uri}" . "/{$this->id}/";
        $uri = explode("/", $uri);
        unset($uri[0]);
        unset($uri[1]);

        return implode("/", $uri);
    }

    /**
     * Price мы выводим тремя разными способами
     */
    function __html_block_price() {
        $city = SiteCity::getInstance();
        $type = $city->get_price_type_name();
        $price = $this->{$type};
# делаем цену красивой
        $price = explode(",", round($price, 2));
        $price_for_trans = $price[0];
        $price[0] = preg_replace("/(\d)(?=(\d{3})+$)/", "$1 ", $price[0]);
        if (isset($price[1])) {
            return $price[0] . "," . $price[1] . " " . Meta::decline($price_for_trans, "рублей", "рубль", "рубля", "рублей");
        } else {
            return $price[0] . " " . Meta::decline($price_for_trans, "рублей", "рубль", "рубля", "рублей");
        }
    }

    function __html_table_price() {
        $city = SiteCity::getInstance();
        $type = $city->get_price_type_name();
        $price = $this->{$type};
# делаем цену красивой
        $price = explode(",", round($price, 2));
        $price[0] = preg_replace("/(\d)(?=(\d{3})+$)/", "$1 ", $price[0]);
        if (isset($price[1])) {
            return $price[0] . "," . $price[1];
        } else {
            return $price[0];
        }
    }

    function __js_price() {
        $city = SiteCity::getInstance();
        $type = $city->get_price_type_name();
        return round($this->{$type}, 2);
    }

    /**
     * Есть в наличии в текущем городе
     */
    function __city_availability() {
        $result = false;

        return $result;
    }

    /**
     * Есть ли в наличии в основном складе города
     */
    function __main_storage_city_availability() {
        $result = false;

        $city = ContactCity::getCurrent()->prefix;
        $qty = json_decode($this->qty, true);

        $result = ($qty[$city]['main'] > 0) ? true : false;

        return $result;
    }

    function afterSave() {
//------------------------ проверка link таблиц -----------------
        $model_groups_ids = explode(",", $this->groups_ids);
        $link_groups_ids = CatalogEntryGroupLinks(array("entry" => $this->id))->values("group");

        if ($model_groups_ids != $link_groups_ids) {
            if (!$this->groups_ids) {
                NamiCore::getBackend()->cursor->execute("DELETE FROM catalogentrygrouplink WHERE entry='" . $this->id . "'");
            } else {
//теги которых уже нет - отсеиваем
                $all_groups_ids = CatalogGroups(array("enabled" => true))->values("id");
                $model_groups_ids = array_intersect($model_groups_ids, $all_groups_ids);


                $groups_in_db = array();
                $groups_ids_kill = array();
                foreach ($link_groups_ids as $group_id) {
                    if (in_array($group_id, $model_groups_ids)) {
                        $groups_in_db[] = $group_id;
                    } else {
                        $groups_ids_kill[] = $group_id;
                    }
                }

                $groups_ids_to_add = array_diff($model_groups_ids, $groups_in_db);
                if (count($groups_in_db > 0)) {
                    foreach ($groups_ids_to_add as $group_id) {
                        CatalogEntryGroupLinks()->create(array("entry" => $this->id, "group" => $group_id));
                    }
                }

                if (count($groups_ids_kill) > 0) {
                    if (count($groups_ids_kill) == 1) {
                        $query = "DELETE FROM `catalogentrygrouplink` WHERE `group`=" . $groups_ids_kill[0] . " AND `entry`= " . $this->id;
                    } else {
                        $query = sprintf("DELETE FROM `catalogentrygrouplink` WHERE `group` in (%s) AND `entry`='" . $this->id . "'", join(',', $groups_ids_kill));
                    }

                    NamiCore::getBackend()->cursor->execute($query);
                }
            }
        }

        if ($this->id && $this->title) {
            $mementry = CatalogEntryMems()->get(['entry' => $this]);
            if (!$mementry) {
                $mementry = CatalogEntryMems()->create(['entry' => $this->id, 'title' => $this->title]);
            }
            $mementry->title = $this->title;
            $mementry->hiddenSave();
        }
    }

    public function beforeSave() {
        $this->cache_availability = NULL;

        $this->title = $this->title . " ";
    }

    public function beforeDelete() {
        $mementry = CatalogEntryMems()->get(['entry' => $this]);
        $mementry->delete();
    }

    public function __emptyextrafield() {

        $result = true;

        $fieldset = $this->category->fieldset;
        if (!$fieldset) {
            $parent_categories = CatalogCategories()->filterParents($item->category)->all();
            foreach ($parent_categories as $parent_category) {
                if ($parent_category->fieldset) {
                    $fieldset = $parent_category->fieldset;
                }
            }
        }
        $extrafields = CatalogFieldSetFields(array('fieldset' => $fieldset))
                ->follow(1)
                ->sortedOrder()
                ->all();

        if (empty($extrafields)) {
            return false;
        }

        foreach ($extrafields as $field) {
            $result = $result && (($this->{$field->field->name}) ? false : true);
        }


        return $result;
    }

    static public function historyAdd($item) {
        $history = isset($_COOKIE['history']) ? json_decode($_COOKIE['history']) : array();
        $history[] = $item->id;
        $history = array_unique($history);
        $history = array_reverse(array_slice(array_reverse($history), 0, 9));
        setcookie('history', json_encode($history), time() + 100000, "/");
    }
    
    public function getSeriesEntry() {
        if (!$this->series->enabled) 
            return false;
        
        $entries = CatalogEntries()
                ->filter(['enabled' => true, 'series' => $this->series])
                ->orderDesc("rating")
                ->all();
        
        return $entries;        
    }
    
    public function getSeriesCategory() {        
        
        $category_ids = CatalogEntries()
                ->filter(['enabled' => true, 'series' => $this->series])
                ->orderDesc("rating")
                ->values('category');        
        $category_ids = array_unique($category_ids);
        
        $return = [];
        
        foreach ($category_ids as $category_id) {       
            
            $cat = CatalogCategories()->get(['id' => $category_id]);
            
            $categories = CatalogCategories()->filter(['enabled' => true, 'lvl__gt' => 2])->filterParents($category_id)->values("id"); 
            $categories[] = $category_id;
            
            foreach ($categories as $i => $category) {
                $categories[$i] = CatalogCategories()->get(['id' => $category])->title;
            }
            
            $entry = CatalogEntries()->filter(['enabled' => true, 'series' => $this->series, 'category' => $category_id])->orderDesc("rating")->first();
            
            $select = (Meta::in_uripath($cat->full_uri, $this->full_uri)) ? true : false;
            
            $return[] = (object) ['title' => implode(" ", $categories), 'full_uri' => $entry->full_uri, 'select' => $select];            
        }
        
        uasort($return, function($a, $b) {
            return (strcmp($a->title, $b->title));
        });
        
        return $return;
    }
    
    public function getSeriesColorCategory() {
         $entries = CatalogEntries()
                ->filter(['enabled' => true, 'series' => $this->series, 'category' => $this->category])
                ->orderDesc("rating")
                ->all();
         
         $return = [];
         
         foreach ($entries as $entry) {
             $select = ($entry->full_uri == $this->full_uri) ? true : false;                         
             $return[] = (object) ['title' => $entry->color, 'full_uri' => $entry->full_uri, 'select' => $select]; 
         }
         
         return $return;
    }
    
    public function getSeriesEntries($count = 50, $offset = 0) {
        return CatalogEntries()->filter(['enabled' => true, 'series' => $this->series, 'id__ne' => $this->id])->order("title")->limit($count, $offset);        
    }
    
    public function getSeriesEntriesCount() {
        return CatalogEntries()->filter(['enabled' => true, 'series' => $this->series, 'id__ne' => $this->id])->count();
    }
    
    public function getSeriesOther() {        
        $parent = ($this->category->lvl > 2) ? CatalogCategories()->filterParents($this->category)->filterLevel(2)->all() : $this->category;
        return CatalogSeriesItems()->filter(['category' => $parent, 'enabled' => true, 'id__ne' => $this->series])->sortedOrder()->all();               
    }

}
