<?

//поиписчик
class SubscriptionUser extends NamiModel {

    static function definition() {
        return array(
            'email' => new NamiCharDbField(array('maxlength' => 500, 'null' => false)),
            'code' => new NamiCharDbField(array('maxlength' => 32, 'index' => true)),
            'created' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y')),
            'enabled' => new NamiBoolDbField(array('default' => false, 'index' => true)),
        );
    }

    public $description = array(
        'email' => array('title' => 'Email'),
    );

    function beforeSave() {
        if (!$this->code) {
            $this->code = md5($this->email . $this->created . rand());
        }
    }

    function __activation_link() {
        return $_SERVER['HTTP_HOST'] . Builder::getAppUri("SubscriptionApplication") . '/activation/' . $this->code;
    }

    function __deactivation_link() {
        return $_SERVER['HTTP_HOST'] . Builder::getAppUri("SubscriptionApplication") . '/deactivation/' . $this->code;
    }

}
