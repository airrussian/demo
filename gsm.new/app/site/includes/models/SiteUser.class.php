<?

class SiteUser extends NamiModel {
    
    static function definition() {
        return array(
            'name' => new NamiCharDbField(array('maxlength' => 250)),
            'login' => new NamiCharDbField(array('maxlength' => 200, 'index' => true)),
            'password' => new NamiCharDbField(array('maxlength' => 100)),
            'pass_old' => new NamiCharDbField(array('maxlength' => 100)),
            'enabled' => new NamiBoolDbField(array('default' => false)),
            'created' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M:%S',)),
            'type' => new NamiCharDbField(array('maxlength' => 255, 'default' => 'rozn')),
            'city' => new NamiCharDbField(array('maxlength' => 255)),
            //'city_unbind' => new NamiBoolDbField(array('default' => false)),
            'phone' => new NamiCharDbField(array('maxlength' => 250)),
            'text' => new NamiTextDbField(array('localized' => false)),
            'oneC_id' => new NamiCharDbField(array('maxlength' => 250)),
            'krsk_place_show' => new NamiBoolDbField(array('default' => false)),
            'irk_place_show' => new NamiBoolDbField(array('default' => false)),
            'vlad_place_show' => new NamiBoolDbField(array('default' => false)),
            'ufa_place_show' => new NamiBoolDbField(array('default' => false)),
            'omsk_place_show' => new NamiBoolDbField(array('default' => false)),
            'mow_place_show'  => new NamiBoolDbField(array('default' => false)),
            // хочу получать новости компании
            'subscribe' => new NamiBoolDbField(array('default' => true)),
            // получать уведомление о заказе
            'subscribe_order_status' => new NamiBoolDbField(array('default' => true)),
            'email' => new NamiCharDbField(array('maxlength' => 250, 'index' => true)),
            'address' => new NamiCharDbField(array('maxlength' => 1000)),
            'company' => new NamiCharDbField(array('maxlength' => 250)),
            'contact_personal' => new NamiCharDbField(array('maxlength' => 250)),
            'addresses_delivery' => new NamiTextDbField(), // Адреса доставки
            // взаиморасчет
            'mutual_settlement' => new NamiIntegerDbField(),
            'old_client' => new NamiBoolDbField(array('default' => false)),
            'last_login' => new NamiDatetimeDbField(array('default' => NULL, 'format' => '%d.%m.%Y %H:%M:%S')),
            'first_login' => new NamiBoolDbField(array('default' => false)),
            'cross_login' => new NamiCharDbField(array('maxlength' => 32, 'index' => true)),
            'cart_data' => new NamiTextDbField(),
            'pending_data' => new NamiTextDbField(),
            //код для восстановления пароля
            'password_recover_code' => new NamiCharDbField(array('maxlength' => 32, 'index' => true)),
            'password_recover_code_created' => new NamiDatetimeDbField(array('format' => '%d.%m.%Y %H:%M:%S')),
            'expert' => new NamiBoolDbField(array('default' => false)),
            'settings'  => new NamiTextDbField(),
        );
    }

    public $description = array(
        //'name' => array('title' => 'ФИО'),
        'contact_personal' => array('title' => 'Контактные данные ФИО'),
        'login' => array('title' => 'Логин'),        
        'created' => array('title' => 'Дата создания'),
        'city' => array('title' => 'Город'),
        //'city_unbind' => array('title' => 'Отвзян от города', 'info' => 'Если "Галочка" стоит, значит данный пользователь может авторизоваться в любом из городов'),
        'phone' => array('title' => 'Телефон'),
        'email' => array('title' => 'Email'),
        'address' => array('title' => 'Адрес'),        
        'subscribe' => array('title' => 'получать новости компании'),
        'subscribe_order_status' => array('title' => 'получать уведомление о заказе'),
        'mutual_settlement' => array('title' => 'взаиморасчет'),
        'type' => array('title' => 'Тип цен'),
        'enabled' => array('title' => 'Учетная запись активирована'),
        'expert' => array('title' => 'Специалист'),
    );

    function beforeSave() {
        if ($this->getQuerySet()->filter(array('login' => $this->login, 'id__ne' => $this->id))->count() > 0) {
            throw new Exception("Логин {$this->login} принадлежит другой учетной записи и не может быть использован.");
        }
        # если нет пароля, придумаем его сами
        if (!$this->password) {
            $this->password = $this->setNewPass(8);
        }
        $this->name = $this->login; 
    }

    /**
     * 	Генерация пароля и запись его мд5
     * @return string пароль
     */
    function setNewPass($number = 8) {        
        $this->password = md5(self::genPass($number));
        $this->save();
        return $new_pass;
    }
    
    /**
     * Генерирует случайный пароль длинной $number
     * @param int $number
     */
    static function genPass($number = 8) {
        $sumbols = array('a', 'b', 'c', 'd', 'e', 'f', 'g'
            , 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'
            , 'r', 's', 't', 'u', 'v', 'x', 'y', 'z', 'A'
            , 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'
            , 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T'
            , 'U', 'V', 'X', 'Y', 'Z', '1', '2', '3', '4'
            , '5', '6', '7', '8', '9', '0');

        $new_pass = "";
        for ($i = 0; $i < $number; $i++) {
            $index = rand(0, count($sumbols) - 1);
            $new_pass .= $sumbols[$index];
        }        
        return $new_pass;
    }
    
    /**
     * Генерирует уникальный login из указанного $email
     * @param string $email
     */
    static function genLogin($email) {
        if (preg_match('~(.+?)@.+~si', $email, $result)) {
            $login_test = $result[1];
        }
        
        // Цикл до тех пор пока в базе есть логин
        while (self::loginTest($login_test)) {
            // К предыдущему варианту добавляем случайную цифру. 
            $login_test .= rand(1, 10);
        }
        
        return $login_test;        
    }
    
    /**
     * Возвращает TRUE если логин найден в базе
     * @param type $login
     */
    static function loginTest($login) {
        return SiteUsers()->get(array('login' => $login)) ? true : false;
    }

}
