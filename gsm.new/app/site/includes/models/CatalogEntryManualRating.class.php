<?php

class CatalogEntryManualRating extends NamiModel {

    static function definition() {
        return array(
            'entry' => new NamiFkDbField(array('model' => 'CatalogEntry', 'index' => true)), 
            'oneC_id' => new NamiCharDbField(array('maxlength' => 255, 'localized' => false, 'index' => true, 'null' => false)),
            'rating' => new NamiIntegerDbField(array('default' => 0, 'null' => false))
        );
    }

    public $description = array(
        'oneC_id' => array('title' => 'Идентификатор товара', 'info' => 'Напиример: 00033693'),
        'rating' => array('title' => 'Рейтинг', 'info' => 'Целое положительное число')
    );
    
    public function beforeSave() {
        if ($this->oneC_id == "") {
            throw new Exception("Идентификатор товара не может быть пустым");
        }
        
        if ($rating = CatalogEntryManualRatings()->get(array('oneC_id' => $this->oneC_id))) {
            $this->id = $rating->id;
        }        
        
        $entry = CatalogEntries()->get(array('oneC_id' => $this->oneC_id));       
        
        if (!$entry) {
            throw new Exception("Товар с таким идентификатором не найден"); 
        }
        
        if ($this->rating < 0) {
            throw new Exception("Рейтинг товара должен быть целым положительным числом");
        }
        
        $this->entry = $entry;
    }
    
    public function afterSave() {        
        $this->entry->rating = $this->rating;
        $this->entry->hiddenSave();
    }
    
    public function beforeDelete() {        
        
        $count = SiteOrderItems()->filter(array('entry__oneC_id' => $this->oneC_id, 'order__date__ge' => strtotime("-1 month"), 'order__user__type' => 'rozn'))->count();
               
        $this->entry->rating = $count;
        $this->entry->hiddenSave();
    }
}
