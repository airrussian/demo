<?php

class CatalogEntryPrice extends NamiModel
{
    static function definition()
    {
        return [
            'entry'     => new NamiFkDbField(['model' => 'CatalogEntry', 'index' => true]),
            'typeprice' => new NamiFkDbField(['model' => 'SiteUserTypePrice', 'index' => true]),
            'value'     => new NamiPriceDbField(['default' => 0])
        ];
    }
}