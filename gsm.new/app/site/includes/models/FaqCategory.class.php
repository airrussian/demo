<?

//категория фака
class FaqCategory extends NamiSortableModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250, 'null' => false)),
            'name' => new NamiCharDbField(array('maxlength' => 250, 'index' => true)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Заголовок'),
        'name' => array('title' => 'Название для url'),
    );

    function __full_uri() {
        return Builder::getAppUri('FaqApplication') . $this->name . "/";
    }

    function beforeSave() {
        // Приводим в порядок name страницы
        if (!Meta::isPathName($this->name)) {
            // Попробуем сначала довести до ума исходный name
            if ($this->name) {
                $this->name = Meta::getPathName($this->name);
            }

            // Не получилось — сделаем на основе title
            if (!Meta::isPathName($this->name)) {
                $this->name = Meta::getPathName($this->title);
            }
        }
    }

    function afterSave($new) {
        if ($this->isDirty('name')) {
            // Проверяем уникальность имени
            if (FaqCategories()->filter(array('id__ne' => $this->id, 'name' => $this->name))->count() > 0) {
                $this->name .= $this->id;
                $this->hiddenSave();
            }
        }
    }

    function beforeDelete() {
        $this_elements = Faqs(array("category" => $this->id))->first();
        if ($this_elements) {
            throw new Exception("Невозможно удалить раздел «{$this->title}», в нем есть записи.");
        }
    }

}
