<?

//коммент для товара
class SimpleCatalogComment extends NamiModel {

    static function definition() {
        return array(
            'entry' => new NamiFkDbField(array('model' => 'SimpleCatalogEntry')),
            'title' => new NamiCharDbField(array('maxlength' => 250)),
            'name' => new NamiCharDbField(array('maxlength' => 250)),
            'text' => new NamiTextDbField(array('null' => false)),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M', 'index' => true)),
            'enabled' => new NamiBoolDbField(array('default' => false, 'index' => true)),
        );
    }

    public $description = array(
        'name' => array('title' => 'Имя'),
        'text' => array('title' => 'Сообщение'),
        'date' => array('title' => 'Дата'),
    );

    function beforeSave() {
        $this->title = $this->date . ". " . $this->name . ". " . Meta::cut_text($this->text, 50);
    }

}
