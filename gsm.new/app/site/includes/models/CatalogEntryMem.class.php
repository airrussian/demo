<?php

class CatalogEntryMem extends NamiModel {

    static function definition() {
        return [
            'entry' => new NamiFkDbField(['model' => 'CatalogEntry', 'index' => true]),
            'title' => new NamiCharDbField(['maxlength' => 1000])
        ];
    }

}
