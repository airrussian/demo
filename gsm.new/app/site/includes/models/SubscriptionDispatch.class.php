<?

class SubscriptionDispatch extends NamiModel {

    static function definition() {
        return array(
            'created' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M:%S', 'index' => true)),
            'emails' => new NamiTextDbField(),
            'publications' => new NamiTextDbField(),
        );
    }

    public $description = array();

    function afterSave($new) {
        //если модель только создали - запускаем рассылку
        if ($new) {
            $last_dispatch = SubscriptionDispatches()
                ->filter(array("id__ne" => $this->id))
                ->orderDesc("created")
                ->first();

            $last_dispatch_date = 0;
            if ($last_dispatch) {
                $last_dispatch_date = $last_dispatch->created;
            }

            $users = SubscriptionUsers(array("enabled" => true))->all();
            $publications = SubscriptionPublications()
                ->filter(array("date__gt" => $last_dispatch_date, "enabled" => true))
                ->all();

            if ($users && $publications) {
                $mail_body = new View("subscription/mail-subscribe", array("publications" => $publications));
                $sent_publications_titles = "";
                $sent_emails = "";

                foreach ($publications as $publications) {
                    $sent_publications_titles .= $publications->title . "<br>";
                }

                foreach ($users as $user) {
                    $mail_body = str_replace("user_unsubscribe_code", $user->deactivation_link, $mail_body);

                    $mail = PhpMailerLibrary::create();
                    $mail->AddAddress($user->email);
                    $mail->Subject = strftime("Рассылка от %d.%m.%Y");
                    $mail->Body = $mail_body;
                    $mail->IsHTML(true);
                    $mail->Send();

                    $sent_emails .= $user->email . "<br>";
                }

                $this->emails = $sent_emails;
                $this->publications = $sent_publications_titles;
                $this->hiddenSave();
            }
        }
    }

}
