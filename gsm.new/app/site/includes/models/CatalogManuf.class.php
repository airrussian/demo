<?

class CatalogManuf extends NamiSortableModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 255, 'default' => 'Новый производитель', 'index' => true, 'localized' => false,)),
            'name' => new NamiCharDbField(array('maxlength' => 500, 'index' => true, 'localized' => false,)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true, 'localized' => false,)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Название'),
        'model' => array('title' => 'Модель', 'widget' => 'select'),
    );

    function beforeSave() {
        // Приводим в порядок name страницы
        if (!Meta::isPathName($this->name)) {
            // Попробуем сначала довести до ума исходный name
            if ($this->name) {
                $this->name = Meta::getPathName($this->name);
            }

            // Не получилось — сделаем на основе title
            if (!Meta::isPathName($this->name)) {
                $this->name = Meta::getPathName($this->title);
            }
        }
    }

}
