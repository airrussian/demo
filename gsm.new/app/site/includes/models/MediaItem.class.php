<?

//фотка или видос или еще чего угодно
class MediaItem extends NamiSortableModel {

    static function definition() {
        return array(
            'album' => new NamiFkDbField(array('model' => 'MediaAlbum')),
            'title' => new NamiCharDbField(array('maxlength' => 250)),
            'is_video' => new NamiBoolDbField(array('default' => false, 'index' => true)),
            'image' => new NamiImageDbField(array(
                'path' => "/static/uploaded/images/media/items",
                'variants' => array(
                    'fancy' => array('width' => 1280, 'height' => 800, 'enlarge' => false),
//                    'preview' => array('width' => 360, 'height' => 225, 'crop' => true),
                    'cms' => array('width' => 120, 'height' => 120, 'crop' => true),
                ))
            ),
            'video_code' => new NamiTextDbField(),
            'video_image' => new NamiImageDbField(array('path' => "/static/uploaded/images/video_previews",
                'variants' => array(
//                    'preview' => array('width' => 360, 'height' => 225, 'crop' => true),
                    'cms' => array('width' => 120, 'height' => 120, 'crop' => true),
                ))
            ),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Заголовок'),
        'image' => array('title' => 'Изображение', 'info' => 'рекомендуемый размер 1280x800 пикс.'),
        'video_code' => array('title' => 'Код видео'),
        'video_image' => array('title' => 'Превью изображение', 'info' => 'рекомендуемый размер 360x225 пикс.'),
    );

    function afterSave() {
        $images_count = MediaItems()
                ->filter(array(
                    "album" => $this->album,
                    "enabled" => true,
                    "is_video" => false,
                ))
                ->count();

        $videos_count = MediaItems()
                ->filter(array(
                    "album" => $this->album,
                    "enabled" => true,
                    "is_video" => true,
                ))
                ->count();

        $this->album->images_count = $images_count;
        $this->album->videos_count = $videos_count;
        $this->album->hiddenSave();
    }

    function beforeDelete() {
        $images_count = MediaItems()
                ->filter(array(
                    "album" => $this->album,
                    "enabled" => true,
                    "is_video" => false,
                    "id__ne" => $this->id,
                ))
                ->count();

        $videos_count = MediaItems()
                ->filter(array(
                    "album" => $this->album,
                    "enabled" => true,
                    "is_video" => true,
                    "id__ne" => $this->id,
                ))
                ->count();

        $this->album->images_count = $images_count;
        $this->album->videos_count = $videos_count;
        $this->album->hiddenSave();
    }

}
