<?php

/**
 * Класс для работы с типами цен
 * Сингелтон
 */
class PriceType {

    static $type = array(
        'm_opt' => array(
            'title' => 'Мелкооптовые',
            'title_one' => 'Мелкооптовая',
            'title_socr' => 'м.опт.',
            'fieldname' => 'price_m_opt'),
        'opt' => array(
            'title' => 'Оптовые',
            'title_one' => 'Оптовая',
            'title_socr' => 'опт.',
            'fieldname' => 'price_opt'),
        'rozn' => array(
            'title' => 'Розничные',
            'title_one' => 'Розничная',
            'title_socr' => 'розн.',
            'fieldname' => 'price_rozn')
    );

    static public function type() {
        $user = SiteSession::getInstance()->getUser();
        return !is_null($user) ? $user->type : "rozn";
    }

    static public function getTitle($prefix = "", $type = false) {
        $type = $type ? $type : self::type();
        return isset(self::$type[$type]) ? self::$type[$type]['title' . $prefix] : false;
    }

    static public function getPrice($item, $type = false) {
        $type = $type ? $type : self::type();        
        return $item->{self::$type[$type]['fieldname']} ? $item->{self::$type[$type]['fieldname']} : 0;
    }

}
