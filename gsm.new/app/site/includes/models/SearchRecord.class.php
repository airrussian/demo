<?

class SearchRecord extends NamiModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 2000, 'index' => true, 'fulltext' => true, 'localized' => true)),
            'text' => new NamiTextDbField(array('fulltext' => true, 'localized' => true)),
            'uri' => new NamiCharDbField(array('maxlength' => 500, 'index' => true)),
            'model_name' => new NamiCharDbField(array('maxlength' => 500, 'index' => true)),
            'element_id' => new NamiCharDbField(array('maxlength' => 500, 'index' => true)),
            'enabled' => new NamiBoolDbField(array('default' => 0, 'index' => 'nav')),
            'updated' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y', 'index' => true)),
        );
    }

    function beforeSave() {
        //Моделька pages имеет особенности дублироваться в поиске. Да и вообще, проверяем дубликаты.
        $search_records = SearchRecords(array('model_name' => $this->model_name, 'element_id' => $this->element_id))->all();
        if ($search_records) {
            foreach ($search_records as $search_record) {
                $search_record->delete();
            }
        }
        $this->text = strip_tags($this->text);
    }

    static $model_names = array(
        'Page' => 'Страницы сайта',
        'PhotoAlbum' => 'Фотогаллерея',
        'Service' => 'Услуги',
        'Article' => 'Полезная информация',
        'Publication' => 'Публикации',
    );

    /**
     *   Возвращает текст с подсвеченными найденными словами.
     *   $search_text - то, что искали
     */
    function matching_text($search_text, $title = false) {
        static $form_cache = array();

        if (array_key_exists($search_text, $form_cache)) {
            $any_word = $form_cache[$search_text];
        } else {
            $processor = new NamiFulltextProcessor();
            $words = array();
            foreach ($processor->get_all_forms($search_text) as $k => $w) {
                if ($w) {
                    $words = array_merge($words, $w);
                }
                $words[] = $k;
            }
            $any_word = join('|', array_map('preg_quote', $words));
            $form_cache[$search_text] = $any_word;
        }

        if ($title) {
            $text = $this->title;
        } else {
            $text = $this->text;
        }

        // Выберем максимально длинный набор предложений с искомыми словами, от начала до конца
        if (preg_match("~[^\.\!\?]*?(?:{$any_word}).*?[$\.\!\?]~siu", $this->text, $matches)) {
            $text = $matches[0];
        } else {
            return mb_substr($text, 0, 250);
        }

        // Выделим все вхождения искомых слов (в том числе как частей большего слова)
        $text = preg_replace("~(\S*(?:{$any_word})\S*)~uis", ' <em><strong>$1</strong></em> ', $text);

        return $text;
    }

}
