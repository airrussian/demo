<?

//пбликация для подписки
class SubscriptionPublication extends NamiModel {

    static function definition() {
        return array(
            'title' => new NamiCharDbField(array('maxlength' => 250, 'null' => false)),
            'text' => new NamiTextDbField(),
            'date' => new NamiDatetimeDbField(array('default_callback' => 'return time();', 'format' => '%d.%m.%Y %H:%M:%S', 'index' => true, 'null' => false)),
            'is_sent' => new NamiBoolDbField(array('default' => false, 'index' => true)),
            'enabled' => new NamiBoolDbField(array('default' => true, 'index' => true)),
        );
    }

    public $description = array(
        'title' => array('title' => 'Заголовок'),
        'text' => array('title' => 'Описание', 'widget' => 'richtext'),
        'date' => array('title' => 'Дата'),
    );

}
