<?php

class SCityLink extends NamiModel {

   static function definition() {
        return array(            
            'city' => new NamiFkDbField(array('model' => 'ContactCity')),
            'page' => new NamiFkDbField(array('model' => 'Page')),            
            'news' => new NamiFkDbField(array('model' => 'NewsItem')),
            'banner' => new NamiFkDbField(array('model' => 'Banner')),
        );
    }
    
}