<?
/**
*   Страница вывода блога
*/
class BlogApplication extends UriConfApplication {
    static $comments_per_page = 10;

    protected $uriconf = array(
        array('/',                      'index'),
        array('/page\d+/?',             'index'),
        array('/:id.html',              'post'),
        array('/:id.html/page\d+',      'post'),
        array('/comments/:id',          'comment_redirect'),
        array('/tags/:id',              'tag'),
        array('/tags/:id/page\d+',      'tag'),
        array('/authors/:id',           'author'),
        array('/authors/:id/page\d+',   'author'),
    );
    
    function index($vars, $page) {
        $blog = Blogs()->get_or_404(array('page_uri' => $page->uri, 'enabled' => true));
        
        $entry_query = $blog->entries
        ->filter(array('enabled' => true))
        ->follow(3)
        ->orderDesc('date');
        
        $paginator = new NamiPaginator($entry_query, 'site-paginator', 10);
    
        print new View('blogs/blog', compact('page', 'blog', 'paginator'));
    }
    
    function post($vars, $page) {
        $blog = Blogs()->get_or_404(array('page_uri' => $page->uri, 'enabled' => true));
        $post = BlogPosts()->get_or_404(array('blog' => $blog, 'enabled' => true, 'id' => $vars->id));
        
        $comment_form = BlogsApplication::comment_form(compact('post'));
        
        $paginator = new NamiPaginator($post->entry_query, 'site-paginator', self::$comments_per_page);
    
        print new View('blogs/post', compact('page', 'blog', 'post', 'paginator', 'comment_form'));
    }
    
    /**
    *   Редирект на страницу комментария
    */
    function comment_redirect($vars, $page) {
        $comment = BlogComments()
        ->filter(array(
            'id'                    => $vars->id,
            'enabled'               => true,
            'post__enabled'         => true,
            'post__blog__enabled'   => true,
            'post__blog__page_uri'  => $page->uri,
        ))
        ->first();
        
        if (!$comment) {
            throw new Http404;
        }
        
        // Посчитаем сколько комментариев в посте перед указанным и определим страницу с нужным комментом
        $comments_before = $comment->post->entry_query->filter(array('id__le' => $comment->id))->count();
        $page = ceil($comments_before / self::$comments_per_page);
        
        // Формируем адрес                
        $uri = sprintf('%s%s#comment%d',
            $comment->post->uri,
            $page > 1 ? "/page{$page}/" : '',
            $comment->id
        );

        // Уиииииии
        throw new HttpRedirect($uri, false);
    }
    
    /**
    *   Показ страницы сообщений по выбранному тегу
    */
    function tag($vars, $page) {
        $blog = Blogs()->get_or_404(array('page_uri' => $page->uri, 'enabled' => true));
        $tag = BlogTags()->get_or_404($vars->id);
        
        $post_ids = BlogPostTags()
        ->filter(array(
            'tag'           => $tag,
            'post__blog'    => $blog,
        ))
        ->values('post');
        
        $entry_query = $blog->entries
        ->filter(array(
            'enabled'   => true,
            'id__in'    => $post_ids,
        ))
        ->follow(3)
        ->orderDesc('date');
        
        $paginator = new NamiPaginator($entry_query, 'site-paginator', 10);
    
        print new View('blogs/blog', compact('page', 'blog', 'paginator', 'tag'));
    }

    /**
    *   Показ страницы сообщений по выбранному тегу
    */
    function author($vars, $page) {
        $blog = Blogs()->get_or_404(array('page_uri' => $page->uri, 'enabled' => true));
        $author = SiteUsers()->get_or_404($vars->id);
        
        $entry_query = $blog->entries
        ->filter(array(
            'enabled'   => true,
            'user'      => $author,
        ))
        ->follow(3)
        ->orderDesc('date');
        
        $paginator = new NamiPaginator($entry_query, 'site-paginator', 10);
    
        print new View('blogs/blog', compact('page', 'blog', 'paginator', 'author'));
    }

}