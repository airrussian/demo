<?

class ContactsApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
        array('~^/(.+?)/?$~', 'index')        
    );

    function index($vars, $page) {

        $contact = ContactCity::GetCurrent();

        $storagies = ContactStoragies()
                ->filter(array(
                    'city' => $contact->id,
                    'enabled' => true,
                    'system' => false                    
                ))
                ->sortedOrder()
                ->all();
        

        print new View('contacts/page-index', compact('contact', 'storagies', 'page'));
        return true;
    }

}
