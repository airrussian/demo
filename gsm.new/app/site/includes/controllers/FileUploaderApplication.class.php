<?

class FileUploaderApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
        array('~^/get-identify/?$~', 'get_identify'),
    );

    function index($vars, $page) {
        header('Content-Type: application/json');

        $path_files = '/static/uploaded/temp/files/';
        $path_tmp = md5($_FILES['file']['name'] . time());

        $filename = $_FILES['file']['name'];

        mkdir($_SERVER['DOCUMENT_ROOT'] . $path_files . mb_substr($path_tmp, 0, 2) . "/" . $path_tmp, 0777, true);
        move_uploaded_file(
            $_FILES['file']['tmp_name'],
            $_SERVER['DOCUMENT_ROOT'] . $path_files . mb_substr($path_tmp, 0, 2) . "/" . $path_tmp . "/" . $filename
        );

        echo json_encode([
            'filename' => $filename,
            'key' => $path_tmp
        ]);

        return true;
    }

    function get_identify($vars, $page) {
        $success = false;
        $message = "ok";
        $identify = null;

        try {
            $new_file = FeedbackTempFiles()->create(array());                
            if ($new_file) {
                $success = true;
                $identify = $new_file->identify;                
            }
        } catch (Exception $exc) {
            $message = $exc->getMessage() . "какого?";
        }


        header('Content-Type: application/json');
        echo json_encode(array(
            "success" => $success,
            "message" => $message,
            "identify" => $identify,
        ));

        return true;
    }

}
