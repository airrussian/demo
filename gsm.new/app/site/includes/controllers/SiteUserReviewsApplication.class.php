<?
class SiteUserReviewsApplication extends UriConfApplication {
    protected $uriconf = array(
        array('~^/(?P<user_id>\d+)(?:/page\d+)?/?$~',   'items'),
        array('~^/success/?~',                          'success'),
    );
    
    static private $items_per_page = 10;
    
    function items($vars, $page) {
        $user = SiteUsers()->get_or_404($vars->user_id);
        
        $items_qs = SiteUserReviews()
        ->filter(array(
            'enabled' => true,
            'user'    => $user
        ))
        ->orderDesc('date');
        
        $errors = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && Meta::vars('userreview-post-form')) {
            $validator = new DataValidator(
                array(
                    'text'  => 'trim required',
                    'mood'  => array('trim', 'optional', array('inarray', SiteUserReview::$moods)),
                ),
                array()
            );
            try {
                $vars = & Meta::vars();
                $status = $validator->process($vars, true);
                if ($status->ok) {
                    $post_data = $status->data;
                    $post_data['user'] = SiteSession::getInstance()->getUser();
                    $post_data['mood'] = $post_data['mood'] ? $post_data['mood'] : SiteUserReview::$moods[0];
                    $post_data['enabled'] = SiteSession::getInstance()->getUser()->author ? true : false;
    
                    // создадим запись
                    SiteUserReviews()->create($post_data);
                                    
                    // перекинем на страницу success
                    throw new HttpRedirect("{$page->uri}/success/?id={$post_data['user']->id}", false);
                } else {
                    $errors = array_merge($errors, $status->errors);
                }
            } catch (HttpRedirect $e) {
                throw $e;
            } catch (Exception $e) {
                $errors['__form__'] = $e->getMessage();
            }
        }         
        $paginator = new NamiPaginator(
            $items_qs,
            'site-paginator',
            self::$items_per_page
        );
        print new View('siteuser_reviews/items', compact(
            'page',
            'user',
            'paginator',
            'errors'
        ));
        return true;
    }
    
    function success($vars, $page) {
        print new View('siteuser_reviews/success', compact('page'));
        return true;
    }
    
}