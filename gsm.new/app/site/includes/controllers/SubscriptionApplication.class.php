<?

class SubscriptionApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~/deactivation/(?P<unsubscribe_hash>[\w+\d+]+)~', 'deactivation'),
        array('~/activation/(?P<activation_hash>[\w+\d+]+)~', 'activation'),
        array('~/?~', 'index'),
    );

    /**
     *   Форма подписки на рассылку и ее обработка
     */
    function index($vars, $page) {
        if (Meta::isAjaxRequest()) {
            $error_msg = "";
            $success = false;

            $user_email = Meta::vars("subscribe_email");

            if ($user_email) {
                if (IsEmailLibrary::check($user_email)) {

                    if (SubscriptionUsers()->get(array("email" => $user_email))) {
                        $success = false;
                        $error_msg = "Данный адрес уже находится в списке рассылки";
                    } else {
                        $new_user = SubscriptionUsers()->create(array("email" => $user_email));

                        $mail = PhpMailerLibrary::create();
                        $mail->AddAddress($new_user->email);
                        $mail->Subject = "Активация подписки";
                        $mail->Body = new View('subscription/mail-activation', array('activation_link' => $new_user->activation_link));
                        $mail->IsHTML(true);
                        $mail->Send();

                        $success = true;
                    }
                } else {
                    $error_msg = "Укажите правильный адрес электронной почты";
                }
            } else {
                $error_msg = "Укажите адрес электронной почты";
            }

            echo json_encode(array(
                "error_msg" => $error_msg,
                "success" => $success
            ));
            return true;
        } else {
            //тут можно дублировать форму.
            Builder::show404();
        }
    }

    /**
     *   Отписка от рассылки
     */
    function deactivation($vars, $page) {
        $user = SubscriptionUsers(array('code' => $vars->unsubscribe_hash))->first();

        if ($user) {
            $message = "Электронная почта {$user->email} была удалена из списка рассылки";
            $user->delete();
        } else {
            $message = "Код рассылки не найден";
        }

        print new View('subscription/page-system_msg', compact('message', 'page'));
    }

    /**
     * активация e-mail подписчика по ссылке,
     * которая пришла ему по почте
     */
    function activation($vars, $page) {
        $user = SubscriptionUsers()
            ->filter(array('code' => $vars->activation_hash))
            ->only('email id enabled code')
            ->first();

        $message = 'Вы прошли по неверной ссылке, либо подписчика с такими данными еще не существует';

        if ($user) {
            if ($user->enabled == false) {
                $user->enabled = 1;
                $user->save();
                $message = 'Спасибо за подписку, теперь Вам будут приходить новости на Ваш e-mail';
            } else {
                $message = 'Вы прошли по неверной ссылке, либо ваш e-mail уже активирован';
            }
        }

        print new View('subscription/page-system_msg', compact('message', 'page'));
    }

}