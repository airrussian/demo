<?

class MediaApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
        array('~^/(?P<category_name>[-\w\.]+)/?(?:/page\d+)?/?$~', 'category'),
        array('~^/(?P<category_name>[-\w\.]+)/(?P<album_id>[\d]+)?/?(?:/page\d+)?/?$~', 'album'),
    );

    function index($vars, $page) {
        $categories = MediaCategories()
            ->filter(array(
                "enabled" => true,
            ))
            ->sortedOrder()
            ->all();

        print new View('media/page-index', compact('page', 'paginator', 'categories'));
        return true;
    }

    function category($vars, $page) {
        $category = MediaCategories()
            ->get_or_404(array(
            'enabled' => true,
            "name" => $vars->category_name,
        ));

        $albums = MediaAlbums()
            ->filter(array(
                "enabled" => true,
                "category" => $category,
            ))
            ->order("date");


        $crumbs_pages = array(
            array(
                "uri" => $page->uri,
                "title" => $page->title,
            )
        );
        $page->title = $category->title;


        $paginator = new NamiPaginator($albums, '_blocks/site-paginator', Config::get('albums.items_per_page'));
        print new View('media/page-category', compact(
                'page'
                , 'paginator'
                , 'category'
                , 'crumbs_pages'
        ));
        return true;
    }

    function album($vars, $page) {
        $album = MediaAlbums()
            ->get_or_404(array(
            "category__enabled" => true,
            "category__name" => $vars->category_name,
            "enabled" => true,
            "id" => $vars->album_id,
        ));

        $items = MediaItems()
            ->filter(array(
            "enabled" => true,
            "album" => $album,
        ));


        $has_video = $items->filter(array(
                "is_video" => true,
            ))
            ->first();

        $has_images = $items->filter(array(
                "is_video" => false,
            ))
            ->first();

        $has_filter = !!$has_images && !!$has_video;


        switch (Meta::vars("only")) {
            case "images":
                $items = $items
                    ->filter(array(
                    "is_video" => false
                ));

                break;

            case "video":
                $items = $items
                    ->filter(array(
                    "is_video" => true
                ));

                break;
        }

        $items = $items->sortedOrder();
        $paginator = new NamiPaginator($items, '_blocks/site-paginator', Config::get('images.items_per_page'));


        $crumbs_pages = array(
            array(
                "uri" => $page->uri,
                "title" => $page->title,
            ),
            array(
                "uri" => $album->category->full_uri,
                "title" => $album->category->title,
            ),
        );
        $page->title = $album->title;


        print new View('media/page-album', compact(
                'page'
                , 'paginator'
                , 'crumbs_pages'
                , 'album'
                , 'items'
                , 'has_filter'
        ));
        return true;
    }

}
