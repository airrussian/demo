<?

class LikesApplication extends UriConfApplication {

    // за кого можно голосовать
    static private $models = array(
        'photo' => 'ServiceItemPhoto',
        'user' => 'SiteUser',
    );
    protected $uriconf = array(
        array('~^/?$~', 'index'),
    );

    /**
     * Лайк (повышение рейтинга объекта)
     * @return json
     */
    function index() {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            throw new Http404();
        }

        try {
            $item_type = Meta::vars('type');
            $item_id = (int) Meta::vars('id');
            if (!$item_id || !$item_type) {
                throw new Exception("Ошибка голосования");
            }

            $model_name = array_key_exists($item_type, self::$models) ? self::$models[$item_type] : null;
            if (!$model_name) {
                throw new Exception(($item_type == "photo") ? "Данная работа недоступна для голосования" : "Данный пользователь недоступен для голосования");
            }

            // если есть кука, то сначала проверим данные по ней, без запроса к бд
            if (isset($_COOKIE['liked'])) {
                $liked_items = unserialize($_COOKIE['liked']);
            } else {
                $liked_items = array();
                foreach (self::$models as $model_name_ => $model_class_name) {
                    $liked_items[$model_name_] = array();
                }
            }

            if (in_array($item_id, $liked_items[$item_type])) {
                throw new Exception("Вы уже голосовали");
            }

            $item = NamiQuerySet($model_name)->get(array('id' => $item_id));
            if (!$item) {
                throw new Exception(($item_type == "photo") ? "Данная работа недоступна для голосования" : "Данный пользователь недоступен для голосования");
            }

            // Проверим, что с этого адреса еще не голосовали
            if (self::ip_liked($item->id, $model_name)) {
                throw new Exception("Вы уже голосовали");
            }


            $remote_addr = array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : '';
            $forwarded = array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
            $forwarded = substr($forwarded, 0, LikeLogItem::$forwarded_length);
            $host = array_key_exists('HTTP_HOST', $_SERVER) ? $_SERVER['HTTP_HOST'] : '';
            $referrer = array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '';
            $user_agent = array_key_exists('HTTP_USER_AGENT', $_SERVER) ? $_SERVER['HTTP_USER_AGENT'] : '';

            LikeLogItems()->create(array(
                'item_id' => $item->id,
                'model_name' => $model_name,
                'remote_addr' => $remote_addr,
                'forwarded' => $forwarded,
                'host' => $host,
                'referrer' => $referrer,
                'user_agent' => $user_agent,
            ));
            $item->rate += 1;
            $item->hiddenSave();

            // записываем в куки id объекта за который проголосовал пользователь
            $liked_items[$item_type][] = $item_id;
            setcookie('liked', serialize($liked_items), null);


            // когда все прошло успешно возвращаем новый рейтинг объекта
            // за время между открытием страницы и голосованием он мог измениться
            $result = array(
                'success' => true,
                'msg' => "Спасибо! Ваш голос принят",
                'count' => $item->rate,
            );
        } catch (Exception $exc) {

            $result = array(
                'success' => false,
                'msg' => $exc->getMessage(),
                'count' => 0
            );
        }

        header('Content-Type: application/json');
        echo json_encode($result);
        return true;
    }

    /**
     * Проверяет, голосовали ли с одного и того же ip
     * @param int $item_id идентификатор объекта
     * @param string $model_name навание модели объекта
     * @return bool
     */
    static function ip_liked($item_id = null, $model_name = null) {
        if (is_null($item_id) || is_null($model_name)) {
            return false;
        }
        $remote_addr = array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : '';
        $forwarded = array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
        $forwarded = substr($forwarded, 0, LikeLogItem::$forwarded_length);

        // эффективнее проверять сортируя по дате
        // (кто-то проголосовал, почистил куки и снова голосует)
        $liked = LikeLogItems()
                        ->filter(array(
                            'item_id' => $item_id,
                            'model_name' => $model_name,
                            'remote_addr' => $remote_addr,
                            'forwarded' => $forwarded)
                        )
                        ->orderDesc('date')
                        ->values('id', 1) ? true : false;
        return $liked;
    }

}
