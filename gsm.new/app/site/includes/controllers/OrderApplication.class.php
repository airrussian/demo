<?

//корзина
//оформление заказа на ajax
class OrderApplication extends UriConfApplication {

    protected $uriconf = array(
        array("~^/ya/check/?~", 'yandex_check'),
        array("~^/ya/aviso/?~", 'yandex_aviso'),
        array("~^/ya/success/?~", 'yandex_success'),
        array("~^/ya/fail/?~", "yandex_fail"),
        array('~^/ajax_cart/(?P<action>\w+)?/?~', 'ajax_cart_actions'),
        array('~^/pending/ajax_cart/(?P<action>\w+)?/?$~', 'ajax_pending_actions'),
        array('~^/pending/?$~', 'pending'),
        array('~^/checkout/?$~', 'order_checkout'),
        array('~^/send_to_email/(?P<order_id>\d+)?/?~', 'send_to_email'),
        array('~^/complete/?$~', 'order_complete'),
        array('~^/?~', 'order_page'),
    );

    function ajax_cart_actions($vars, $page) {
        if (!Meta::isAjaxRequest()) {
            Builder::show404();
        }

        $cart = Cart::getInstance();
        $item = Meta::vars("data");
        $result = array();

        switch ($vars->action) {
            case "add":
                $cart->add_item($item);
                $result = $cart->get_items();
                break;

            case "update":
                $cart->update_item($item);
                $result = $cart->get_items();
                break;

            case "update_many":
                $cart->update_many($item);
                $result = $cart->get_items();
                break;

            case "remove":
                $cart->remove_item($item);
                $result = $cart->get_items();
                break;

//            case "full_info":
//                $result = $this->get_full_info();
//                break;

            case "clear":
                $cart->clear_cart();
                break;

            default :
                $result = $cart->get_items();
                break;
        }

        header('Content-Type: application/json');
        echo json_encode($result);
        return true;
    }

    public function ajax_pending_actions($vars, $page) {
        if (!Meta::isAjaxRequest()) {
            Builder::show404();
        }

        $pending = Pending::getInstance();
        $item = Meta::vars("data");
        $result = array();

        switch ($vars->action) {
            case "add":
                $pending->add_item($item);
                $result = $pending->get_items();
                break;

            case "update":
                $pending->update_item($item);
                $result = $pending->get_items();
                break;

            case "update_many":
                $pending->update_many($item);
                $result = $pending->get_items();
                break;

            case "remove":
                $pending->remove_item($item);
                $result = $pending->get_items();
                break;

//            case "full_info":
//                $result = $this->get_full_info();
//                break;

            case "clear":
                $pending->clear_cart();
                break;

            default :
                $result = $pending->get_items();
                break;
        }

        header('Content-Type: application/json');
        echo json_encode($result);
        return true;
    }

    function ajax_checkout($vars, $page) {

        $validator_rules = array(
            'name' => array('trim', 'required', array('length', 1, 100)),
            'phone' => array('trim', 'required', 'phone', array('length', 1, 100)),
            'email' => array('trim', 'required', 'email', array('length', 1, 100)),
            'delivery' => array('trim', 'required'),
            'pay' => array('trim', 'required'),
        );
        $validator_messages = array(
            'delivery' => 'Укажите способ доставки',
            'pay' => 'Выберите способ оплаты',
        );

        switch (Meta::vars('delivery')) {
            case 'cour':
                $validator_rules['cour-city'] = array('trim', 'required', array('length', 1, 255));
                $validator_rules['cour-address'] = array('trim', 'required', array('length', 1, 255));
                $validator_messages['cour-city'] = "Выберите город";
                $validator_messages['cour-address'] = "Укажите адрес";
                break;
            case 'post':
                $validator_rules['index'] = array('trim', 'required', array('length', 6, 6));
                $validator_rules['post-city'] = array('trim', 'required', array('length', 1, 255));
                $validator_rules['post-address'] = array('trim', 'required', array('length', 1, 255));
                $validator_messages['index'] = "Укажите почтовый индекс";
                $validator_messages['nrgtk-address'] = "Укажите адрес";
                $validator_messages['nrgtk-city'] = "Выберите город";
                break;
            case 'nrgtk':
                $validator_rules['nrgtk-city'] = array('trim', 'required', array('length', 1, 255));
                $validator_rules['nrgtk-address'] = array('trim', 'required', array('length', 1, 255));
                $validator_messages['nrgtk-city'] = "Выберите город";
                $validator_messages['nrgtk-address'] = "Укажите адрес";
                break;
            case 'other':
                $validator_rules['other_trans_company'] = array('trim', 'required', array('length', 1, 1000));
                $validator_messages['other_trans_company'] = 'Необходимо указать какой транспортной компанией будет осуществлятся доставка';
                break;
            case 'pickup':
                $validator_rules['pickup-address'] = ['trim', 'required', ['length', 1, 1000]];
                $validator_messages['pickup-address'] = "Необходимо выбрать адрес самовывоза";
                break;
        }

        $validator = new DataValidator($validator_rules, $validator_messages);
        $status = $validator->process(Meta::vars());
        $msg = "";

        $delivery_code = '0';
        $delivery_price = 0;

        $curCity = ContactCity::GetCurrent();

        if ($status->ok) {

            switch ($status->data['delivery']) {
                case 'cour':
                    $delivery_type = "Курьерская доставка";
                    $delivery_code = SiteOrderDeliveryMethod::getCode($status->data['delivery'], $status->data['cour-city']);
                    $delivery_price = SiteOrderDeliveryMethod::getPrice(Cart::getInstance()->get_total_price(), $status->data['delivery'], $status->data['cour-city']);
                    $city = ContactCity::GetCurrent()->prefix;
                    $address = ContactCity::GetNameByPrefix($status->data['cour-city']) . ", " . $status->data['cour-address'];
                    break;

                case 'post':
                    $delivery_type = "Доставка почтой";
                    $city = ContactCity::GetCurrent()->prefix;
                    $delivery_code = SiteOrderDeliveryMethod::getCode($status->data['delivery'], $city);
                    $delivery_code = $delivery_code ? $delivery_code : "00007558";
                    $delivery_price = SiteOrderDeliveryMethod::getPrice(Cart::getInstance()->get_total_price(), $status->data['delivery'], $city);
                    $address = $status->data['index'] . ", " . $status->data['post-city'] . ", " . $status->data['post-address'];
                    break;

                case 'pickup':
                    $delivery_type = "Самовывоз";
                    list($delivery_code, $city, $address) = explode("|", $status->data['pickup-address']);
                    $delivery_price = 0;
                    $address = $city . ", " . $address;
                    $city = ContactCity::GetByName($city)->prefix;
                    break;

                case 'nrgtk':
                    $delivery_type = "Транспортной компанией Энергия NRG-TK.RU";
                    $city = ContactCity::GetCurrent()->prefix;
                    $address = $status->data['nrgtk-city'] . ", " . $status->data['nrgtk-address'];
                    break;

                case 'other':
                    $delivery_type = $status->data['other_trans_company'];
                    $city = ContactCity::GetCurrent()->prefix;
                    $address = "";
                    break;
            }

            $storage = Meta::vars('storage') ? Meta::vars('storage') : 'main';

            $cart = Cart::getInstance();

            $user = SiteSession::getInstance()->getUser();
            // Если пользователь не авторизован, тогда
            if (!$user) {
                // Проверяем есть ли такой пользователь с указанным email в базе данных
                $user = SiteUsers()->get(array('email' => $status->data['email']));
                // Если пользователь с таким email есть предлагаем авторизоваться
                if ($user) {
                    $msg = "Пользователь с таким email существует, вам необходимо авторизоваться для оформления заказа";
                    $status->ok = false;
                    return json_encode(array($status, $msg));
                }
                // Если пользователя такого не оказалась в базе, значит можно его зарегистировать
                // и оформить заказ от нового зарегистированного и отправить на почту сообщение об этом.
                $login = SiteUser::genLogin($status->data['email']);
                $pass = SiteUser::genPass();

                $user = SiteUsers()->create(array(
                    'name' => $status->data['name'] ? $status->data['name'] : $login,
                    'login' => $login,
                    'email' => $status->data['email'],
                    'phone' => $status->data['phone'],
                    'password' => md5($pass),
                    'text' => 'Сгенерированный пароль: ' . $pass,
                    'enabled' => true,
                    'city' => $city,
                ));

                self::send(
                    $user->email,
                    "Учетная запись на сайте: {$_SERVER['HTTP_HOST']}",
                    (string) new View("_mails/new_user-autoreg", compact('user', 'city', 'pass')),
                    $curCity->order_sendFromEmailArray                    
                );

                SiteSession::getInstance()->auth($login, $pass);
                setcookie('newuser', '1', time() + 1000, '/');
            }

            $pay_type = SiteOrderDeliveryPayMethods()->get(["name" => $status->data['pay']])->short_title;

            $order_array = array(
                'name' => $status->data['name'],
                'email' => $status->data['email'],
                'phone' => $status->data['phone'],
                'user' => $user,
                'city' => $city,
                'storage' => $storage,
                'comment' => $status->data['comment'],
                'pay_type' => $pay_type,
                'delivery_type' => $delivery_type,
                'delivery_code' => $delivery_code,
                'delivery_price' => $delivery_price,
                'address' => $address,
                'items_price' => Cart::getInstance()->get_total_price(),
                'total_price' => Cart::getInstance()->get_total_price() + $delivery_price
            );

            $order = SiteOrders()->create($order_array);

            $items = OrderApplication::cart_items_list($storage, $cart);

            $q_total = 0; // Общее количество позиций в заказе            
            $total_price = 0;

            foreach ($items as $item) {
                $item_price = 0;
                $entry = CatalogEntries()->get(array('id' => $item['entry']));
                
                $price = PriceType::getPrice($entry);

                SiteOrderItems()->create(array(
                    'order' => $order,
                    'entry' => $item['entry'],
                    'qty' => $item['qty'],
                    'oneC_id' => $entry->oneC_id,
                    'price' => $price
                ));
                $item_price = $item['qty'] * $price;

                $total_price += $item_price;
                $q_total += $item['qty'];
            }

            // Сохраняем количество позиций в заказе
            $order->items_price = $total_price;
            $order->total_price = $total_price + $delivery_price;
            $order->qty = $q_total;
            $order->save();

            // Если у пользователя стоит галочка, что бы ему отправлялись заказы тоже
            if ((!$user) || ($user->subscribe_order_status)) {
                // Есть уже AJAX функция отправки заказа, что бы не дублировать код, сделать такую хитрость
                $_vars = new stdClass();
                $_vars->order_id = $order->id;
                $_vars->noprint = true;
                $this->send_to_email($_vars, $page);
            }

            // Отправка письма админу
            $city = ContactCity::GetCurrent();

            $emailTo = $curCity->order_sendToEmail ?  $curCity->order_sendToEmail : false;

            self::send(
                    $emailTo,
                    "Заказ на сайте {$_SERVER['HTTP_HOST']}",
                    (string) new View('_mails/new_order', compact("order", "items"))
            );

            $cart->clear_cart($storage);

            // Куку ставим, что бы юзер мог видеть свой заказ и не кто другой...
            setcookie("order", $order->id, time() + 10000, '/');
        } else {
            $msg = reset($status->errors)->message;
        }

        $json = json_encode(array($status, $msg));
        return $json;
    }

    function send_to_email($vars, $page) {

        $order_id = $vars->order_id;

        $order = SiteOrders()->get(array('id' => $order_id));
        if (is_null($order)) {
            Builder::show403();
        }

        $items = SiteOrderItems()->filter(array('order' => $order))->all();

        $curCity = ContactCity::GetCurrent();

        $domain = $curCity->domain;

        self::send(
            $order->email,
            "Заказ на сайте " . $domain,
            (string)new View('_mails/new_order-user', ["order" => $order, "items" => $items, 'domain' => $domain]),
            $curCity->order_sendFromEmailArray
        );

        // OrdersEmails()->create(['order_id' => $order->id, 'curcity' => $curCity->id, 'enabled' => true]);

        if (!isset($vars->noprint)) {
            print "OK";
        }
        return true;
    }

    function order_page($vars, $page) {
        //запрет на кэширование страницы броузером и прокси
        header("Cache-Control: no-store, no-cache, must-revalidate");
        $storage = Meta::vars('storage') ? Meta::vars('storage') : 'main';
        $cart = Cart::getInstance();
        $items_list = $this->cart_items_list($storage, $cart);
        print new View('order/page-cart', compact('page', 'items_list'));
        return true;
    }

    public function pending($vars, $page) {
        header("Cache-Control: no-store, no-cache, must-revalidate");
        $pending = Pending::getInstance();
        $storage = Meta::vars('storage') ? Meta::vars('storage') : 'main';
        $items_list = $this->cart_items_list($storage, $pending);
        print new View("order/page-pending", compact('items_list', 'page'));
        return true;
    }

    // Оформление заказа
    function order_checkout($vars, $page) {

        if (Meta::isAjaxRequest()) {
            print $this->ajax_checkout($vars, $page);
            return true;
        }

        $storage = Meta::vars('storage') ? Meta::vars('storage') : 'main';

        $cart = Cart::getInstance();
        $items_list = $this->cart_items_list($storage, $cart);

        if (empty($items_list)) {
            self::onpagecart();
        }

        $crumbs_pages[] = array(
            'title' => 'Корзина',
            'uri' => Builder::getAppUri('OrderApplication')
        );

        $page->title = "Оформление заказа";
        print new View('order/page-checkout', compact('page', 'items_list', 'status', 'crumbs_pages'));
        return true;
    }

    function order_complete($vars, $page) {

        if (!isset($_COOKIE['order']) || is_null($_COOKIE['order']) || !is_numeric($_COOKIE['order'])) {
            self::onpagecart();
        }

        $order = SiteOrders()->get(array('id' => $_COOKIE['order']));

        if (is_null($order)) {
            self::onpagecart();
        }

        $items = SiteOrderItems(array('order' => $order))->all();

        if (!isset($_COOKIE['newuser'])) {
            setcookie("newuser", NULL, time() - 10000, '/');
            $user = SiteSession::getInstance()->getUser();
        }

        print new View("order/page-complete", compact("page", 'order', 'items', 'user'));
        return true;
    }

    static function onpagecart() {
        header("Location: http://" . $_SERVER['HTTP_HOST'] . Builder::getAppUri('OrderApplication'));
        exit();
    }

    static function cart_items_list($storage = "main", $cart = null) {
        if (is_null($cart)) {
            $cart = Cart::getInstance();
        }

        $items_list = array();
        $cart_items = $cart->get_items();
        if ($cart_items) {
            // Собирем всё идентификаторы товаров
            $entry_ids = array();
            foreach ($cart_items as $cart_item) {
                $entry_ids[] = $cart_item['id'];
            }
            // Выберим их из базы
            $db_items = CatalogEntries()
                    ->filter(array('id__in' => $entry_ids))
                    ->all();
            // Индексируем типо
            $items = array();
            foreach ($db_items as $db_item) {
                $items[$db_item->id] = $db_item;
            }
            $db_items = null; // Массив более не нужен, удаляем, что бы не жрал память

            foreach ($cart_items as $cart_item) {
                if (isset($items[$cart_item['id']])) {
                    if ($cart_item['storage'] == $storage) {
                        $items_list[] = array(
                            'qty' => $cart_item['qty'],
                            'storage' => $cart_item['storage'],
                            'entry' => $items[$cart_item['id']],
                            'price' => PriceType::getPrice($items[$cart_item['id']])
                        );
                    }
                } else {
                    // Удаляем из корзины, если нету такого товара, но удаляем только для склада.
                    $cart->remove_item(array('id' => $cart_item['id'], 'storage' => $storage));
                }
            }
        }

        return $items_list;
    }

    static $shopId = 586602;
    static $shopPassword = "gsmservicegsm";
    static $scId = 904785;
    static $yaShop = "https://money.yandex.ru/eshop.xml";

    // static $yaShop = "https://demomoney.yandex.ru/eshop.xml"; // http://money.yandex.ru/eshop.xml 

    static function yandex_payform($order, $items) {

        $merchant = array(
            "customerContact" => $order->email ? $order->email : $order->phone,
            "taxSystem" => 2,
            "items" => array(
                array( 
                    'quantity' => 1,
                    'price' => array('amount' => $order->delivery_price ), 
                    'tax' => 3, 
                    'text' => mb_substr( $order->delivery_type, 0, 127 ),
                    "paymentMethodType" => "full_prepayment",
                    "paymentSubjectType" => "service"
                )
            )
        );
        
        foreach ($items as $item) {
            $merchant['items'][] = array(
                "quantity" => $item->qty, 
                "price" => array( "amount" => PriceType::getPrice($item->entry) ),
                "tax" => 3,
                "text" => mb_substr( $item->entry->title, 0, 127 ),
                "paymentMethodType" => "full_prepayment",
                "paymentSubjectType" => "commodity"
            );
        }

        $merchant = preg_replace_callback('/\\\\u(\w{4})/', function ($matches) {
            return html_entity_decode('&#x' . $matches[1] . ';', ENT_COMPAT, 'UTF-8');
        }, json_encode($merchant));


        $return = (string) new View("order/yandex_payform", array(
            "yaShop" => self::$yaShop,
            "shopId" => self::$shopId,
            "scId" => self::$scId,
            "order" => $order,
            "merchant" => $merchant
        ));
               
        return $return;
    }

    function yandex_check($vars, $page) {

        $f = fopen($_SERVER['DOCUMENT_ROOT'] . '/static/uploaded/yandex.txt', 'a+');

        $orderNumber = Meta::vars("orderNumber");
        if ($orderNumber) {
            $order = SiteOrders()->get(array("id" => $orderNumber, "status" => 1));
        }

        $fields = array(
            'action',
            'orderSumAmount',
            'orderSumCurrencyPaycash',
            'orderSumBankPaycash',
            'shopId',
            'invoiceId',
            'customerNumber'
        );

        $hashStr = "";
        foreach ($fields as $field) {
            $hashStr .= Meta::vars($field) . ";";
        }
        $hashStr .= self::$shopPassword;

        $md5 = mb_strtoupper(md5($hashStr));
        $time = date("c");

        fwrite($f, "YANDEX_CHECK. TIME: " . $time);
        fwrite($f, "METAVARS: " . json_encode(Meta::vars()));
        fwrite($f, "orderNumber: " . $orderNumber);
        fwrite($f, "order id: " . $order->id);
        fwrite($f, "Status: " . $order->status);

        if ($md5 == Meta::vars("md5") && ($order)) {
            $xml_out = '<?xml version="1.0" encoding="UTF-8"?>' .
                    '<checkOrderResponse performedDatetime="' . $time . '"' .
                    ' code="0" invoiceId="' . Meta::vars("invoiceId") . '"' .
                    ' shopId="' . self::$shopId . '"/>';
        } else {
            $xml_out = '<?xml version="1.0" encoding="UTF-8"?>' .
                    '<checkOrderResponse performedDatetime="' . $time . '"' .
                    ' code="1" invoiceId="' . Meta::vars("invoiceId") . '"' .
                    ' shopId="' . self::$shopId . '"' .
                    ' message="Указанный заказ не существует"' .
                    ' techMessage="Неверный заказ" />';
        }

        fclose($f);

        header("Content-Type: text/xml");
        echo $xml_out;
        return true;
    }

    function yandex_aviso($vars, $page) {

        $f = fopen($_SERVER['DOCUMENT_ROOT'] . '/static/uploaded/yandex.txt', 'a+');

        $orderNumber = Meta::vars("orderNumber");
        if ($orderNumber) {
            $order = SiteOrders()->get(array("id" => $orderNumber, "status" => 1));
        }

        $fields = array(
            'action',
            'orderSumAmount',
            'orderSumCurrencyPaycash',
            'orderSumBankPaycash',
            'shopId',
            'invoiceId',
            'customerNumber'
        );

        $hashStr = "";
        foreach ($fields as $field) {
            $hashStr .= Meta::vars($field) . ";";
        }
        $hashStr .= self::$shopPassword;

        $md5 = mb_strtoupper(md5($hashStr));
        $time = date("c");

        fwrite($f, "YANDEX_AVISO. TIME: " . $time . "\n");
        fwrite($f, "METAVARS: " . json_encode(Meta::vars()) . "\n");
        fwrite($f, "orderNumber: " . $orderNumber . "\n");
        fwrite($f, "order id: " . $order->id . "\n");
        fwrite($f, "MD5=" . json_encode($md5) . "\n");
        fwrite($f, "Status: " . $order->status . "\n");

        if ($md5 == Meta::vars("md5") && ($order)) {

            // Последний оплаченный онлайн платеж
            $lastOnlineOrder = SiteOrders()->filter(array('status' => 3, 'pay_type' => $order->pay_type))->orderDesc("online_id")->first();
            $order->online_id = $lastOnlineOrder->online_id + 1;

            $order->status = 3;
            $order->lastUpdate = date("Y-m-d H:i");
            $order->save();

            $items = SiteOrderItems()->filter(array("order" => $order))->all();

            $content = (string) new View("_mails/order_pay-success", compact('order', 'items'));

            fwrite($f, "Content: " . $content . "\n");

            $city = ContactCity::GetByPrefix($order->city);

            fwrite($f, "Content: " . json_encode($city) . "\n");

            $xml_out = '<?xml version="1.0" encoding="UTF-8"?>' .
                '<paymentAvisoResponse performedDatetime="' . $time . '"' .
                ' code="0" invoiceId="' . Meta::vars("invoiceId") . '"' .
                ' shopId="' . self::$shopId . '"/>';

            fwrite($f, "xml_out: " . $xml_out . "\n");

            // Пользователю
            try {
                self::send($order->email, "Заказ {$order->id} успешно оплачен", $content, $city->order_sendFromEmailArray);
                fwrite($f, "send " . $order->email . "\n");
            } catch (Exception $exception) {
                fwrite($f, "error send " . json_encode($exception) . "\n");
            }

            // Администрации
            try {
                self::send($city->order_sendToEmail, "Пользователь совершил онлайн оплату, заказа {$order->id}", $content);
                fwrite($f, "send " . $city->order_sendToEmail . "\n");
            } catch (Exception $exception) {
                fwrite($f, "error send " . json_encode($exception) . "\n");
            }

        } else {
            $xml_out = '<?xml version="1.0" encoding="UTF-8"?>' .
                    '<paymentAvisoResponse performedDatetime="' . $time . '"' .
                    ' code="1" invoiceId="' . Meta::vars("invoiceId") . '"' .
                    ' shopId="' . self::$shopId . '"' .
                    ' message="Указанный заказ не существует"' .
                    ' techMessage="Неверный заказ" />';
        }

        fclose($f);

        header("Content-Type: text/xml");
        echo $xml_out;
        return true;
    }

    function yandex_success($vars, $page) {
        $page->title = "Заказ успешно оплачен";

        $orderNumber = Meta::vars("orderNumber");
        $order = SiteOrders()->get_or_404(array("id" => $orderNumber, "status" => 3));
        $items = SiteOrderItems(array('order' => $order))->all();

        setcookie("order", null, time() - 10000, "/");

        print new View("order/page-yandex_success", compact('page', 'order', 'items'));
        return true;
    }

    function yandex_fail($vars, $page) {
        $orderNumber = Meta::vars("orderNumber");
        $order = SiteOrders()->get_or_404(array("id" => $orderNumber));
        $items = SiteOrderItems(array('order' => $order))->all();

        $page->title = "Произошла ошибка при оплате заказа";
        print new View("order/page-yandex_fail", compact('page', 'order', 'items'));
        return true;
    }

    static function send($to, $subject, $body, $from = null) {

        $mail = PhpMailerLibrary::create();

        $from = [
            'Username'  =>  Config::get("common.from_email"),
            'Password'  =>  Config::get("common.from_email_pass"),
            'From'      =>  Config::get("common.from_email"),
            'FromName'  =>  Config::get('common.from_email')
        ];

        MessageQueueItems()->create([
            'title' => $subject,
            'body'  => $body,
            'addresses' =>  $to,
            'fromSend'  =>  json_encode($from)
        ]);

        return true;

//        if (($from) && (is_array($from))) {
//            $mail->Username = isset($from['email']) ? $from['email'] : $mail->Username;
//            $mail->Password = isset($from['password']) ? $from['password'] : $mail->Password;
//            $mail->From = isset($from['email']) ? $from['email'] : $mail->From;
//            $mail->FromName = isset($from['name']) ? $from['name'] : $mail->FromName;
//        }
//
//        $emails = explode(",", $to);
//        foreach ($emails as $email) {
//            $mail->AddAddress(trim($email));
//        }
//
//        $mail->Subject = $subject;
//        $mail->Body = $body;
//        $mail->IsHTML(true);
//
//        @$mail->Send();
    }

}
