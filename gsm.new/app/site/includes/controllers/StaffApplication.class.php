<?

class StaffApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
    );

    function index($vars, $page) {
        $staff = array();

        $jobs = Jobs(array("enabled" => true, "employee__enabled" => true))->order(array("department__sortpos", "sortpos"))->all();

        if ($jobs) {
            foreach ($jobs as $job) {
                if (isset($staff[(string) $job->department->id])) {
                    $staff[(string) $job->department->id]['employees'][] = $job->employee;
                }else{
                    $staff[(string) $job->department->id] = array('employees' => array($job->employee), 'deprtment_data' => $job->department);
                }
            }
        }
        
        print new View('staff/page-departments', compact('page', 'staff'));
        return true;
    }

}