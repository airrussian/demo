<?php

class CertificateApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
    );

    function index($vars, $page) {
        
        $items = Certificates()->filter(array('enabled' => true))->sortedOrder()->all();
        
        print new View('certificate/page-index', compact('page', 'items'));
        return true;
    }

}
