<?

class SimpleCatalogApplication extends UriConfApplication {

    protected $uriconf = array(
//        array('~^/search/?(?:/page\d+)?/?$~', 'search'),
        array('~^/(?P<category>[-\w\.]+(?:/[-\w\.]+)*?)+/(?P<id>\d+)/?$~', 'item'),
        array('~^/(?P<category>[-\w\.]+(?:/[-\w\.]+)*?)+?(?:/page\d+)?/?$~', 'category'),
        array('~^/?$~', 'category'),
    );

    function search($vars, $page) {
        if (Meta::vars("q")) {
            $entries = $entries->filter(array('*__match' => Meta::vars("q")))->orderRelevant();
            $result_count = $entries->count();
        } else {
            $entries = array();
            $result_count = 0;
        }

        $page->title = "Результаты поиска";

        $paginator_per_page = 10;
        $paginator = new NamiPaginator($entries, '_blocks/site-paginator', $paginator_per_page);

        print new View('simple_catalog/page-search', compact('paginator', 'page', 'result_count'));
        return true;
    }

    function category($vars, $page) {
        if (!$vars->category) {
            $this->redirect_to_first_category();
        }

        $category = SimpleCatalogCategories()
            ->get_or_404(array(
            'enabled' => true,
            'uri' => $vars->category . "/",
        ));

        $sub_cat_ids = SimpleCatalogCategories(array("enabled" => true))
            ->filterChildren($category)
            ->embrace($category)
            ->values("id");

        $entries = SimpleCatalogEntries()
            ->filter(array(
                'enabled' => true,
                'category__id__in' => $sub_cat_ids,
            ))
            ->order("title");

//      $sort_params = self::get_current_sort_params();
//      $entries = $entries->$sort_params['method']($sort_params['value']);

        $paginator_per_page = Config::get('catalog.paginator_size');
//      $paginator_per_page = self::get_current_perpage();

        $paginator = new NamiPaginator($entries, '_blocks/site-paginator', $paginator_per_page);
        $page->title = $category->title;
        if ($category->meta_title) {
            $page->meta_title = $category->meta_title;
        }
        if ($category->meta_keywords) {
            $page->meta_keywords = $category->meta_keywords;
        }
        if ($category->meta_description) {
            $page->meta_description = $category->meta_description;
        }

        print new View('simple_catalog/page-category', compact('category', 'paginator', 'page'));
        return true;
    }

    private function redirect_to_first_category() {
        $category = SimpleCatalogCategories(array('enabled' => true, "lvl" => 2))->treeOrder()->first();
        header("Location: " . $category->full_uri, true);
        exit();
    }

    function item($vars, $page) {
        $entry = SimpleCatalogEntries()
            ->get_or_404(array(
            'enabled' => true,
            'category__enabled' => true,
            'category__uri' => $vars->category . "/",
            'id' => $vars->id
        ));

        $page->title = $entry->title;
        if ($entry->meta_title) {
            $page->meta_title = $entry->meta_title;
        }
        if ($entry->meta_keywords) {
            $page->meta_keywords = $entry->meta_keywords;
        }
        if ($entry->meta_description) {
            $page->meta_description = $entry->meta_description;
        }

        print new View('simple_catalog/page-item', compact('page', 'entry'));
    }

    /**
     * Доступные методы для сортировки данных
     */
    static $avalible_sort_params = array(
        "price" => "по возрастанию цены",
        "-price" => "по убыванию цены",
        "title" => "по названию",
        "-discount" => "по размеру скидки"
    );

    /**
     * Сортировка ORM данных
     */
    static function get_current_sort_params() {
        $meta_vars = &Meta::vars();

        if (array_key_exists('sort', $meta_vars) && $meta_vars['sort']) {
            $value = $meta_vars['sort'];
            $method = 'order';

            if (array_key_exists($value, self::$avalible_sort_params)) {
                if (mb_substr($value, 0, 1) === '-') {
                    $value = mb_substr($value, 1, mb_strlen($value));
                    $method = 'orderDesc';
                }

                return array('method' => $method, 'value' => $value);
            }
        }

        return array('method' => 'order', 'value' => 'price');
    }

    /**
     * Данные для вывода в шаблоне страницы
     */
    static function get_sort_links_list() {
        $new_sort_list = array();
        $cur_sort = Meta::vars("sort");
        $uri = $_SERVER['REQUEST_URI'];
        $uri = preg_replace("/sort\=[-]*[^\&]*/", "new_sort_params", $uri);

        if (!mb_strpos($uri, "new_sort_params")) {
            if (preg_match("/\?/", $uri)) {
                $uri = $uri . "&new_sort_params";
            } else {
                $uri = $uri . "?new_sort_params";
            }
        }

        foreach (self::$avalible_sort_params as $param => $title) {
            $new_sort = array(
                "link" => str_replace("new_sort_params", "sort=" . $param, $uri),
                "title" => $title,
                "current" => false
            );

            if ($cur_sort == $param) {
                $new_sort['current'] = true;
            }
            $new_sort_list[] = $new_sort;
        }

        return $new_sort_list;
    }

    /**
     * Доступные варианты количества элементов на страницу 
     */
    static $avalible_perpage_params = array(15, 30, 60);

    /**
     * Определение текущего значеиня "элементов на страницу"
     */
    static function get_current_perpage() {
        $current_perpage = Meta::vars("perpage");

        if (in_array($current_perpage, self::$avalible_perpage_params)) {
            setcookie('perpage', $current_perpage, time() + (60 * 60 * 24 * 365), '/');
            $perpage = $current_perpage;
        } elseif (array_key_exists('perpage', $_COOKIE)) {
            if (in_array($_COOKIE['perpage'], self::$avalible_perpage_params)) {
                $perpage = $_COOKIE['perpage'];
            } else {
                $perpage = self::$avalible_perpage_params[0];
            }
        } else {
            $perpage = self::$avalible_perpage_params[0];
        }


        return $perpage;
    }

    /**
     * Ссылки для вывода списка "количества эл на страницу"
     */
    static function get_perpage_link_list() {
        $new_perpage_list = array();
        $current_perpage = Meta::vars("perpage");

        if (in_array($current_perpage, self::$avalible_perpage_params)) {
            $perpage = $current_perpage;
        } elseif (array_key_exists('perpage', $_COOKIE)) {
            if (in_array($_COOKIE['perpage'], self::$avalible_perpage_params)) {
                $perpage = $_COOKIE['perpage'];
            } else {
                $perpage = self::$avalible_perpage_params[0];
            }
        } else {
            $perpage = self::$avalible_perpage_params[0];
        }

        $uri = $_SERVER['REQUEST_URI'];
        $uri = preg_replace("/perpage\=[-]*[^\&]*/", "new_perpage_params", $uri);

        if (!mb_strpos($uri, "new_perpage_params")) {
            if (preg_match("/\?/", $uri)) {
                $uri = $uri . "&new_perpage_params";
            } else {
                $uri = $uri . "?new_perpage_params";
            }
        }

        foreach (self::$avalible_perpage_params as $param) {
            $new_sort = array(
                "link" => str_replace("new_perpage_params", "perpage=" . $param, $uri),
                "title" => $param,
                "current" => false
            );

            if ($perpage == $param) {
                $new_sort['current'] = true;
            }
            $new_perpage_list[] = $new_sort;
        }

        return $new_perpage_list;
    }

}
