<?php

class TextPageApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
    );

    function index($vars, $page, $uri) {
        $page = Page::replace($page);
        
        print new View('page-text', array('page' => $page));
        return true;
    }

}

