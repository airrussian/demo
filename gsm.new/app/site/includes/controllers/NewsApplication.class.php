<?php

/**
 * Контроллер новостей
 */
class NewsApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?(?:/page\d+)?/?$~', 'index'),
        array('~^/(?P<item>[-\w\.]+)/?$~', 'item')
    );

    /**
     * Выводит список новостей
     */
    function index($vars, $page) {

        $city = ContactCity::GetCurrent();
        $ids = SCityLinks()->filter(array('city' => $city->id))->values('news');
        
        $items = NewsItems()
                ->embrace(array('city__iexact' => 'null'))                
                ->embrace(array('id__in' => $ids))
                ->filter(array('enabled' => true))
                ->orderDesc('date');

        print new View('news/page-index', array(
            'paginator' => new NamiPaginator($items, '_blocks/site-paginator', Config::get('news.items_per_page')),
            'page' => $page
        ));
        return true;
    }

    /**
     * Выводит новость
     * Ссылки старого сайте \d{4}\{2}\d{2}
     * по ТЗ каждая новость имеет свою ссылку.
     */
    function item($vars, $page) {
        
        $city = ContactCity::GetCurrent();
        $ids = SCityLinks()
                ->filter(array('city' => $city->id))
                ->values('news');        
        
        $item = NewsItems()
                ->filter(array('uri' => $vars->item, 'enabled' => true))
                ->first();
        
        if (is_null($item)) {
            Builder::show404();
        }
               
        $crumbs_pages = array(
            array(
                "uri" => $page->uri,
                "title" => $page->title,
            ),
        );
        
        $page->title = $item->title;
        
        $otherNews = NewsItems()
                ->embrace(array('city__iexact' => 'null'))                
                ->embrace(array('id__in' => $ids))                
                ->filter(array('enabled' => true, 'id__ne' => $item->id))
                ->orderDesc('date')
                ->limit(5);

        print new View('news/page-item', array(
            'item' => $item,
            'otherNews' => $otherNews,
            'page' => $page,
            'crumbs_pages' => $crumbs_pages
        ));
        return true;
    }

}
