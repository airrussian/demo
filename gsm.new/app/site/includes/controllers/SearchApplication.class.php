<?

class SearchApplication extends UriConfApplication {

    protected $uriconf = array(
        //array('~^/search-autocomplete/?$~', 'search_autocomplete'),
        array('~^/?(?:/page\d+)?$~', 'search'),
        array('~^/reindex/?$~', 'reindex'),
    );

    /**
     * Переиндексовка всх моделек
     * @param type $vars
     * @param type $page
     */
    function reindex($vars, $page) {

        $model_names = array(
            'Page',
            'PhotoAlbum',
            'Service',
            'Article',
            'Publication'
        );

        foreach ($model_names as $model_name) {
            $models = NamiQuerySet($model_name)->all();
            foreach ($models as $model) {
                $model->save();
            }
            echo "model {$model_name} been reindexed<br />";
        }

        echo "Yahoo!";
    }

    function search($vars, $page) {
        $query = htmlspecialchars(Meta::vars('search'));
        $search_items = SearchRecords()->filter(array('*__match' => $query, 'enabled' => true))->orderDesc("model_name")->orderRelevant();
        $search_count = $search_items->count();
        $paginator = new NamiPaginator(
                $search_items,
                '_blocks/site-paginator',
                Config::get('search.paginator_size'),
                "{$page->uri}/page%{page}/?search=" . $query
        );

        print new View("search/page-result", compact('page', 'query', 'paginator', 'search_count'));
    }

}