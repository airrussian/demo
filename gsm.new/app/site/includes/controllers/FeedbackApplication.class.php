<?

class FeedbackApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
    );

    function index($vars, $page, $uri) {
        print new View('feedback/page-form', compact('page'));
        return true;
    }

}