<?
/**
*   Блоги.
*/
class BlogsApplication extends UriConfApplication {
    protected $uriconf = array(
        array('/',                                      'index'),
        array('/page\d+/?',                             'index'),
        array('/post.html',                             'create_post'),
        array('/tags/:id',                              'tag'),
        array('/tags/:id/page\d+',                      'tag'),
        array('/upload-image',                          'upload_image'),
        array('/upload-video',                          'upload_video'),
        array('/my-posts/delete/:id',                   'user_post_delete'),
        array('/my-posts/:id',                          'user_post_edit'),
        array('/my-posts',                              'user_posts'),
        array('/my-posts/page\d+',                      'user_posts'),
        array('/moderator/messages/all',                'moderate_messages', array('suburi' => 'all')),
        array('/moderator/messages/all/page\d+',        'moderate_messages', array('suburi' => 'all')),
        array('/moderator/messages',                    'moderate_messages', array('checked' => false)),
        array('/moderator/messages/page\d+',            'moderate_messages', array('checked' => false)),
        array('/moderator/messages/hidden',             'moderate_messages', array('enabled' => false, 'suburi' => 'hidden')),
        array('/moderator/messages/hidden/page\d+',     'moderate_messages', array('enabled' => false, 'suburi' => 'hidden')),
        array('/moderator/action',                      'moderate_action'),
    );
        
    function index($vars, $page) {
        $entry_query = BlogPosts()
        ->filter(array(
            'enabled'       => true,
            'blog__enabled' => true,
        ))
        ->follow(3)
        ->orderDesc('date');
        
        $paginator = new NamiPaginator($entry_query, 'site-paginator', 5);
    
        print new View('blogs/index', compact('page', 'paginator'));
    }
    
    function create_post($vars, $page) {
        $blogs = self::get_postable_blog_query()->only('id title page_uri')->sortedOrder()->all();

        if (!$blogs) {
            $heading = 'Только для зарегистрированных пользователей';
            $text = sprintf('Чтобы написать сообщение в блог, необходимо зарегистрироваться на нашем сайте.<br>' .
                            'Это не займет много времени, <a href="%s/register/">добро пожаловать</a> :)',
                            Builder::getAppUri('AccountsApplication')); 
            print new View('failure-page', compact('page', 'heading', 'text'));
            return true;
        }

        $errors = array();

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && Meta::vars('blog-post-form')) {
            $blog_ids = array();
            foreach ($blogs as $i) {
                $blog_ids[] = $i->id;
            }

            $validator = new DataValidator(
                array(
                    'title' => 'striptags trim required',
                    'blog'  => array('required', array('inarray', $blog_ids)),
                    'text'  => array(array('striptags', 'p hr a object param embed blockquote b i s strong strike sup sub h2 h3 ol ul li img br'), 'stripbadattrs', 'trim', 'required'),
                    'tags'  => 'striptags trim optional',
                    'date'  => 'required trim date',
                    'time'  => 'required trim time',
                    'disable_comments'  => 'optional',
                    'is_advice'  => 'optional',
                ),
                array(
                    'title.required'    => 'Укажите заголовок сообщения',
                    'blog.required'     => 'Выберите блог',
                    'blog.inarray'      => 'Вы не можете писать в указанный блог',
                    'text.required'     => 'Укажите текст сообщения',
                    'date.required'     => 'Укажите дату',
                    'date.date'         => 'Неправильная дата',
                    'time.required'     => 'Укажите время',
                    'time.time'         => 'Непра&shy;вильное время',
                )
            );
            
            try {
                $vars = & Meta::vars();
                
                // Предварительная обработка - поменяем в тексте <div> на <p>
                $vars['text'] = preg_replace('~(?<=<)div|(?<=</)div~', 'p', $vars['text']);
                
                $status = $validator->process($vars, true);
                if ($status->ok) {
                    $post_data = $status->data;
                    $post_data['date'] .= ' ' . $post_data['time'];
                    $post_data['user'] = SiteSession::getInstance()->getUser();
                    $post_data['disable_comments'] = @$post_data['disable_comments'] ? true : false;
                    $post_data['is_advice'] = @$post_data['is_advice'] ? true : false;
                    
                    $post = BlogPosts()->create($post_data);
                    
                    throw new HttpRedirect($post->uri, false);
                } else {
                    $errors = array_merge($errors, $status->errors);
                }
            } catch (HttpRedirect $e) {
                throw $e;
            } catch (Exception $e) {
                $errors['__form__'] = $e->getMessage();
            }

        } else {
            $vars = & Meta::vars();
            $vars['date'] = strftime('%d.%m.%Y');
            $vars['time'] = strftime('%R');
            if (array_key_exists('HTTP_REFERER', $_SERVER)) {
                foreach ($blogs as $i) {
                    if (strpos($_SERVER['HTTP_REFERER'], $i->page_uri) !== false) {
                        $vars['blog'] = $i->id;
                        break;
                    }
                }
            }
        }
        
        print new View('blogs/create-post', compact('page', 'blogs', 'errors'));
    }
    
    function get_postable_blog_query() {
        $user = SiteSession::getInstance()->getUser();
        
        $query = Blogs()->filter(array('enabled' => true));
        
        if ($user) {
            $author_blog_ids = BlogAuthors()
            ->filter(array('user'=> $user))
            ->values('blog');
            
            $query = $query->filter(QOR(
                Q('id__in', $author_blog_ids),
                Q('post_access__bitsset', BLOG_ACCESS_USERS)
            ));
        } else {
            $query = $query->filter(array('post_access__bitsset' => BLOG_ACCESS_ANONYMOUS));
        }
        
        return $query;
    }
    
    function tag($vars, $page) {
        $tag = BlogTags()->get_or_404($vars->id);
        
        $post_ids = BlogPostTags()
        ->filter(array('tag' => $tag))
        ->values('post');
        
        $entry_query = BlogPosts()
        ->filter(array(
            'enabled'       => true,
            'blog__enabled' => true,
            'id__in'        => $post_ids,
        ))
        ->follow(3)
        ->orderDesc('date');
        
        $paginator = new NamiPaginator($entry_query, 'site-paginator', 5);
    
        print new View('blogs/index', compact('page', 'paginator', 'tag'));
    }
    
    function upload_image($vars, $page) {
        $file = Meta::vars('file');
        $session = SiteSession::getInstance();
        if ($_SERVER['REQUEST_METHOD'] == 'POST'
         && @$file['error'] == UPLOAD_ERR_OK
         && preg_match("~https?://{$_SERVER['HTTP_HOST']}~", $_SERVER['HTTP_REFERER'])) {
            $image = UserImages()->create(array('image' => $file));
            print $image->image->resized->uri;
        }
    }

    function upload_video($vars, $page) {
        $file = Meta::vars('file');
        $session = SiteSession::getInstance();
        if ($_SERVER['REQUEST_METHOD'] == 'POST'
         && @$file['error'] == UPLOAD_ERR_OK
         && $session->isAuthorized()
         && $session->getUser()->author_blogs->values('id', 1)) {
            $video = UserVideos()->create(array('video' => $file))->video;
            ?><embed flashvars="file=<?= $video->uri ?>&image=<?= $video->preview_uri ?>&controlbar.position=bottom" allowfullscreen="true" allowscripaccess="always" src="/static/3rdparty/jwplayer/player.swf" width="<?= $video->width ?>" height="<?= $video->height ?>" wmode="opaque"/><?
        }
    }

    function moderate_messages($vars, $page) {
        if (!self::moderator_logged_in()) {
            return self::no_moderator_access($page);
        }
        
        $condition = array();
        $raw_condition = array('1 = 1');
        
        foreach (array('checked', 'enabled') as $param) {
            if (!is_null($vars->$param)) {
                $condition[$param] = $vars->$param;
                $raw_condition[] = sprintf("%s = '%d'", $param, $vars->$param);
            }
        }
        $raw_condition = join(' and ', $raw_condition);
        
        /*  В списке отображаются и посты и комменты, поэтому нужно всю хуйню приготовить руками,
            а пагинатору подсунуть болванку. */

        // Считаем количество записей
        $count = BlogPosts()->filter($condition)->count()
               + BlogComments()->filter($condition)->count();
               
        // Делаем пагинатор с болванкой
        $paginator = new NamiPaginator($count ? range(0, $count - 1) : array(), 'site-paginator', 10);
        
        if ($count) {
            // Теперь мы из пагинатора можем узнать смещение и количество записей :3
            $query = "select id, 'BlogComment' as 'model', date from blogcomment where {$raw_condition} " . 
                     "union " . 
                     "select id, 'BlogPost' as 'model', date from blogpost where {$raw_condition} " . 
                     "order by date asc limit {$paginator->size} offset {$paginator->objects[0]}";
            $cursor = NamiCore::getBackend()->cursor;
            $cursor->execute($query); 
            $entry_rows = $cursor->fetchAll();
    
            // Сгруппируем идентификаторы объектов по моделям        
            $ids = array();
            foreach ($entry_rows as $i) {
                if (!array_key_exists($i['model'], $ids)) {
                    $ids[$i['model']] = array();
                }
                $ids[$i['model']][] = $i['id'];
            }
            
            // Запросим объекты каждой модели одним запросом и разложим в ассоциативный массив по id
            $objects = array();
            foreach ($ids as $model => $entry_ids) {
                $entries = NamiQuerySet($model)->filter(array('id__in' => $entry_ids))->all();
                $objects[$model] = Meta::getAssocArray($entries, 'id');
            }
            
            // Наконец, соберем объекты в упорядоченный список
            $entries = array();
            foreach ($entry_rows as $i) {
                $entries[] = $objects[$i['model']][$i['id']];
            }
        } else {
            $entries = array();
        }
        
        $suburi = $vars->suburi;

        $return  = $suburi ? "{$page->uri}/moderator/messages/{$suburi}/"
                           : "{$page->uri}/moderator/messages/";

        print new View('blogs/moderate-comments', compact('page', 'paginator', 'entries', 'suburi', 'return'));
    }
    
    function moderate_action($vars, $page) {
        if (!self::moderator_logged_in()) {
            return self::no_moderator_access($page);
        }
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $in = Meta::vars();
            
            $targets = array();
            
            foreach (array('posts' => 'BlogPost', 'comments' => 'BlogComment') as $key => $model) {
                if (array_key_exists($key, $in)) {
                    $targets = array_merge($targets, NamiQuerySet($model)->filter(array('id__in' => $in[$key]))->all());
                }
            }
            
            if ($targets) {
                switch ($in['action']) {
                case 'check':
                    foreach ($targets as $i) {
                        $i->checked = true;
                        $i->save();
                    }
                    break;
                
                case 'hide':
                    foreach ($targets as $i) {
                        $i->checked = true;
                        $i->enabled = false;
                        $i->save();
                    }
                    break;
    
                case 'show':
                    foreach ($targets as $i) {
                        $i->enabled = true;
                        $i->save();
                    }
                    break;
    
                case 'delete':
                    foreach ($targets as $i) {
                        $i->delete();
                    }
                    break;
                }
            }
            
            $return = array_key_exists('return', $in) ? $in['return'] : $_SERVER['HTTP_REFERER'];
            throw new HttpRedirect($return, false);
        }
    }
    
    /*
    *   Проверка наличия привилегии модератора у текущего пользователя
    *
    */
    function moderator_logged_in() {
        $session = SiteSession::getInstance();
        return $session->isAuthorized() && $session->getUser()->moderator;
    }
    
    /*
    *   Отображение сообщения о недоступности модераторских инструментов стороннему пользователю
    */
    function no_moderator_access($page) {
        $heading = 'Только для модераторов';
        $text = 'Эта страница доступна только модераторам сайта.';
        print new View('failure-page', compact('heading', 'text', 'page'));
        return;
    }
    
    /**
    *   Проверка возможности комментирования поста/блога переданным пользователем.
    *   Возвращает массив [bool $enabled, string $reason], причина заполняется для enabled = false
    *   и может принимать значения 'unauthorized', 'comments disabled', 'access denied'.
    */
    function user_can_comment($user, $post, $blog) {
        if ($post && $post->disable_comments) {
            return array(false, 'comments disabled');
        }
        
        if ($user) {
            if ($blog->comment_access & BLOG_ACCESS_USERS) {
                return array(true, '');
            }
            
            if ($blog->comment_access & BLOG_ACCESS_AUTHORS 
            &&  $blog->authors->filter(compact('user'))->values('id')) {
                return array(true, '');
            }
        } else {
            if ($blog->comment_access & BLOG_ACCESS_ANONYMOUS) {
                return array(true, '');
            }
            
            if ($blog->comment_access & BLOG_ACCESS_USERS) {
                return array(false, 'unauthorized');
            }
        }
        
        return array(false, 'access denied');
    }
    
    /**
    *   Отображение и обработка формы постинга комментария в указанный пост
    */
    function comment_form($parameters) {
        $args = new NamedArguments($parameters, 'post | auto_post');
        
        $user = SiteSession::getInstance()->getUser();
        $post = $args->post;
        $blog = $post ? $post->blog : $args->auto_post['blog'];

        // Определимся, может ли пользователь комментировать пост
        list($enabled, $reason) = self::user_can_comment($user, $post, $blog);

        if (!$enabled) {
            return new View('blogs/comment-form-disabled', compact('reason'));
        }
        
        // Штуки для обработки постинга
        $errors = array();
        
        // Имя формы постинга
        $form_name = strtolower(__CLASS__ . '_' . __FUNCTION__);

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && Meta::vars($form_name)) {
            $validator_rules = array(
                //'text'  => 'striptags trim required',
                'text'  => array(array('striptags', 'p a object param embed b i s strong strike sup sub img br'), 'stripbadattrs', 'trim', 'required'),
            );
            
            if (!SiteSession::getInstance()->isAuthorized()) {
                $validator_rules = array_merge($validator_rules, array(
                    'anon_name'  => 'striptags trim required',
                    'anon_email' => 'optional email',
                ));
            }
        
            $validator = new DataValidator(
                $validator_rules,
                array(
                    'text.required'     => 'Укажите текст комментария',
                    'anon_name.required'     => 'Укажите имя',
                    'anon_email.email'       => 'Укажите правильный адрес email',
                )
            );
            
            try {
                $vars = & Meta::vars();
                $status = $validator->process($vars, true);
                if ($status->ok) {
                    // Теперь интересный момент - если поста нет - нужно его создать
                    if (!$post) {
                        $post = $blog->entries->create($args->auto_post);
                        $args->auto_post['related']->post = $post;
                        $args->auto_post['related']->save();
                    }

                    $comment_data = $status->data;
                    if ($user) {
                        $comment_data['user'] = $user;
                    }

                    $comment = $post->entries->create($comment_data);
                    
                    // Сохраним анонимное имя и email
                    setcookie('blog_anon_name', $comment->anon_name, time() + 31536000, '/');
                    setcookie('blog_anon_email', $comment->anon_email, time() + 31536000, '/');
                    
                    $uri = $args->return ? "{$args->return}#comment{$comment->id}"
                                            : "{$blog->uri}/comments/{$comment->id}";

                    throw new HttpRedirect($uri, false);
                } else {
                    $errors = array_merge($errors, $status->errors);
                }
            } catch (HttpException $e) {
                throw $e;
            } catch (Exception $e) {
                $errors['__form__'] = $e->getMessage();
            }

        } else {
            if (!SiteSession::getInstance()->isAuthorized()) {
                $vars = & Meta::vars();
                foreach (array('anon_name', 'anon_email') as $name) {
                    if (array_key_exists("blog_{$name}", $_COOKIE)) {
                        $vars[$name] = $_COOKIE["blog_{$name}"];
                    }
                }
            }
        }
    
        return new View('blogs/comment-form', compact('blog', 'post', 'errors', 'form_name'));
    }
    
    function user_posts($vars, $page) {
        $user = SiteSession::getInstance()->getUser();
        if (!$user) {
            $heading = 'Только для зарегистрированных пользователей';
            $text = 'Вы должны войти на сайт, чтобы получить доступ к этой странице.';
            print new View('failure-page', compact('heading', 'text', 'page'));
            return;
        }
        
        $post_query = $user->blog_posts->only('id title date blog')->orderDesc('date');
        $paginator = new NamiPaginator($post_query, 'site-paginator', 10);

        print new View('blogs/user-posts', compact('page', 'paginator'));
    }

    function user_post_edit($vars, $page) {
        $user = SiteSession::getInstance()->getUser();
        if (!$user) {
            $heading = 'Только для зарегистрированных пользователей';
            $text = 'Вы должны войти на сайт, чтобы получить доступ к этой странице.';
            print new View('failure-page', compact('heading', 'text', 'page'));
            return;
        }
        
        $post = $user->blog_posts->filter(array('id' => $vars->id))->first();
        if (!$post) {
            throw new Http404;
        }

        $blogs = self::get_postable_blog_query()->only('id title page_uri')->sortedOrder()->all();

        $errors = array();

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && Meta::vars('blog-post-form')) {
            $blog_ids = array();
            foreach ($blogs as $i) {
                $blog_ids[] = $i->id;
            }

            $validator = new DataValidator(
                array(
                    'title' => 'striptags trim required',
                    'blog'  => array('required', array('inarray', $blog_ids)),
                    'text'  => array(array('striptags', 'p hr a object param embed blockquote b i s strong strike sup sub h2 h3 ol ul li img br'), 'stripbadattrs', 'trim', 'required'),
                    'tags'  => 'striptags trim optional',
                    'date'  => 'required trim date',
                    'time'  => 'required trim time',
                    'disable_comments'  => 'optional',
                    'is_advice'         => 'optional',
                ),
                array(
                    'title.required'    => 'Укажите заголовок сообщения',
                    'blog.required'     => 'Выберите блог',
                    'blog.inarray'      => 'Вы не можете писать в указанный блог',
                    'text.required'     => 'Укажите текст сообщения',
                    'date.required'     => 'Укажите дату',
                    'date.date'         => 'Неправильная дата',
                    'time.required'     => 'Укажите время',
                    'time.time'         => 'Непра&shy;вильное время',
                )
            );
            
            try {
                $vars = & Meta::vars();
                
                // Предварительная обработка - поменяем в тексте <div> на <p>
                $vars['text'] = preg_replace('~(?<=<)div|(?<=</)div~', 'p', $vars['text']);
                
                $status = $validator->process($vars, true);
                if ($status->ok) {
                    $post_data = $status->data;
                    $post_data['date'] .= ' ' . $post_data['time'];
                    $post_data['user'] = SiteSession::getInstance()->getUser();
                    $post_data['disable_comments'] = @$post_data['disable_comments'] ? true : false;
                    $post_data['is_advice'] = @$post_data['is_advice'] ? true : false;
                    
                    $post->copyFrom($post_data);
                    $post->save();

                    throw new HttpRedirect("{$page->uri}/my-posts/", false);
                } else {
                    $errors = array_merge($errors, $status->errors);
                }
            } catch (HttpRedirect $e) {
                throw $e;
            } catch (Exception $e) {
                $errors['__form__'] = $e->getMessage();
            }

        } else {
            $vars = & Meta::vars();
            $vars['date']   = $post->date->format('%d.%m.%Y');
            $vars['time']   = $post->date->format('%R');
            $vars['blog']   = $post->blog->id;
            $vars['title']  = $post->title;
            $vars['text']   = $post->text;
            $vars['tags']   = $post->tags;
            $vars['disable_comments'] = $post->disable_comments ? true : false;
            $vars['is_advice'] = $post->is_advice ? true : false;
        }
        
        print new View('blogs/edit-post', compact('page', 'blogs', 'errors'));
    }
    
    function user_post_delete($vars, $page) {
        $user = SiteSession::getInstance()->getUser();
        if (!$user) {
            $heading = 'Только для зарегистрированных пользователей';
            $text = 'Вы должны войти на сайт, чтобы получить доступ к этой странице.';
            print new View('failure-page', compact('heading', 'text', 'page'));
            return;
        }
        
        $post = $user->blog_posts->filter(array('id' => $vars->id))->first();
        
        if (!$post) {
            throw new Http404;
        }
        
        $post->delete();
        $return = Meta::vars('return') ? Meta::vars('return') : $_SERVER['HTTP_REFERER'];
        throw new HttpRedirect($return, false);
    }
}