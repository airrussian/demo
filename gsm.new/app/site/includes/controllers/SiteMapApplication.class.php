<?

class SiteMapApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?(?:/page\d+)?/?$~', 'index'),
    );

    function index($vars, $page) {

        $site_map = new SiteMapProcessor();
//        $cars = SimpleCatalogEntries(array("enabled" => true))->sortedOrder()->all();
//        $site_map->add_branch_by_appclassname($cars, "SimpleCatalogApplication", true, array("title" => "title", "uri" => "name"));

        $site_map_data = $site_map->get_tree_as_array();

        print new View('sitemap/page-map', compact('page', 'site_map_data'));
        return true;
    }

}