<?

class CatalogApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^(?:/page\d+)?/?$~', 'index'),
        array('~^/showcase/(?P<type>[-\w\.]+)/?(?:/page\d+)?/?$~', 'showcase'),
        array('~^/model/(?P<model_id>\d+)/?$~', 'model'),
        array('~^/brand/(?P<brand_id>\d+)/?$~', 'brand'),
        array('~^/spares/?$~', 'spares'),
        array('~^/ajax/get_models/?$~', 'get_models'),
        array('~^/nophoto(?:/page\d+)?/?$~', 'nophoto'),
        array('~^/search(?:/page\d+)?/?$~', 'search'),
        array('~^/(?P<onec_code>\d{8})/?$~', 'item'),
        array('~^/(?P<category>[-\w\.]+(?:/[-\w\.]+)*?)+?/(?P<id>\d+)/?$~', 'item'),
        array('~^/(?P<category>[-\w\.]+(?:/[-\w\.]+)*?)+?(?:/page\d+)?/?$~', 'category'),
    );
    static public $availability = array(0, 4, 20, 80);

    static function getAvailability($count) {
        foreach (self::$availability as $key => $availability) {
            if ($count <= $availability) {
                return $key;
                break;
            }
        }
        return 4;
    }

    function brand($vars, $page) {
        $brand_id = (int) $vars->brand_id;

        $models = CatalogModels()->filter(array('enabled' => true, 'brand' => $brand_id))->all();
        if (Meta::isAjaxRequest()) {
            foreach ($models as $model) {
                echo "<option value='{$model->id}'>{$model->title}</option>";
            }
            exit();
        }
        return false;
    }

    function showcase($vars, $page) {

        // Кэш время на 12 часов, если нет nocache в параметре
        $cachetime = Meta::vars("nocache") ? 0 : 12 * 3600;

        $user = SiteSession::getInstance()->getUser();
        $string = $_SERVER['HTTP_HOST'] . Meta::getUriPath();
        if ($user) {
            $string .= "_user:" . $user->type . "_" . $user->city . "_" . $user->expert;
        }

        $md5 = md5($string);

        $pathCache = $_SERVER['DOCUMENT_ROOT'] . "/static/uploaded/cache/showcase/";
        $path = $pathCache . "/" . mb_substr($md5, 0, 2);
        $filename = $path . "/" . $md5;

        if (file_exists($filename) && ((time() - filemtime($filename)) < $cachetime)) {
            print file_get_contents($filename);
            return true;
        }

        $cat_ids = CatalogSinglton::getInstance()->category_getids();

        switch ($vars->type) {
            default:
            case 'popular':
                $items = CatalogEntries(array("enabled" => true, 'category__in' => $cat_ids))
                        ->orderDesc("rating")
                        ->limit(Config::get("main.popular.count"));
                shuffle($items);
                break;
            case 'last':
                $items = CatalogEntries(array("enabled" => true, 'new' => true, 'category__in' => $cat_ids))
                        ->orderDesc("oneC_id")
                        ->limit(Config::get("main.last.count"));
                break;
            case 'recomend':
                $items = CatalogEntries(array("enabled" => true, 'recomended' => true, 'category__in' => $cat_ids))
                        ->orderRand()
                        ->limit(Config::get("main.recomend.count"));
                break;
            case 'sale':
                $items = CatalogEntries(array("enabled" => true, 'sale' => true, 'category__in' => $cat_ids))
                        ->orderRand()
                        ->limit(Config::get("main.sale.count"));
                break;
            case 'onorder':
                $items = CatalogEntries(array('enabled' => true, 'onorder' => true, 'category__in' => $cat_ids))
                        ->orderRand();
                break;
        }

        $paginator = new NamiPaginator($items, '_blocks/site-paginator_showcase', 12);
        $content = (string) new View("catalog/block-showcase", compact('paginator'));
        mkdir($path, 0777, true);
        file_put_contents($filename, $content);

        print $content;
        return true;
    }

    function spares($vars, $page) {

        if (Meta::vars()) {
            $brand_id = Meta::vars('brand');
            $model_id = Meta::vars('model');

            $model = CatalogModels()->get_or_404(array('id' => $model_id));
            $brand = CatalogBrands()->get(array('id' => $brand_id));

            $crumbs_pages[] = array('title' => 'Подбор запчастей и аксессуаров по модели', 'uri' => Builder::getAppUri('CatalogApplication') . 'spares/');

            $title = "Запчасти и аксессуары для " . $brand->title . " " . $model->title;
        } else {
            $title = "Подбор запчастей и аксессуаров по модели";
        }

        $groups = CatalogGroups()->filter(array('enabled' => true, 'model' => $model_id))->all();

        $entries_group = array();

        foreach ($groups as $group) {
            $entry_ids = CatalogEntryGroupLinks()->filter(array('group' => $group->id, 'entry__enabled' => true))->values('entry');
            $entries_group[$group->id] = CatalogEntries()->filter(array('enabled' => true, 'id__in' => $entry_ids))->all();
        }


        $page->title = $title;

        print new View("catalog/page-spares", compact('page', 'crumbs_pages', 'groups', 'entries_group'));
        return true;
    }

    function index($vars, $page) {

        $categoryTree = CatalogSinglton::getInstance()->category_filterLever(2, 3);

        print new View("catalog/page-index", compact('page', 'categoryTree'));

        return true;
    }

    function item($vars, $page) {

        $filter = [
            'enabled' => true,
            'category__enabled' => true,
        ];

        if ($vars->onec_code) {
            $filter['oneC_id'] = $vars->onec_code;
        } else {
            $filter['id'] = $vars->id;
        }

        $item = CatalogEntries()->get_or_404($filter);

        CatalogEntry::historyAdd($item);

        $crumbs_pages[] = ['title' => 'Каталог', 'uri' => $page->uri];
        $categories = CatalogSinglton::getInstance()->category_parents($item->category);
        foreach ($categories as $category) {
            $crumbs_pages[] = ['title' => $category->title, 'uri' => $category->full_uri];
        }

        $fieldset = $item->category->fieldset;
        if (!$fieldset) {
            $parent_categories = CatalogCategories()->filterParents($item->category)->all();
            foreach ($parent_categories as $parent_category) {
                if ($parent_category->fieldset) {
                    $fieldset = $parent_category->fieldset;
                }
            }
        }
        $extrafields = CatalogFieldSetFields(array('fieldset' => $fieldset))
                ->follow(1)
                ->sortedOrder()
                ->all();

        $page->title = $item->title;

        $page->meta_title = "{$item->title} купить в интернет-магазине GSM – Service в {{city}}";
        $page->meta_description = "{$item->title}: цена: {$item->price_rozn} рублей, характеристики. Информация о наличии в {{city}}, способах доставки и оплаты.";
        $page = Page::replace($page);

        if (Meta::isAjaxRequest()) {
            if (Meta::vars("series")) {
                header("Content-type:application/json");
                print json_encode(array(
                    'success' => true,
                    'content' => (string) new View('catalog/ajax-item', array(
                'page' => $page,
                'item' => $item,
                'category_uri' => $item->category->uri,
                'category_title' => $item->category->title,
                'extrafields' => $extrafields,
                'crumbs_pages' => $crumbs_pages
                    )),
                    'crumbs' => (string) new View("_blocks/crumbs", compact('crumbs_pages', 'page')),
                    'title' => $page->title
                ));
            } elseif (Meta::vars("series_entry")) {
                header("Content-type:application/json");
                $limit = 50;
                $offset = Meta::vars("offset");
                $items = $item->getSeriesEntries($limit, $offset);
                $count = $item->getSeriesEntriesCount();
                print json_encode([
                    'success' => true,
                    'content' => (string) new View('income/block-entry', compact('items')),
                    'more' => ($count - $limit - $offset)
                ]);
            } else { // Используется в формах 
                echo "<a href='{$item->full_uri}'>{$item->title}</a>";
            }
            return true;
        }

        print new View('catalog/page-item', array(
            'page' => $page,
            'item' => $item,
            'category_uri' => $item->category->uri,
            'category_title' => $item->category->title,
            'extrafields' => $extrafields,
            'crumbs_pages' => $crumbs_pages
        ));
        return true;
    }

    function category($vars, $page) {

        if ($category = CatalogCategories(array('uri' => "/" . $vars->category, 'enabled' => true))->first()) {

            $ids = $this->childrenCategoryIds($category);

            $filter = array(
                'enabled' => true,
                'category__in' => $ids,
                self::getPriceType() . '__gt' => 0
            );

            $maxprice = (int) CatalogEntries()->filter($filter)->orderDesc(self::getPriceType())->first()->{self::getPriceType()};

            $items = CatalogEntries()->filter($filter);

            $items = self::filtred($items);
            $items = self::sorted($items);

            $page->title = $category->title;

            $crumbs_pages[] = array('title' => 'Каталог', 'uri' => Builder::getAppUri('CatalogApplication'));
            $categories = CatalogSinglton::getInstance()->category_parents($category);
            unset($categories[count($categories) - 1]);
            foreach ($categories as $_category) {
                $crumbs_pages[] = array('title' => $_category->title, 'uri' => $_category->full_uri);
            }

            $paginator = new NamiPaginator($items, '_blocks/site-paginator', self::get_current_perpage());

            $page = $this->category_seo($page, $category);

            print new View('catalog/page-category', array(
                'page' => $page,
                'paginator' => $paginator,
                'category_uri' => $vars->category,
                'category_title' => $category->title,
                'category' => $category,
                'crumbs_pages' => $crumbs_pages,
                'maxprice' => $maxprice
            ));

            return true;
        } else {
            return false;
        }
    }

    private function category_seo(Page $page, CatalogCategory $category) {

        $page->meta_title = $category->meta_title ? $category->meta_title : $page->meta_title;
        $page->meta_keywords = $category->meta_keywords ? $category->meta_keywords : $page->meta_keywords;
        $page->meta_description = $category->meta_description ? $category->meta_description : $page->meta_description;
        $page->meta_h1 = $category->meta_h1 ? $category->meta_h1 : $page->meta_h1;
        $page->title = $page->meta_h1 ? $page->meta_h1 : $page->title;

        $parent = CatalogCategories()->filterParents($category)->filterLevel(2)->first();

        if (($parent->title == "Чехлы и украшения") && ($category->lvl == 4)) {
            $sub = CatalogCategories()->filterParents($category)->filterLevel(3)->first();
            $page->meta_title = "Чехлы {$sub->title} {$category->title} купить в {{city}} - GSM – Service";
            $page->meta_description = "Доступные цены на чехлы и кейсы для {$sub->title} {$category->title} в интернет-магазине GSM Service в {{city}}.";
            $page->meta_h1 = "Чехлы для {$sub->title} {$category->title}";
        }

        if ($parent->title == "Защитные стёкла и плёнки") {
            if ($category->lvl == 3) {
                $page->meta_title = "Защитные стекла и пленки для телефонов {$category->title} купить в {{city}} - GSM – Service";
                $page->meta_description = "Доступные цены на защитные стекла и пленки для {$category->title} в интернет-магазине GSM Service в {{city}}.";
                $page->meta_h1 = "Защитные стекла и пленки {$category->title}";
            }
            if ($category->lvl == 4) {
                $sub = CatalogCategories()->filterParents($category)->filterLevel(3)->first();
                $page->meta_title = "Защитные стекла и пленки для телефонов {$sub->title} {$category->title} купить в {{city}} - GSM – Service";
                $page->meta_description = "Доступные цены на защитные стекла и пленки для {$sub->title} {$category->title} в интернет-магазине GSM Service в {{city}}.";
                $page->meta_h1 = "Защитные стекла и пленки {$sub->title} {$category->title}";
            }
        }

        $page = Page::replace($page);
        return $page;
    }

    function search($vars, $page) {

        $vars = Meta::vars();

        $category_uri = '';
        $category_title = 'Каталог';

// Фильтр для товаров
        $filter = array('enabled' => true);

// Добавляем к фильтру по товарам, если указана категория 
        if (isset($vars['sc']) && ($vars['sc'] != "")) {
            $sc = $vars['sc'];
            if ($category = CatalogCategories(array('enabled' => true, 'id' => $sc))->first()) {

                $filter['category__in'] = $this->childrenCategoryIds($category);

                $category_uri = $category->uri;
                $category_title = $category->title;

                $filter[self::getPriceType() . '__gt'] = 0;
            }
        } else {
// Поиск по всем категориями, которые активны. 
            $filter['category__in'] = CatalogCategories(array('enabled' => true))->values('id');
        }

        $items = CatalogEntries()->filter($filter);

        $sorting_relevant = false;
        if (isset($vars['s']) && ($vars['s'])) {
            $s = $vars['s'];
            if ($s == 'Поиск товаров в каталоге') {
                $s = '';
            }
            $s = rawurldecode($s);

            if ($codeItems = self::find_by_code($items, $s)) {
                $items = $items->orderDescByField("id", array($codeItems->id));
            }

            $items = self::full_text_search($items, $s);
        }

        $maxprice = (int) $items->orderDesc(self::getPriceType())->first()->{self::getPriceType()};

        $items = self::filtred($items);
        $items = self::sorted($items);

        if (Meta::isAjaxRequest()) {

            header('Content-Type: application/json');
            $count = $items->count();
            $items = $items->limit(6);
            echo json_encode([
                'html' => (string) new View("catalog/block-search__autocomplete", compact('items', 'count')),
                'count' => $count
            ]);
            return true;
        }

        $page->title = 'Результаты поиска';

        $paginator = new NamiPaginator($items, '_blocks/site-paginator', self::get_current_perpage());

        print new View("catalog/page-search", compact('page', 'paginator', 'category_uri', 'category_title', 'maxprice'));
        return true;
    }

    /** @TODO Будет проблема если оключена родительская категория */
    private function childrenCategoryIds($category) {
        return CatalogCategories()
                        //->filter(array("enabled" => true))
                        ->filterChildren($category)
                        ->embrace($category)
                        ->values('id');
    }

    static private function filtred($items) {

        $settings = self::get_settings();

        $prefix = ContactCity::GetCurrent()->prefix;

        $meta = Meta::vars('f');
        if (isset($meta['price'])) {
            if (isset($meta['price']['min'])) {
                $items = $items->filter(array(self::getPriceType() . '__ge' => $meta['price']['min']));
            }
            if (isset($meta['price']['max'])) {
                $items = $items->filter(array(self::getPriceType() . '__le' => $meta['price']['max']));
            }
        }

        $na = false;
        if (Meta::vars("filter")) {
            $na = Meta::vars("na") ? true : false;
            $settings['na'] = $na;
            self::set_settings($settings);
        } elseif (isset($settings['na'])) {
            $na = $settings['na'];
        }

        if ($na)
            $items = $items->filter(array("a_{$prefix}__gt" => 0));

        return $items;
    }

    public static function get_filter_na()
    {
        $settings = self::get_settings();
        if (Meta::vars("filter")) {
            return Meta::vars("na") ? true : false;;
        } elseif (isset($settings['na'])) {
            return $settings['na'];
        }

        return false;
    }

    static public function ru2lat($search) {
// поиск русских символов
//preg_match('/[\x{0410}-\x{044F}]+/u', $search, $matches);
        preg_match_all('/\b(?:[A-z0-9АМСКТНРОЕХаеурсох])(?:[A-zАМСКТНРОЕХаеурсох]*)(?:\d+)(?:[A-zАМСКТНРОЕХаеурсох]*)\b/u', $search, $matches);

        $matches = $matches[0];

        if ($matches) {

// кирилица => латиница
            $replacement_array = array(
                'а' => 'a',
                'А' => 'A',
                'е' => 'e',
                'Е' => 'E',
                'о' => 'o',
                'О' => 'O',
                'р' => 'p',
                'Р' => 'P',
                'с' => 'c',
                'С' => 'C',
                'х' => 'x',
                'Х' => 'X',
                'у' => 'y',
                'К' => 'K',
                'М' => 'M',
                'Н' => 'H',
                'Т' => 'T',
            );

            foreach ($matches as $match) {
                preg_match_all('/[А-я]/u', $match, $letters);
                $letters = $letters[0];
                if (!empty($letters)) {
                    $new_match = $match;
                    foreach ($letters as $letter) {
                        $new_match = str_replace($letter, $replacement_array[$letter], $new_match);
                    }
                    $search = str_replace($match, $new_match, $search);
                }
            }
        }
        return $search;
    }

    static private function find_by_code($items, $search) {
        $item = [];
        if (preg_match("/^\d{1,8}$/", $search)) {
            $number = str_repeat("0", 8 - mb_strlen($search)) . $search;
            $item = $items->get(array('oneC_id__containswords' => $number));
        }

        return $item;
    }

    static private function full_text_search($items, $search) {

        $search = trim(self::ru2lat($search));

        $search = str_replace(array("-", "/", "\\"), " ", $search);
        $search = str_replace("'", "\'", $search);

        preg_match_all('~"(.+?)"~', $search, $r);

        foreach ($r[0] as $w) {
            $search = trim(str_replace($w, "", $search));
        }

        $words = $r[1];

        $words = array_merge($words, explode(" ", trim($search)));

        $where = array();
        foreach ($words as &$word) {
            $word = trim($word);

            if (mb_strlen($word) < 1)
                continue;

            $where[] = "title LIKE '%{$word}%'";
        }

        $query = "SELECT entry as id FROM `catalogentrymem` WHERE " . implode(" AND ", $where);

        $savetime = microtime(true);
        $backend = NamiCore::getBackend();
        $backend->cursor->execute($query);
        $result = $backend->cursor->fetchAll();
        //echo "<!-- SEARCH TIME: ".microtime(true)." ".$savetime."-->";


        $search_ids = [];

        if ($item = self::find_by_code($items, $search)) {
            $search_ids[] = $item->id;
        }

        foreach ($result as $val) {
            $search_ids[] = $val['id'];
        }

        $items = $items->filter(array('id__in' => $search_ids));

        return $items;
    }

    /**
     * Достпуные параметры отобржаения вывода     
     */
    static $avalible_view_params = array(
        'block' => 'Плиткой',
        'list' => 'Списком'
    );

    static function get_current_view() {
        $meta_vars = &Meta::vars();
        $settings = self::get_settings();

        if (array_key_exists('view', $meta_vars) && $meta_vars['view']) {
            $value = $meta_vars['view'];

            if (array_key_exists($value, self::$avalible_view_params)) {
                $settings['view'] = $value;
                self::set_settings($settings);
                return $value;
            }
        } else {
            if (isset($settings['view']) && array_key_exists($settings['view'], self::$avalible_view_params)) {
                return $settings['view'];
            }
        }

        reset(self::$avalible_view_params);
        return key(self::$avalible_view_params);
    }

    /**
     * Данные для вывода в шаблоне страницы
     */
    static function get_view_links_list() {
        $view_list = array();

        $current = false;

        $settings = self::get_settings();

        if (Meta::vars("view")) {
            if (!array_key_exists(Meta::vars('view'), self::$avalible_view_params)) {
                $current = true;
            }
        } elseif (isset($settings['view'])) {
            if (!array_key_exists($settings['view'], self::$avalible_view_params)) {
                $current = true;
            }
        }

        foreach (self::$avalible_view_params as $param => $title) {
            $new_sort = array(
                "link" => Meta::set_uri_param("view", $param),
                "value" => $param,
                "title" => $title,
                "current" => $current
            );

            if (Meta::vars("view")) {
                if ($param == Meta::vars("view")) {
                    $new_sort['current'] = true;
                }
            } else {
                if (isset($settings['view']) && ($param == $settings['view'])) {
                    $new_sort['current'] = true;
                }
            }

            $view_list[$param] = $new_sort;

            $current = false;
        }

        return $view_list;
    }

    public static function get_settings() {
        $settings = [];
        if (SiteSession::getInstance()->isAuthorized()) {
            $user = SiteSession::getInstance()->getUser();
            $settings = json_decode($user->settings, true);
        }
        return $settings;
    }

    public static function set_settings($settings) {
        if (SiteSession::getInstance()->isAuthorized()) {
            $user = SiteSession::getInstance()->getUser();
            $user->settings = json_encode($settings);
            $user->hiddenSave();
            SiteSession::getInstance()->reloadData();
        }
        return self::get_settings();
    }


    /**
     * Доступные методы для сортировки данных
     */
    static $avalible_sort_params = array(
        'rat' => array('method' => 'orderDesc', 'field' => 'rating', 'title' => 'По рейтингу'),
        'abc' => array('method' => 'order', 'field' => 'title', 'title' => 'В алфавитном порядке'),
        'bca' => array('method' => 'orderDesc', 'field' => 'title', 'title' => 'В обратном алфавитном порядке'),
        'price' => array('method' => 'order', 'field' => 'price', 'title' => 'По возрастанию цены'),
        'ecirp' => array('method' => 'orderDesc', 'field' => 'price', 'title' => 'По убыванию цены'),
        'available' => array('method' => 'orderDesc', 'field' => 'a_', 'title' => 'По наличию'),
        'new' => array('method' => 'order', 'field' => 'new', 'title' => 'Сначала новинки'),
            //'rel' => array('method' => 'orderRelevant', 'field' => '', 'title' => 'По релевантности')
    );

    /**
     * Сортировка ORM данных
     */
    static function get_current_sort_params() {
        $meta_vars = &Meta::vars();

        $settings = self::get_settings();

        if (isset($meta_vars['sort']) && array_key_exists('sort', $meta_vars)) {
            $value = $meta_vars['sort'];
            if (array_key_exists($value, self::$avalible_sort_params)) {
                if (SiteSession::getInstance()->isAuthorized()) {
                    $settings['sort'] = $value;
                    self::set_settings($settings);
                } else {
                    setcookie('sort', $value, time() + 24 * 60 * 60 * 365, '/');
                }
                return self::$avalible_sort_params[$value];
            }
        } elseif (isset($settings['sort'])) {
            $value = isset($settings['sort']) ? $settings['sort'] : null;
            if (array_key_exists($value, self::$avalible_sort_params)) {
                return self::$avalible_sort_params[$value];
            }
        } else {
            $value = isset($_COOKIE['sort']) ? $_COOKIE['sort'] : null;
            if (array_key_exists($value, self::$avalible_sort_params)) {
                return self::$avalible_sort_params[$value];
            }
        }

        return self::$avalible_sort_params[reset(array_keys(self::$avalible_sort_params))];
    }

    /**
     * Данные для вывода в шаблоне страницы
     */
    static function get_sort_links_list() {
        $new_sort_list = array();

        $settings = self::get_settings();

        foreach (self::$avalible_sort_params as $key => $params) {
            $new_sort = array(
                "link" => Meta::set_uri_param("sort", $key),
                "value" => $key,
                "title" => $params['title'],
                "current" => false
            );

            if ($key == Meta::vars("sort")) {
                $new_sort['current'] = true;
            } elseif (isset($settings['sort']) && ($settings['sort'] == $key)) {
                $new_sort['current'] = true;
            }
            $new_sort_list[] = $new_sort;
        }

        return $new_sort_list;
    }



    static function sorted($items) {
        $sort = self::get_current_sort_params();

        switch ($sort['field']) {
            case 'a_':
                $prefix = ContactCity::GetCurrent()->prefix;
                $field = $sort['field'] . $prefix;
                break;
            case 'price':
                $field = self::getPriceType();
                break;
            default:
                $field = $sort['field'];
                break;
        }

        switch ($sort['method']) {
            case 'order':
                $items = $items->order($field);
                break;
            case 'orderDesc':
                $items = $items->orderDesc($field);
                break;
            case 'orderRelevant':
                /** @todo Разобрать вообще с полнотекстовым поиском и релевантной сортировкой */
                $items = $items->orderRelevant();
                break;
        }

        return $items;
    }

    /**
     * Доступные варианты количества элементов на страницу 
     */
    static $avalible_perpage_params = array(24, 36, 48);

    /**
     * Определение текущего значеиня "элементов на страницу"
     */
    static function get_current_perpage() {

        $settings = self::get_settings();

        $current_perpage = (int) Meta::vars("perpage");
        $current_perpage = in_array($current_perpage, self::$avalible_perpage_params) ? 
                $current_perpage : 
                self::$avalible_perpage_params[0];

        if ( Meta::vars("perpage")) {
            if (in_array($current_perpage, self::$avalible_perpage_params)) {
                setcookie('perpage', $current_perpage, time() + (60 * 60 * 24 * 365), '/');
                $perpage = $current_perpage;
            } elseif (array_key_exists('perpage', $_COOKIE)) {
                if (in_array($_COOKIE['perpage'], self::$avalible_perpage_params)) {
                    $perpage = $_COOKIE['perpage'];
                } else {
                    $perpage = self::$avalible_perpage_params[0];
                }
            }
            $settings['perpage'] = $perpage;
            self::set_settings($settings);
        } elseif ( isset($settings['perpage'] )) {
            $perpage = $settings['perpage'];
        } else {
            $perpage = self::$avalible_perpage_params[0];
        }

        return $perpage;
    }

    /**
     * Ссылки для вывода списка "количества эл на страницу"
     */
    static function get_perpage_link_list() {
        $new_perpage_list = array();
        $current_perpage = (int) Meta::vars("perpage");

        if (in_array($current_perpage, self::$avalible_perpage_params)) {
            $perpage = $current_perpage;
        } elseif (array_key_exists('perpage', $_COOKIE)) {
            if (in_array($_COOKIE['perpage'], self::$avalible_perpage_params)) {
                $perpage = $_COOKIE['perpage'];
            } else {
                $perpage = self::$avalible_perpage_params[0];
            }
        } else {
            $perpage = self::$avalible_perpage_params[0];
        }

        foreach (self::$avalible_perpage_params as $param) {
            $new_sort = array(
                "link" => Meta::set_uri_param("perpage", $param),
                "title" => $param,
                "current" => false
            );
                      
            if ($perpage == $param) {
                $new_sort['current'] = true;
            }
            $new_perpage_list[] = $new_sort;
        }

        return $new_perpage_list;
    }

    static function getPriceType() {
        $user = SiteSession::getInstance()->getUser();

        if (!is_null($user)) {
            $type = $user->type;
        } else {
            $type = "rozn";
        }

        return "price_{$type}";
    }

}
