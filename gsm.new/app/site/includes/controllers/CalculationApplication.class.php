<?

class CalculationApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
        array('~^/step2/?$~', 'form2'),
        array('~^/new_request/?$~', 'new_request'),
        array('~^/check_form1/?$~', 'check_form1'),
    );

    function index($vars, $page) {
        $cities_list = DeliveryCities(array("enabled" => true))->order("title")->all();
        $input_text = parse_ini_file("{$_SERVER['DOCUMENT_ROOT']}/static/txt_files/multilang/page-calculate/form1_" . NamiCore::getLanguage()->name . ".ini", true);

        print new View('calculation/page-calculate', compact('page', 'cities_list', 'input_text'));
        return true;
    }

    function form2($vars, $page) {
        $input_text = parse_ini_file("{$_SERVER['DOCUMENT_ROOT']}/static/txt_files/multilang/page-calculate/form2_" . NamiCore::getLanguage()->name . ".ini", true);

        print new View('calculation/page-calculate_form2', compact('page', 'input_text'));
        return true;
    }

    function new_request($vars, $page) {
        if (!Meta::isAjaxRequest()) {
            Builder::show404();
        }
        $result = false;

        if (Meta::vars("data") && Meta::vars("info") && Meta::vars("form")) {
            $order_form = Meta::vars("form");
            $order_info = Meta::vars("info");
            $order_data = Meta::vars("data");
            $new_order_params = array();
            $fields_as_html = "";

            $input_text = parse_ini_file("{$_SERVER['DOCUMENT_ROOT']}/static/txt_files/multilang/page-calculate/form2_ru.ini", true);

            $delivery_type = mb_strcut($order_form, mb_strrpos($order_form, "_") + 1);
            $new_order_params["type"] = $input_text[1]["values"][$delivery_type - 1];

            foreach ($order_data as $field_data) {
                $field_title = $input_text[mb_strcut($field_data['name'], mb_strrpos($field_data['name'], "_") + 1)]['title'];
                $field_value = $field_data['value'];
                if ($field_value == ""){
                    $field_value = "<i>не указано</i>";
                }

                $fields_as_html .= "{$field_title}: {$field_value}<br />";
            }

            $new_order_params["name"] = $order_info['name'];
            $new_order_params["company"] = $order_info['company'];
            $new_order_params["phone"] = $order_info['phone'];
            $new_order_params["email"] = $order_info['email'];
            $new_order_params['text'] = $fields_as_html;

            $new_order = DeliveryOrders()->create($new_order_params);

            // пишем письмо админу
            $mail = PhpMailerLibrary::create();
            $addresses = Config::get('calculation.email');
            foreach (explode(',', $addresses) as $address) {
                $mail->AddAddress(trim($address));
            }
            $mail->Subject = "Заявка на расчет с сайта {$_SERVER['HTTP_HOST']}";
            $mail->Body = new View('calculation/mail/new_order', array('order' => $new_order));
            $mail->IsHTML(true);
            @$mail->Send();

            $result = true;
        }


        $json_data = array(
            'result' => $result,
        );
        echo json_encode($json_data);
        return true;
    }

    function check_form1($vars, $page) {

        if (!Meta::isAjaxRequest()) {
            Builder::show404();
        }

        $result = false;
        $city_from_title = false;
        $city_to_title = false;
        $duration = "";
        $total_price = "";

        $city_from_id = Meta::vars("city_from");
        $city_to_id = Meta::vars("city_to");
        $weight = Meta::vars("weight");
        $volume = Meta::vars("volume");

        if (($city_from_id && $city_to_id && $weight && $volume)) {
            $city_from = DeliveryCities()->get(array("enabled" => true, "id" => $city_from_id));
            $city_to = DeliveryCities()->get(array("enabled" => true, "id" => $city_to_id));

            $city_from_title = $city_from->title;
            $city_to_title = $city_to->title;

            $delivery_rule = DeliveryRules()->get(array("city_from" => $city_from, "city_to" => $city_to, "enabled" => true));

            if ($delivery_rule) {
                $volume_price = false;
                $weight_price = false;

                foreach (json_decode($delivery_rule->weight_rule) as $rule) {
                    if ($weight > $rule[0] && $weight <= $rule[1]) {
                        $weight_price = $rule[2] * $weight;
                    }
                }

                foreach (json_decode($delivery_rule->volume_rule) as $rule) {
                    if ($volume > $rule[0] && $volume <= $rule[1]) {
                        $volume_price = $rule[2] * $volume;
                    }
                }

                if ($volume_price && $weight_price) {
                    $result = true;
                }

                if ($weight_price > $volume_price) {
                    $total_price = $weight_price;
                } else {
                    $total_price = $volume_price;
                }

                if ($total_price < $delivery_rule->min_price) {
                    $total_price = $delivery_rule->min_price;
                }
                $duration = $delivery_rule->duration;
            }
        }

        $json_data = array(
            'result' => $result,
            'city_from' => $city_from_title,
            'city_to' => $city_to_title,
            'weight' => $weight,
            'volume' => $volume,
            'duration' => $duration,
            'total_price' => $total_price
        );
        echo json_encode($json_data);
        return true;
    }

}