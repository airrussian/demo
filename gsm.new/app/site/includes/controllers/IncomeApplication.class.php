<?php

class IncomeApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
    );

    function index($vars, $page) {

        if (Meta::isAjaxRequest()) {
            $date = Meta::vars('date');
            // $offset = Meta::vars('offset');
            if ($date) {
                $items = CatalogIncome::getCategoryDate($date);                
                header("Content-type: application/json; charset=utf-8");
                print json_encode(array(
                    'success' => true,
                    'items' => (string) new View("income/block-category_date", compact('items', 'date')),
                    'next' => CatalogIncome::nextDate($date)
                ));
                return true;
            } else {
                $dates = self::GetDates($offset);
                echo new View("income/block-dates", compact('dates'));
            }
            exit();
        }
        
        $date = CatalogIncome::nextDate(date("Y-m-d"));
        $items = CatalogIncome::getCategoryDate($date);               
        print new View("income/page-index", compact('items', 'date', 'page'));
        return true;
    }

    static function GetEntry($date, $offset = 0) {

        $count = Config::get("income.entries");

        $result = array();

        $result['items'] = CatalogIncomes()
                ->filter(array(
                    'date_day' => $date,
                    'city' => ContactCity::GetCurrent()->prefix
                ))
                ->order("id")
                ->limit($count, $offset);

        $result['count'] = CatalogIncomes()
                        ->filter(array(
                            'date_day' => $date,
                            'city' => ContactCity::GetCurrent()->prefix
                        ))->count();

        return $result;
    }

    static function GetDates($offset = 0) {
        $city_prefix = ContactCity::GetCurrent()->prefix;

        $user = SiteSession::getInstance()->getUser();
        if ($user) {
            $price_type = "price_" . $user->type;
        } else {
            $price_type = "price_rozn";
        }

        $days_count = Config::get("income.dates");

        $pathcache = $_SERVER['DOCUMENT_ROOT'] . "/static/uploaded/temp/income_cache";
        if (!file_exists($pathcache)) {
            mkdir($pathcache, 0777);
        }
        $filename = "{$city_prefix}_{$price_type}_{$days_count}_{$offset}.cache";

        $filecache = $pathcache . "/" . $filename;

        if (file_exists($filecache) && (filectime($filecache) > time() - 3600)) {

            echo "<!-- income from cache -->";

            $return = unserialize(file_get_contents($filecache));
        } else {
            $cursor = NamiCore::getInstance()->getBackend()->getCursor();
            $sql = ""
                    . "SELECT DISTINCT catalogincome.date_day "
                    . "FROM catalogincome "
                    . "LEFT JOIN catalogentry ON catalogincome.entry = catalogentry.id "
                    . "WHERE catalogincome.city='" . $city_prefix . "' "
                    . "AND catalogentry." . $price_type . " > 0 "
                    . "ORDER BY catalogincome.date_day DESC "
                    . "LIMIT " . $days_count . " OFFSET " . $offset;
            $cursor->execute($sql);

            $dates = $cursor->fetchAll();

            $return = array();
            foreach ($dates as $row) {
                $return[$row['date_day']] = self::GetEntry($row['date_day']);
            }
            file_put_contents($filecache, serialize($return));
        }
        return $return;
    }

}
