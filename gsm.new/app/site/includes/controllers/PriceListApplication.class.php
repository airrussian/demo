<?php

class PriceListApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
    );

    function index($vars, $page) {
        
        $city_id = ContactCity::GetCurrent()->id;
        
        $pricelist = PriceLists()->filter(array('enabled' => true, 'city' => $city_id))->first();
        
        print new View('pricelist/page-index', compact('page', 'pricelist'));
        return true;
    }

}
