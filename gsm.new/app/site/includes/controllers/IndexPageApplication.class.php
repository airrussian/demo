<?

class IndexPageApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
    );

    function index($vars, $page, $uri) {
        
        $page = Page::replace($page);
        
        print new View('home/page-index', compact('page'));
        return true;
    }

}
