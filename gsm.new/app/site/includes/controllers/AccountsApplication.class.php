<?php

class AccountsApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'profile'),
        array('~^/auth/?$~', 'auth'),
        array('~^/registration/?$~', 'registration'),
        array('~^/loginchangecity/?$~', 'loginchangecity'),
        array('~^/login/?$~', 'login'),
        array('~^/logout/?$~', 'logout'),
        array('~^/address/?(?P<action>\w+)?/?$~', 'address'),
        array('~^/editpassword/?$~', 'edit_password'),
        array('~^/edit/info/?$~', 'edit_info'),
        //восстановление пароля
        array('~^/password_recover/?$~', 'password_recover'),
        array('~^/password_change/(?P<code>[a-fA-F0-9]{32})?/?$~', 'password_change'),
        array('~^/profile/history/?$~', 'profile_history'),
        array('~^/profile/history/(?P<id>\d+)/?$~', 'profile_order'),
        array('~^/edit/(?P<field>.+?)/?$~', 'edit'),
        array('~^/profile/preorders(?:/page\d+)?/?$~', 'profile_preorders')
    );
    
    public function auth($vars, $page) {
        
        if (SiteSession::getInstance()->isAuthorized()) {
            header("Location: " . Builder::getAppUri("AccountsApplication"));
            exit();
        } 
        
        print new View("accounts/page-auth", compact('page'));
        return true;
    }

    // Регистрация
    function registration($vars, $page) {
        if (Meta::isAjaxRequest()) {
            $validator_rules = array(
                'email' => array('trim', 'required', 'email', 'useremailavailable', array('length', 1, 100)),
                'login' => array('trim', 'required', 'userloginavailable', array('length', 1, 100)),
                'password' => array('trim', 'required', array('length', 4, 30)),
                'password2' => array('trim', 'required', array('matchfield', 'password')),
            );

            $validator_messages = array(
                'email.email' => 'введите корректный адрес электронной почты',
                'email.useremailavailable' => 'данный адрес электронной почты уже используется',
                'login.userloginavailable' => 'такой логин уже используется, придумайте другой',
                'password.length' => 'длина пароля должна быть от 4 до 30 символов',
                'password2.matchfield' => 'введенные пароли не совпадают',
            );

            $validator = new DataValidator($validator_rules, $validator_messages);
            $status = $validator->process(Meta::vars());
            $msg = "";

            if ($status->ok) {

                $city = ContactCity::GetCurrent();

                $data = $status->data;

                $data["enabled"] = true;
                $data["password"] = md5($status->data['password']);
                $data["name"] = $data["login"];
                $data["city"] = $city->prefix;

                $user = SiteUsers()->create($data);

                $user->save();

                // отправим письмо с данными
                $mail = PhpMailerLibrary::create();
                $mail->AddAddress($user->email);
                $mail->Subject = "Вы успешно зарегистрировались на сайте {$_SERVER['HTTP_HOST']}";
                $mail->Body = new View('_mails/new_site_user', compact('status', 'city'));
                $mail->IsHTML();
                @$mail->Send();


                // Проверяем предзаказы 
                $items = Preorders()->filter(['email' => $user->email])->all();
                foreach ($items as $item) {
                    $item->user = $user;
                    $item->hiddenSave();
                }
            }

            $json = json_encode(array($status, $msg));

            header('Content-Type: application/json');
            print $json;
            return true;
        } else {
            $page->title = "Регистрация на сайте";

            print new View('accounts/page-registration', compact('page'));
            return true;
        }
    }

    // В этот метод может попасть только при переходе через город REFERER канает
    function loginchangecity($vars, $page) {
        if (SiteSession::getInstance()->isAuthorized()) {
            if (isset($_SERVER['HTTP_REFERER'])) {
                $r = parse_url($_SERVER['HTTP_REFERER']);
                $url = $r['path'];
            } else {
                $url = "/";
            }
            header("Location: $url");
            exit();
        }

        if (Meta::vars("hash")) {
            if ($u = SiteUsers()->get(array('cross_login' => Meta::vars("hash")))) {
                SiteSession::getInstance()->login($u->login, $u->password, true);
                if (isset($_SERVER['HTTP_REFERER'])) {
                    $r = parse_url($_SERVER['HTTP_REFERER']);
                    $url = $r['path'];
                } else {
                    $url = "/";
                }
                header("Location: $url");
                exit();
            }
        }
        header("Location: /");
        exit();
    }

    /**
     * Авторизация пользователей на сайте
     */
    function login($vars, $page) {
        // Если авторизован, переадресуем его на ... 
        if (SiteSession::getInstance()->isAuthorized()) {
            // При первой авторизации отправляем его на страницу профиля и предлагаем ему заполнить контактные данные
            if (SiteSession::getInstance()->getUser()->first_login) {
                $url = Builder::getAppUri('AccountsApplication');
            } else { // При последующих авторизациях отправляем его на туже страницу с которой он логинился.                
                $url = isset($_COOKIE['referer']) ? $_COOKIE['referer'] : "/";
            }
            setcookie("referer", NULL, time() - 10000, "/");
            header("Location: " . $url);
            exit();
        }

        setcookie("referer", isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "/", time() + 100000, "/");

        $page->title = "Авторизация на сайте";

        $validator_rules = array(
            'login_form-login' => array('trim', 'required', array('length', 1, 100)),
            'login_form-password' => array('trim', 'required', array('length', 1, 100)),
        );
        $validator_messages = array();
        $validator = new DataValidator($validator_rules, $validator_messages);
        $status = $validator->process(Meta::vars());

        $msg = "";
        if (!$status->ok) {
            $msg = "Пожалуйста, введите логин и пароль";
        }

        $data = $status->data;
        $auth = SiteSession::getInstance()->auth($data['login_form-login'], $data['login_form-password']);

        if (!$auth['success']) {
            // Не удалось авторизоваться 
            $msg = $auth['error'];
            $status->ok = false;
        } else {

            /* $user = SiteSession::getInstance()->getUser();
              $cart = $user->cart_data */

            $user = SiteSession::getInstance()->getUser();
            // Проверяем предзаказы 
            $items = Preorders()->filter(['email' => $user->email])->all();
            foreach ($items as $item) {
                $item->user = $user;
                $item->hiddenSave();
            }

            if (Meta::isAjaxRequest()) {
                $status->ok = true;
                $msg = (string) new View("accounts/block-user_login");
            } else {
                header("Location: /");
                exit();
            }
        }

        if (Meta::isAjaxRequest()) {
            $json = json_encode(array($status, $msg));
            header('Content-Type: application/json');
            print $json;
            return true;
        }

        print new View('accounts/page-login', compact('page', 'msg', 'data'));
        return true;
    }

    function address($vars, $page) {
        if (true || Meta::isAjaxRequest()) {
            $user = SiteSession::getInstance()->getUser();
            $userdb = SiteUsers()->get(array('id' => $user->id));

            switch ($vars->action) {
                case 'addaddress':
                    $validator_rules = array(
                        'city' => array('trim', 'required', array('length', 1, 50)),
                        'index' => array('trim', 'required', array('length', 6, 6)),
                        'address' => array('trim', 'required', array('length', 1, 1000)),
                    );

                    $validator_messages = array();

                    $validator = new DataValidator($validator_rules, $validator_messages);
                    $status = $validator->process(Meta::vars());

                    if ($status->ok) {
                        $addresses_delivery = array();
                        if ($userdb->addresses_delivery) {
                            $addresses_delivery = json_decode($userdb->addresses_delivery, true);
                        }

                        $addresses_delivery[] = array(
                            'city' => isset($status->data['city']) ? $status->data['city'] : "",
                            'index' => $status->data['index'],
                            'street' => $status->data['address']
                        );

                        $userdb->addresses_delivery = json_encode($addresses_delivery);
                        $userdb->save();

                        $msg = (string) $text_block->text;
                        SiteSession::getInstance()->reloadData();
                    } else {
                        $msg = "Пожалуйста, корректно заполните отмеченные поля";
                    }

                    $json = json_encode(array($status, $msg));
                    echo $json;
                    return true;
                    break;
                case 'editaddress':
                    $validator_rules = array(
                        'city' => array('trim', 'required', array('length', 1, 50)),
                        'index' => array('trim', 'required', array('length', 6, 6)),
                        'address' => array('trim', 'required', array('length', 1, 1000)),
                            //'id' => array('trim', 'required', array('length', 1, 10))
                    );

                    $validator_messages = array();

                    $validator = new DataValidator($validator_rules, $validator_messages);
                    $status = $validator->process(Meta::vars());

                    if ($status->ok) {
                        $addresses_delivery = array();
                        if ($userdb->addresses_delivery) {
                            $addresses_delivery = json_decode($userdb->addresses_delivery, true);
                        }

                        $addresses_delivery[$status->data['id']] = array(
                            'city' => isset($status->data['city']) ? $status->data['city'] : "",
                            'index' => $status->data['index'],
                            'street' => $status->data['address']
                        );

                        $userdb->addresses_delivery = json_encode($addresses_delivery);
                        $userdb->save();

                        $msg = (string) $text_block->text;
                        SiteSession::getInstance()->reloadData();
                    } else {
                        $msg = "Пожалуйста, корректно заполните отмеченные поля";
                    }

                    $json = json_encode(array($status, $msg));
                    echo $json;
                    return true;
                    break;
                case 'get':
                    $addresses = json_decode($userdb->addresses_delivery, true);
                    echo new View("accounts/block-address", compact('addresses'));
                    return true;
                    break;
                case 'del':
                    $addresses = json_decode($userdb->addresses_delivery, true);
                    unset($addresses[Meta::vars('id')]);
                    $userdb->addresses_delivery = json_encode($addresses);
                    $userdb->save();
                    SiteSession::getInstance()->reloadData();
                    return true;
                    break;
            }
        }
    }

    function logout($vars, $page) {
        if (!SiteSession::getInstance()->isAuthorized()) {
            header("Location: /", true);
        }

        SiteSession::getInstance()->logout();
        header("Location: /", true);
    }

    function profile($vars, $page) {
        if (!SiteSession::getInstance()->isAuthorized()) {
            Builder::show403();
        }

        $user = SiteSession::getInstance()->getUser();

        if (Meta::isAjaxRequest()) {
            $user->subscribe = Meta::vars('subscribe') ? TRUE : FALSE;
            $user->subscribe_order_status = Meta::vars('subscribe_order_status') ? TRUE : FALSE;
            $user->save();
            SiteSession::getInstance()->reloadData();
            print "OK";
            return true;
        }

        $page->title = "Личный кабинет";

        print new View('accounts/page-profile', compact('page', 'user'));
        return true;
    }

    function edit_password($vars, $page) {
        if (!SiteSession::getInstance()->isAuthorized()) {
            Builder::show403();
        }

        if (Meta::isAjaxRequest()) {
            $validator_rules = array(
                'old_pass' => array('trim', 'required', 'is_current_password', array('length', 1, 100)),
                'new_pass' => array('trim', 'required', array('length', 1, 100)),
                'new_pass2' => array('trim', 'required', array('length', 1, 100), array('matchfield', 'new_pass')),
            );

            $validator_messages = array(
                'old_pass' => 'введите текущий пароль',
                'old_pass.is_current_password' => 'введите правильный текущий пароль',
                'new_pass' => 'введите новый пароль',
                'new_pass2' => 'введите подтверждение нового пароля',
                'new_pass2.matchfield' => 'введенные пароли не совпадают',
            );

            $validator = new DataValidator($validator_rules, $validator_messages);
            $status = $validator->process(Meta::vars());
            $msg = "";

            if ($status->ok) {
                $data = $status->data;

                SiteSession::getInstance()->reloadData();
                $user = SiteSession::getInstance()->getUser();

//данные — ок. обновляем.
                $user_db = SiteUsers()->get($user->id);
                $user_db->password = md5($data['new_pass']);
                $user_db->save();

                $msg = "Ваш пароль успешно изменен";
            } else {
                $msg = "";
            }

            $json = json_encode(array($status, $msg));
            header('Content-Type: application/json');
            echo $json;
        } else {
            Builder::show404();
        }

        return true;
    }

    function edit_info($vars, $page) {
        if (!SiteSession::getInstance()->isAuthorized()) {
            Builder::show403();
        }

        if (Meta::isAjaxRequest()) {

            $validator_rules = array(
                'name' => array('trim', 'required', array('length', 1, 1000)),
            );

            $validator_messages = array();

            $validator = new DataValidator($validator_rules, $validator_messages);
            $status = $validator->process(Meta::vars());
            $msg = "";

            if ($status->ok) {
                $data = $status->data;

                $user = SiteSession::getInstance()->getUser();

                $user_db = SiteUsers()->get($user->id);
                $user_db->name = $data['name'];
                $user_db->save();

                SiteSession::getInstance()->reloadData();
            } else {

                $msg = "Пожалуйста, заполните отмеченные поля";
            }

            $json = json_encode(array($status, $msg));
            header('Content-Type: application/json');
            echo $json;
        } else {
            print new View('accounts/page-edit_info', compact('page'));
        }

        return true;
    }

    /**
      Страница восстановления пароля (шаг 1)
      Отсылаем человеку на почту ссылку на страницу со сменой пароля (password_change).
     */
    function password_recover($vars, $page) {
        if (Meta::isAjaxRequest()) {
            $validator_rules = array(
                'email' => array(
                    'trim',
                    'required',
                    'email',
                    'useremailexist',
                    array('length', 1, 100)
                ),
            );

            $validator_messages = array(
                'email' => 'Укажите адрес электронной почты',
                'email.email' => 'Укажите верный адрес электронной почты',
                'email.useremailexist' => 'Пользователь с данным email не найден. Возможно при регистрации на нашем сайте вы указывали другой адрес электронной почты.',
            );

            $validator = new DataValidator($validator_rules, $validator_messages);
            $status = $validator->process(Meta::vars());
            $msg = "";

            if ($status->ok) {
                $data = $status->data;

                $user = SiteUsers()->get(array('email' => $data['email']));
                if ($user) {
                    if (!$user->enabled) {
                        $msg = "Пользователь с данным email заблокирован. Обратитесь к администратору сайта.";
                    } else {

//генерим код для создания временной сессии
                        $user->password_recover_code = md5($user->id . rand(1, 1000) . time());
// запомним время создания кода (он будет рабочим только 3 часа)
                        $user->password_recover_code_created = time();
                        $user->save();

// отправим письмо
                        $mail = PhpMailerLibrary::create();
                        $mail->AddAddress($user->email);
                        $mail->Subject = "Восстановление пароля сайта {$_SERVER['HTTP_HOST']}";
                        $mail->Body = new View('_mails/site_user_password_recover', compact('user'));
                        $mail->IsHTML();
                        @$mail->Send();

                        $msg = "На ваш email отправлено письмо, содержащее ссылку, перейдя по которой вы сможете изменить свой пароль.";
                    }
                }
            }

            $json = json_encode(array($status, $msg));
            header('Content-Type: application/json');
            print $json;
        } else {

            $page->title = "Восстановление логина и пароля";
            print new View('accounts/page-password_recover', compact('page'));
        }

        return true;
    }

    /**
      Страница восстановления пароля (шаг 2)

      Сюда человек приходит по ссылке, которую получил по почте из password_recover,
      для того, чтобы установить новый пароль. Ему дается одноразовая сессия и возможность
      один раз воспользоваться формой изменения пароля.
      Эта страница остается доступной в течение 3-х часов после отправки письма.
     */
    function password_change($vars, $page) {
        $code_error = false;
//3 часа
        $live_time = 3 * 60 * 60;

        if ($vars->code) {
            $user = SiteUsers()->get(array('password_recover_code' => $vars->code));
        } else {
            $user = SiteSession::getInstance()->getUser();
        }

        if ($user) {
            SiteSession::getInstance()->login($user->login, $user->password, false);

            if (time() > (strtotime($user->password_recover_code_created) + $live_time)) {
//ссылка старая, удаляем код
                $user->password_recover_code = null;
                $user->password_recover_code_created = null;
                $user->save();

                $code_error = true;
            }
        } else {
//ссылка битая
            $code_error = true;
        }

        if (Meta::isAjaxRequest()) {
            if ($code_error) {
                return true;
            }

            $validator_rules = array(
                'new_password' => array('trim', 'required', array('length', 1, 100)),
                'new_password_confirm' => array('trim', 'required', array('length', 1, 100), array('matchfield', 'new_password')),
            );

            $validator_messages = array(
                'new_password' => 'введите новый пароль',
                'new_password_confirm' => 'введите подтверждение нового пароля',
                'new_password_confirm.matchfield' => 'введенные пароли не совпадают',
            );

            $validator = new DataValidator($validator_rules, $validator_messages);
            $status = $validator->process(Meta::vars());
            $msg = "";

            if ($status->ok) {
//обновление данных
// удалялем код из базы
                $user->password_recover_code = null;
                $user->password_recover_code_created = null;
// ставим новый пароль
                $user->password = md5($status->data['new_password']);
                $user->save();
                SiteSession::getInstance()->reloadData();

                $msg = "Ваш пароль успешно изменен";
            } else {
                $msg = reset($status->errors)->message;
            }

            $json = json_encode(array($status, $msg));
            header('Content-Type: application/json');
            print $json;
        } else {

            $page->title = "Восстановление пароля";
            print new View('accounts/page-password_change', compact('page', 'code_error'));
        }

        return true;
    }

    function profile_history($vars, $page) {
        if (!SiteSession::getInstance()->isAuthorized()) {
            return false;
        }

        $orders = SiteOrders(array('user' => SiteSession::getInstance()->getUser()->id))->orderDesc("date")->all();

        $page->title = "История заказов";
        print new View('accounts/page-profile_history', compact('page', 'orders'));

        return true;
    }

    function profile_order($vars, $page) {

        if (!SiteSession::getInstance()->isAuthorized()) {
            return false;
        }

        $crumbs_pages[] = array(
            'title' => 'История заказов',
            'uri' => Builder::getAppUri('AccountsApplication') . 'profile/history/'
        );

        $order = SiteOrders()->get(array('id' => $vars->id));
        $items = SiteOrderItems()->filter(array('order' => $order))->all();

        $page->title = "Заказ №{$order->id} от {$order->date}";
        print new View('accounts/page-profile_history_order', compact('page', 'order', 'items', 'crumbs_pages'));

        return true;
    }

    function edit($vars, $page) {

        $user = SiteSession::getInstance()->getUser();

        if ($vars->field == 'phone') {
            $user->phone = Meta::vars("value");
            print $user->phone;
        }
        if ($vars->field == 'contact_personal') {
            $user->contact_personal = Meta::vars("value");
            print $user->contact_personal;
        }

        $user->hiddenSave();

        SiteSession::getInstance()->reloadData();

        return true;
    }

    public function profile_preorders($vars, $page) {

        $user = SiteSession::getInstance()->getUser();

        $items = Preorders()->filter(array('user' => $user))->orderDesc("date");
        $paginator = new NamiPaginator($items, "_blocks/site-paginator", 20);

        print new View("accounts/page-preorders", compact('page', 'paginator'));
        return true;
    }

}
