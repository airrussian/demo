<?

class PublicationsApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?(?:/page\d+)?/?$~', 'category'),
        array('~^/(?P<category_name>\w+)/?(?:/page\d+)?/?$~', 'category'),
        array('~^/(?P<category_name>\w+)/post-(?P<post_id>\d+)/?$~', 'publication'),
    );

    function category($vars, $page) {
        if ($vars->category_name) {
            $category = PublicationCategories()->get_or_404(array("enabled" => true, "name" => $vars->category_name));
            $items = Publications(array('enabled' => true, "category" => $category))->orderDesc("date");
        } else {
            $items = Publications(array('enabled' => true, "category__enabled" => true))->orderDesc("date");
        }

        $paginator = new NamiPaginator($items, '_blocks/site-paginator', Config::get('publications.items_per_page'));
        print new View('publications/page-category', compact('page', 'paginator'));
        return true;
    }

    function publication($vars, $page) {
        $publication = Publications()->get_or_404(array(
            "id" => $vars->post_id,
            "enabled" => true,
            "category__enabled" => true,
            "category__name" => $vars->category_name,
        ));

        $page->title = $publication->title;

        print new View('publications/page-item', compact('page', 'publication'));
        return true;
    }

}
