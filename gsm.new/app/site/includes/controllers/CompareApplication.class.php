<?

class CompareApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
    );

    function index($vars, $page) {
        $compare_list = false;
        $compare_items = self::get_compare_items();

        /*
         * Формируем массив вида
         * 
         * товары для сравнения
         *      |
         *      |--категория 1--|
         *      |               |--items-----|
         *      |               |            |- элемент 1
         *      |               |            |- элемент 2
         *      |               |            |- элемент 3
         *      |               |            |- элемент 4
         *      |               |            |- элемент 5
         *      |               |            |- элемент 6
         *      |               |
         *      |               |
         *      |               |--category--|
         *      |               |
         *      |               |
         *      |               |--fields----|
         *      |                            |--field 1
         *      |                            |--field 2
         *      |                            |--field 3
         *      |
         *      |--категория 2--|...
         *      |--категория 3--|...
         */
        if ($compare_items) {
            foreach ($compare_items as $item) {
                $entry = CatalogEntries(array('enabled' => true))->get_or_404($item);
                if ($entry) {
                    if (isset($compare_list[$entry->category->id])) {
                        $compare_list[$entry->category->id]['items'][] = $entry;
                    } else {
                        $fields = array();
                        $fields_db = CatalogFieldSetFields(array('fieldset' => $entry->category->fieldset))->follow(1)->order('sortpos')->all();
                        if ($fields_db) {
                            foreach ($fields_db as $field_db) {
                                $fields[] = array('name' => $field_db->field->name, 'title' => $field_db->field->title);
                            }
                        }
                        $compare_list[$entry->category->id] = array('items' => array($entry), 'fields' => $fields, 'category' => $entry->category);
                    }
                }
            }
        }
        
        print new View('compare/page-compare', array(
                    'page' => $page,
                    'compare_list' => $compare_list
                ));
        return true;
    }

    static function get_compare_items() {
        if (array_key_exists('compare', $_COOKIE) && $_COOKIE['compare']) {
            $compare = json_decode(stripslashes($_COOKIE['compare']), true);
            if (array_key_exists('items', $compare)) {
                return $compare['items'];
            }
        }
        return null;
    }

    static function is_compared($id) {
        if (self::get_compare_items()) {
            foreach (self::get_compare_items() as $item) {
                if ($item['id'] == $id) {
                    return true;
                }
            }
        }
        return false;
    }

}