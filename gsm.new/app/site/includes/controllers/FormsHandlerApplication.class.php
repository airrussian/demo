<?

/**
 * Класс для обработки форм
 */
class FormsHandlerApplication extends UriConfApplication {

    protected $uriconf = array(
        array('~^/?$~', 'index'),
        array('~^/(?P<action>\w+)/?~', 'call_action')
    );

    function index($vars, $page) {
        Builder::show404();
        return true;
    }

    function call_action($vars) {
        $this->check_ajax_request();

        header('Content-Type: application/json');
        echo $this->{$vars->action}();
        return true;
    }

    function check_ajax_request() {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            throw new Http404();
        }
    }

    static $feedback_themes = array(
        'predlojenie' => array(
            'value' => 'Вопросы по товарам и заказам',
            'theme' => "Вопросы по товарам и заказам"
        ),
        'otzyv' => array(
            'value' => 'Отзывы и предложения',
            'theme' => "Отзывы и предложения"
        )
    );

    function profilechange() {
        $validator_rules = array(
            'message' => array('trim', 'required', array('length', 1, 1000)),
        );

        if (!$user = SiteSession::getInstance()->getUser()) {
            exit("Error, no user auth");
        }

        $validator_messages = array();

        $validator = new DataValidator($validator_rules, $validator_messages);
        $status = $validator->process(Meta::vars());

        if ($status->ok) {
            $data = $status->data;
            $mail = PhpMailerLibrary::create();
            foreach (explode(",", Config::get('profilechange.email')) as $email) {
                $mail->AddAddress(trim($email));
            }

            $mail->Subject = "Запрос на изменение персональных данных на сайте " . $_SERVER['HTTP_HOST'];
            $mail->Body = new View('_mails/new_profilechange', compact("data", "user"));
            $mail->IsHTML(true);
            @$mail->Send();

            $text_block = TextBlocks()->getOrCreate(array('name' => 'Изменение персональных данных: форма отправлена', 'rich' => '1'));
            $msg = (string) $text_block->text;
        } else {
            $msg = "Пожалуйста, корректно заполните отмеченные поля";
        }

        $json = json_encode(array($status, $msg));
        return $json;
    }

    function feedback() {
        $validator_rules = array(
            'feedback-email' => array('trim', 'required', 'email', array('length', 1, 100)),
            'feedback-name' => array('trim', 'required', array('length', 1, 100)),
            'feedback-phone' => array('trim', 'required', 'phone', array('length', 3, 40)),
            'feedback-theme' => array('trim', 'required'),
            'feedback-message' => array('trim', 'required'),
            'files' => array()
        );


        $validator_messages = array();

        $validator = new DataValidator($validator_rules, $validator_messages);
        $status = $validator->process(Meta::vars());

        if ($status->ok) {

            $data = $status->data;

            $city = ContactCity::GetCurrent();
            $emails = $city->{"feedback_sendToEmail_{$data['feedback-theme']}"};
            if ($emails) {
                $emails = explode(",", $emails);
            } else {
                $emails = $city->feedbackemail ? explode(",", $city->feedbackemail) : false;
            }

            if (!empty($emails)) {
                $mail = PhpMailerLibrary::create();
                foreach ($emails as $email) {
                    $mail->AddAddress(trim($email));
                }
                $mail->Subject = self::$feedback_themes[$data['feedback-theme']]['theme'] . $_SERVER['HTTP_HOST'];
                $mail->Body = (string) new View('_mails/new_feedback', compact("data"));
                $mail->IsHTML(true);
                @$mail->Send();
            }

            $text_block = TextBlocks()->getOrCreate(array('name' => 'Обрантый звонок: форма отправлена', 'rich' => '1'));
            $msg = (string) $text_block->text;
        } else {
            $msg = "Пожалуйста, корректно заполните отмеченные поля";
        }

        $json = json_encode(array($status, $msg));
        return $json;
    }

    /**
     * @todo не тестировался только прототип!!! 
     * @return type@
     */
    function preorder() {
        $validator_rules = array(
            'preorder-entry' => array('trim', 'required', array('length', 1, 10)),
            'preorder-name' => array('trim', 'optional', array('length', 1, 255)),
            'preorder-city' => array('trim', 'required', array('length', 1, 100)),
            'preorder-email' => array('trim', 'required', 'email', array('length', 1, 255)),
        );

        $validator_messages = array();

        $validator = new DataValidator($validator_rules, $validator_messages);
        $status = $validator->process(Meta::vars());

        $user = SiteSession::getInstance()->getUser();

        if ($status->ok) {

            $entry = CatalogEntries()->get(array("oneC_id" => $status->data['preorder-entry']));

            $data = array(
                "Название товара" => $entry->title,
                "Ссылка" => 'http://' . $_SERVER['HTTP_HOST'] . '/' . $entry->full_uri,
                "Имя" => $status->data['preorder-name'],
                "Город" => $status->data['preorder-city'],
                "Email" => $status->data['preorder-email']
            );

            if (!Preorders()->get(['user' => $user, 'entry' => $entry, 'done' => false])) {
                $preorder = Preorders()->create(array(
                    'entry' => $entry->id,
                    'name' => $status->data['preorder-name'],
                    'city' => $status->data['preorder-city'],
                    'email' => $status->data['preorder-email'],
                    'user' => $user
                ));
                $mail = PhpMailerLibrary::create();
                foreach (explode(",", Config::get('preorder.email')) as $email) {
                    $mail->AddAddress(trim($email));
                }
                $mail->Subject = "Предзаказа с сайта {$_SERVER['HTTP_HOST']}";
                $mail->Body = new View('_mails/new_preorder', compact("data"));
                $mail->IsHTML(true);
                @$mail->Send();
                $text_block = TextBlocks()->getOrCreate(array('name' => 'Предзаказ: форма отправлена', 'rich' => '1'));
                $msg = (string) $text_block->text;
            } else {
                $msg = "Вы уже подписаны на поступление этого товара";
                $status->ok = false;
            }
        } else {
            $msg = "Пожалуйста, корректно заполните отмеченные поля";
        }

        $json = json_encode(array($status, $msg));
        return $json;
    }

    function onorder() {
        $validator_rules = array(
            'onorder-entry' => array('trim', 'required', array('length', 1, 10)),
            'onorder-name' => array('trim', 'required', array('length', 1, 255)),
            'onorder-city' => array('trim', 'required', array('length', 1, 100)),
            'onorder-email' => array('trim', 'required', 'email', array('length', 1, 255)),
        );

        $validator_messages = array();

        $validator = new DataValidator($validator_rules, $validator_messages);
        $status = $validator->process(Meta::vars());

        $user = SiteSession::getInstance()->getUser();

        if ($status->ok) {

            if (!$user && SiteUsers()->get(['email' => $status->data['onorder-email']])) {
                $msg = "Данный email используется, пожалуйста авторизуйтесь.";
                $status->ok = false;
            } else {

                $entry = CatalogEntries()->get(array("oneC_id" => $status->data['onorder-entry']));

                $data = array(
                    "Название товара" => $entry->title,
                    "Артикул товара"    => $entry->oneC_id,
                    "Ссылка" => 'http://' . $_SERVER['HTTP_HOST'] . '/' . $entry->full_uri,
                    "Имя" => $status->data['onorder-name'],
                    "Город" => $status->data['onorder-city'],
                    "Email" => $status->data['onorder-email']
                );

                if (!Onorders()->get(['user' => $user, 'entry' => $entry, 'done' => false])) {
                    $onorder = Onorders()->create(array(
                        'entry' => $entry->id,
                        'name' => $status->data['onorder-name'],
                        'city' => $status->data['onorder-city'],
                        'email' => $status->data['onorder-email'],
                        'user' => $user
                    ));
                    $mail = PhpMailerLibrary::create();
                    foreach (explode(",", Config::get('onorder.email')) as $email) {
                        $mail->AddAddress(trim($email));
                    }
                    $mail->Subject = "Заказ товара с сайта {$_SERVER['HTTP_HOST']}";
                    $mail->Body = new View('_mails/new_onorder', compact("data"));
                    $mail->IsHTML(true);
                    @$mail->Send();
                    $text_block = TextBlocks()->getOrCreate(array('name' => 'Товар под заказ: форма отправлена', 'rich' => '1'));
                    $msg = (string) $text_block->text;
                } else {
                    $msg = "Вы уже подписаны на поступление этого товара";
                    $status->ok = false;
                }
            }
        } else {
            $msg = "Пожалуйста, корректно заполните отмеченные поля";
        }

        $json = json_encode(array($status, $msg));
        return $json;
    }

    function countentry() {
        $validator_rules = array(
            'countentry-count' => array('trim', 'required', array('length', 1, 255)),
            'countentry-entry' => array('trim', 'required', array('length', 1, 10)),
            'countentry-name' => array('trim', 'required', array('length', 1, 255)),
            'countentry-email' => array('trim', 'required', 'email', array('length', 1, 255)),
            'countentry-city' => array('trim', 'required', array('length', 1, 100)),
        );

        $validator_messages = array();

        $validator = new DataValidator($validator_rules, $validator_messages);
        $status = $validator->process(Meta::vars());

        if ($status->ok) {

            $entry = CatalogEntries()->get(array("oneC_id" => $status->data['countentry-entry']));

            $data = array(
                "Название товара" => $entry->title,
                "Ссылка" => 'http://' . $_SERVER['HTTP_HOST'] . '/' . $entry->full_uri,
                "Имя" => $status->data['countentry-name'],
                "Город" => ContactCity::GetNameByPrefix($status->data['countentry-city']),
                "Email" => $status->data['countentry-email'],
                'Количество' => $status->data['countentry-count']
            );

            $mail = PhpMailerLibrary::create();
            foreach (explode(",", Config::get("order.email_" . ContactCity::GetCurrent()->prefix)) as $email) {
                $mail->AddAddress(trim($email));
            }
            $mail->Subject = "Заказ на большее количество товара с {$_SERVER['HTTP_HOST']}";
            $mail->Body = new View('_mails/new_countentry', compact("data"));
            $mail->IsHTML(true);
            @$mail->Send();

            $text_block = TextBlocks()->getOrCreate(array('name' => 'Связь с менеджером: форма отправлена', 'rich' => '1'));
            $msg = (string) $text_block->text;
        } else {
            $msg = "Пожалуйста, корректно заполните отмеченные поля";
        }

        $json = json_encode(array($status, $msg));
        return $json;
    }

    /**
     * аплоадер файлов
     */
    function fileupload() {
        $response = array(
            "success" => false,
            "msg" => "",
            "data" => null,
        );

        try {
            foreach (Meta::vars("files") as $file) {
                $file = FeedbackTempFiles()
                        ->create(array(
                    "file" => $file
                ));

                $response["data"] = $file->idetify;
                $response["success"] = true;
                break;
            }
        } catch (Exception $exc) {
            $response['msg'] = $exc->getMessage();
        }

        return json_encode($response);
    }

    /**
     * Добавление данных в лог
     * @param array $data данные в виде массива, название поля => значение
     * @param string $type тип формы, обратная связь, обратный звонок и пр.
     */
    static function add_to_log($data, $type) {
        $data_string = "";
        foreach ($data as $key => $value) {
            $data_string .= $key . ": " . ($value ? $value : "не указано") . "<br>";
        }

        $record = FormsHandlerLogItems()
                ->create(array(
            "user_info" => json_encode($_SERVER),
            "type" => $type,
            "text" => $data_string,
        ));

        return $record;
    }

}
