<?

class RedirectToFirstChildApplication extends AbstractApplication {

    function run(Page $page, $uri = '') {
        $first_child = Pages()
            ->filterChildren($page)
            ->filter(array("enabled" => true))
            ->filterLevel($page->lvl + 1)
            ->first();
        
        header("Location: $first_child->uri", true, 302);
        return true;
    }

}
