<?php
//вывод ошибок
ini_set('display_errors', 0);
error_reporting(E_ALL);

if ($_SERVER['REQUEST_URI'] == '/robots.txt') {
    $robots_txt = dirname(__FILE__) . "/robots-{$_SERVER['HTTP_HOST']}.txt";
    if (file_exists($robots_txt)) {
        header('Content-type: text/plain');
        echo file_get_contents($robots_txt);
        exit();
    } else {
        header("Location: /404/");
        exit();
    }
}

if ($_SERVER['REQUEST_URI'] == "/sitemap.xml") {
    $sitemap_xml = dirname(__FILE__) . "/sitemap-{$_SERVER['HTTP_HOST']}.xml";    
    if (file_exists($sitemap_xml)) {
        header('Content-type: text/xml');
        echo file_get_contents($sitemap_xml);
        exit();
    } else {
        header("Location: /404/");
        exit();
    }
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/loader.php");

//Builder::detectUserLang();
Builder::run();
