<?php

$controlfile = $_SERVER['DOCUMENT_ROOT'] . "/robots-{$_SERVER['HTTP_HOST']}.txt";
if (file_exists($controlfile)) {
    header('Content-type: text/plain');
    echo file_get_contents($controlfile);
} else {
    header("Location: /404/"); exit();
}


