<?

error_reporting(E_ALL);
require_once( "{$_SERVER['DOCUMENT_ROOT']}/includes/loader.php" );

$location = "/";

if ($banner = Banners(array('pk' => Meta::vars('id')))->first()) {
    $banner->clicks = $banner->clicks + 1;
    $banner->save();
    $location = $banner->uri;
}

header("Location: $location", true);
