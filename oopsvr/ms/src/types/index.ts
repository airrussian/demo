export type TMethod = string;
export type TUrl = string;

export type TPHPSessID = string;

export type TUserSettings = {
    whowrite: string,
    fontsize: string,
    volume: number,
    mute:boolean
}

export type TUserService = {
    id: number;
    title: string;
    exec: string;
    rate: string;
    price: number;
}

export type TError = string;

export type TResponse = {
    success: boolean,
    session?: TSiteSession,
    chat?: TChat,
    broadcast?:TBroadcast,
    message?: TMessage,
    stream?: TStream,
    error?: TError
    messages?: Array<any>,
    user?:TUser
}

export type THeader = {
    usermenu: TMenu
}

export type TFooter = {
    menu: TMenu,
    copyright:string,
    text:string,
    record:string
}

export type TDocument = {
    header: THeader,
    footer: TFooter,
    langs:TMenu
}

export type TSiteSession = {
    connectID:number,
    phpsessid?: TPHPSessID,
    auth: boolean,
    user: TUser,
}

export type TMenuItem = {
    id: number,
    title: string,
    full_uri: string
}

export type TMenu = Array<TMenuItem>;

export type TBroadcast = {
    id: number,
    title:string,
    link:string,
    owner: number,
    active:boolean,
    streams:Array<TStream>;
}

export type TChat = {
    id: number,
    type: TChatType,
    owner: number,
    members?:TChatMembers,
    messages?:TMessages
}

export type TChatMembers = Array<TChatMember>;

export type TChatMember = {
    id: number,
    login: string
}

export type TAuthor = {
    nickname: string,
    user: number
}

export type TMessage = {
    id: number,
    text: string,
    donation: number,
    dateCreate:string,
    type: string,
    author: TAuthor,
}

export type TChatType = {
    id: number,
    title: string
}

export type TMsg = {
    id: string,
    [key: string]: any
}

export type TQRCode = any;

export type TStream = {
    id:number;
    source:TStreamSource,
    quality?:TStreamQuality,
    bitrate?:number,
    rtmp?:string,
    key?:string,
    qrcode?:TQRCode,
    active:boolean,
    projection: TStreamProjection
}

export type TStreamSource = {
    id:number;
    title: string
}

export type TStreamQuality = {
    id:number;
    title: string
}

export type TStreamProjection = {
    id:number;
    title: string;
    name: string;
    descr: string;
}

export type TUserRole = {
    id: number,
    title: string,
    name: string
}

export type TUser = {
    id: number,
    login:string,
    name?:string,
    uri:string,
    role?:TUserRole,
    email?:string,
    last_login?:string,
    balance?:number,
    settings?:TUserSettings,
    services?:Array<TUserService>,
    broadcast?:TBroadcast,
    chats?: Array<TChat>
    status?: TUserStatus;
}

export type TUserStatus = {
    id: number,
    title: string,
    price: number;
    rate: string;
}

export type TChatUser = {
    chat: TChat,
    unread: number
}

export type TMessages = {
    messages: Array<TMessage>,
    unread: number,
    total: number
}

export type TMessagesFilter = {
    chat_id: number,
    count?: number,
    message?: {
        lastId: number,
        type: string
    }
}