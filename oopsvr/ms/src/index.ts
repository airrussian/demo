import { Store } from './classes/Store';

import * as express from 'express';
import * as http from 'http';
import * as WebSocket from 'ws';
import { config } from './config';

import { Session } from './classes/Session';
import * as oops from "./types";

const app = express();

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const server = http.createServer(app);
const wss = new WebSocket.Server({port: config.mr.socket.port});

(async () => {
    try {
        const store = Store.getInstance();
        const res = await store.init();

        if ( res ) {
            wss.on('connection', async (ws: WebSocket, req: any) => await Session.connect(ws, Session.token(req)));
            app.use('/wsi/*', express.static('public'));
            app.listen( config.mr.http.port );
            console.log("Manager sessions, port listen", config.mr.http.port);
        } else {
            throw Error("site not found");
        }
    } catch (ex) {
        console.log( ex.message );
        process.exit();
    }
})();

