import {Store} from "./Store";

import {Message} from "./Message";
import {Session} from "./Session";

import * as oops from '../types';

export class Messages {

    public messages:Array<Message>;
    public unread:number;
    public total:number;

    constructor( messages ) {
        this.unread = messages.unread || 0;
        this.total  = messages.total || 0;
        this.messages = [];
    }

    public static async factory( response: oops.TResponse ):Promise<Messages> {
        const messages:Messages = new Messages( response );

        await Promise.all(response.messages.map( async _message => {
            const message: Message = await Message.factory(_message);
            messages.messages.push(message)
        }));

        return messages;
    }

    public static async get(session: Session, filter:oops.TMessagesFilter ):Promise<Messages> {
        const store = Store.getInstance();
        return await store.messages.get(filter, session);
    }

    public async toJSON( session: Session, full:Boolean = true ):Promise<oops.TMessages> {
        const _messages = [];

        const p_messages = async (message:Message) => {
            _messages.push( await message.toJSON(session));
        }
        await Promise.all( this.messages.map( p_messages ) );

        return {
            messages: _messages,
            unread: this.unread,
            total: this.total
        }
    }

}