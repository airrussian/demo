import * as oops from "../types";
import {Session} from "./Session";

export class StreamQuality {

    public id:number;

    public title:string;

    public constructor( streamQuality: oops.TStreamQuality ) {
        this.id = streamQuality.id;
        this.title = streamQuality.title;
    }

    public static async factory( _streamQuality: oops.TStreamQuality ):Promise<StreamQuality> {
        const streamQuality:StreamQuality = new StreamQuality( _streamQuality );
        return streamQuality;
    }

    public toJSON(session:Session, full:boolean = true):oops.TStreamQuality {
        return {
            id: this.id,
            title: this.title
        }
    }

}