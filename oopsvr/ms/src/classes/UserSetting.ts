import * as oops from "../types";
import {Session} from "./Session";

export class UserSetting {

    public whowrite: string;

    public fontsize: string;

    public volume: number;

    public mute:boolean;

    public constructor( settings: oops.TUserSettings ) {
        this.whowrite = settings && settings.whowrite ? settings.whowrite : null;
        this.fontsize = settings && settings.fontsize ? settings.fontsize : null;
        this.volume = settings && settings.volume ? settings.volume : null;
        this.mute = settings && settings.mute ? settings.mute : null;
    }

    public static async factory( _settings: oops.TUserSettings ):Promise<UserSetting> {
        const settings:UserSetting = new UserSetting( _settings );
        return settings;
    }

    public toJSON( session: Session, full:boolean = true ):oops.TUserSettings {
        return {
            whowrite: this.whowrite,
            fontsize: this.fontsize,
            volume: this.volume,
            mute: this.mute
        }
    }

}
