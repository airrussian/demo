import * as oops from "../types";
import {Session} from "./Session";
import {Stream} from "./Stream";
import {User} from "./User";

import {Store} from './Store';
import {BroadcastStatus} from "./BroadcastStatus";

export class Broadcast {

    public id:number;

    public title:string;
    public link:string;
    public active:boolean;
    public type:string;

    public owner:number;

    public status:BroadcastStatus;

    public streams:Map<number, Stream>;

    public sessions:Map<number, Session>;

    public duration: number;

    private constructor( broadcast: oops.TBroadcast ) {

        this.id = broadcast.id;

        this.title = broadcast.title;
        this.link = broadcast.link ? broadcast.link : this.link;
        this.active = broadcast.active ? broadcast.active : this.active;

        this.owner = broadcast.owner;

        this.duration = 0;

        this.streams = new Map();
        this.sessions = new Map();
    }

    public static async factory( _broadcast:oops.TBroadcast ):Promise<Broadcast> {
        try {
            const broadcast = new Broadcast( _broadcast );
            const store = Store.getInstance();

            await broadcast.addstreams( _broadcast.streams );

            return broadcast;
        } catch {
            return null;
        }
    }

    private async addstreams( streams: Array<oops.TStream> ):Promise<Broadcast> {
        const store = Store.getInstance();
        this.streams = new Map();
        const p_streams = async (_stream:oops.TStream) => {
            const stream:Stream = await store.streams.get(_stream);
            this.streams.set( stream.id, stream );
        }
        await Promise.all( streams.map( p_streams ) );
        return this;
    }

    public update( broadcast: oops.TBroadcast ):Broadcast {

        this.title = broadcast.title || this.title;
        this.link =  broadcast.link ? broadcast.link : this.link;
        this.active = broadcast.active ? broadcast.active : this.active;

        if ( broadcast.streams )
            this.addstreams( broadcast.streams );

        return this;
    }

    private static async get( session:Session, msg:oops.TMsg ):Promise<Broadcast> {
        const store = Store.getInstance();
        const broadcast:Broadcast = await store.broadcasts.get(msg.broadcast, session);
        return broadcast;
    }

    public static async join( session:Session, msg:oops.TMsg):Promise<Broadcast> {
        const store = Store.getInstance();
        const broadcast:Broadcast = await store.broadcasts.get(msg.broadcast, session);
        return await broadcast.join( session );
    }

    public async join( session: Session ):Promise<Broadcast> {
        const store = Store.getInstance();
        if (!await store.broadcasts.join( this, session ))
            throw Error("not joined to broadcast");
        session.broadcastConnected = this;
        return this;
    }

    public static async leave( session: Session, msg: oops.TMsg ):Promise<Broadcast> {
        const store = Store.getInstance();
        const broadcast:Broadcast = await store.broadcasts.get( msg.broadcast, session );
        return await broadcast.leave( session );
    }

    public async leave( session: Session ):Promise<Broadcast> {
        const store = Store.getInstance();
        if (!await store.broadcasts.leave( this, session ))
            throw Error("not leave from broadcast id:" + this.id);
        session.broadcastConnected = null;
        return this;
    }

    public toJSON( session: Session, full:boolean = true ):oops.TBroadcast {
        return ( session.auth ) ?
            (( this.owner == session.user.id ) ?
                    this.toJSON_owner(session, full) :
                    this.toJSON_owner(session, false)
            ) : this.toJSON_owner(session, false);
    }

    private toJSON_owner(session:Session, full:boolean = true):oops.TBroadcast {
        return {
            id: this.id,
            title: this.title,
            link: this.link,
            active: this.active,
            owner: this.owner,
            streams: this.getStream(session, full)
        }
    }


    public async changeStatus( status: BroadcastStatus ) {
        this.status = status;
    }

    private getStream( session: Session, full:boolean = true ):Array<oops.TStream> {
        const _ret = [];
        if ( this.streams.size ) {
            this.streams.forEach( stream => _ret.push( stream.toJSON( session, full )));
        }
        return _ret;
    }

    // Выполняется каждую секунду
    public async tick() {

        this.duration++;

        const store = Store.getInstance();
        const broadcastUsers = store.transaction.has( this.id ) ? store.transaction.get( this.id ) : new Map();

        const owner:User = await store.users.get(<oops.TUser> {id: this.owner});

        // Здесь и будем выполнять логику по транзакциям за приваты и групповые чаты от времени.
        if ( owner.status ) {

            let price = 0;
            if ( owner.status.rate == "minute" ) {
                price = owner.status.price / 60;
            }

            const users_price = new Map();
            this.sessions.forEach( _session => {
                users_price.set(_session.user.id, { session: _session, price: price})
            });

            users_price.forEach( user_price => {
                const session_price = broadcastUsers.has( user_price.user_id ) ? broadcastUsers.get( user_price.user_id ) : {session: user_price.session, price: 0};
                broadcastUsers.set( user_price.user_id, {session: user_price.session, price: price + user_price.price});
            });

            // broadcastUsers.forEach( async user_price => {
            //     const response:oops.TResponse = await user_price.session.site.user.transaction({
            //
            //     });
            //     if ( response.success ) {
            //
            //     } else {
            //
            //     }
            //
            // });
        }

        store.transaction.set(this.id, broadcastUsers);
    }


}