/**
 * @author Alexey Voronenko <airrussian@gmail.com>
 *
 * @description
 * Класс описывает пользователей в приложении.
 * Каждый пользователей должен иметь уникальный id идентификатор
 *
 * У каждого пользователя может быть broadcast(трансляция)
 * Список его приватных чатов(chats)
 * Публичный чат(publicChat)
 * Групповой чат(groupChat)
 *
 * publicChat и groupChat в логике приложения, может быть только у пользователей со role = {id: 2} (моделей)
 *
 */
import { Store } from './Store';

import {TMsg, TUser, TChat, TUserService} from "../types";

import {Session} from "./Session";
import {UserSetting} from "./UserSetting";
import {UserService} from "./UserService";
import {Broadcast} from "./Broadcast";
import {Chat} from "./Chat";
import {UserRole} from "./UserRole";
import {UserStatus} from "./UserStatus";

export class User {

    /**
     * Идентификатор пользователя
     */
    public id:number;

    /**
     * Роль пользователя
     */
    public role:UserRole;

    /**
     * Логин пользователя
     */
    public login:string;

    /**
     * Ссылка на профиль пользователя
     */
    public uri:string;

    /**
     * Текущий баланс пользователя
     */
    public balance:number;

    /**
     * Настройки пользователя
     */
    public settings:UserSetting;

    /**
     * Сервисы пользователя
     */
    public services:Array<UserService>;

    /**
     * Чаты пользователя
    */
    public chats:Map<number, Chat>;

    /**
     * Публичный чат, присутствует только у моделей
     */
    public publicChat:Chat;

    /**
     * Групповой чат, присутствует только у моделей
     */
    public groupChat:Chat;

    // Трансляция пользователя
    public broadcast:Broadcast;

    public status: UserStatus;

    // Сессии подключенный к текущему пользователю
    public sessionsConnected:Map<number, Session>;

    // Сессии пользователя
    public sessions:Map<number, Session>;

    private constructor( user: TUser ) {
        this.id = user.id;

        this.login = user.login;
        this.uri = user.uri;
        this.balance = user.balance;

        this.publicChat = null;
        this.groupChat = null;

        this.services = [];

        this.sessions = new Map();
        this.sessionsConnected = new Map();
        this.chats = new Map();

        this.status = null;
    }

    public static async factory( _user: TUser ):Promise<User> {

        const store = Store.getInstance();

        const user:User = new User( _user );

        if ( _user.broadcast )
            user.broadcast = await store.broadcasts.get( _user.broadcast );

        user.role = await UserRole.factory( _user.role );
        user.settings = await UserSetting.factory( _user.settings );
        user.status = await UserStatus.factory( _user.status );

        await user.addServices( _user.services );
        await user.addChats( _user.chats );

        return user;
    }

    private async addServices( services: Array<TUserService> ):Promise<User> {
        this.services = [];
        const p_services = async (_service:TUserService) => {
            const services:UserService = await UserService.factory( _service );
            this.services.push( services );
        }
        await Promise.all( services.map( p_services ) );
        return this;
    }

    public async addChats( chats: Array<TChat> ):Promise<User> {
        const store = Store.getInstance();

        this.chats = new Map();
        const p_chats = async (_chat:TChat) => {
            const chat:Chat = await store.chats.get( _chat );
            if ( chat.type.id == 1 ) this.publicChat = chat;
            if ( chat.type.id == 2 ) this.chats.set(chat.id, chat);
            if ( chat.type.id == 3 ) this.groupChat = chat;
        }
        await Promise.all( chats.map( p_chats ) );
        return this;
    }

    public async update( user: TUser ):Promise<User> {

        user.login && (this.login = user.login);
        user.uri && (this.uri = user.uri);
        user.balance && (this.balance = user.balance);

        return this;
    }

    /**
     * Данный метод возвращает пользователя
     * Может быть вызван по websocket {id:user.get} c объектом msg, который должен содержать свойство user.
     * @param session
     * @param msg
     */
    public static async get( session: Session, msg: TMsg ):Promise<User> {
        const store = Store.getInstance();
        const user:User = await store.users.get( msg.user );
        return user;
    }

    /**
     * Данный метод возврает пользователя к которому произошло подключение session.
     * Информация о пользователе к которому должно произойти подключение session, должна содержаться в объекте msg
     *
     * Может быть вызван по websocket {id:user.join} с объектом msg, который должен содержать свойство user.
     *
     * @param session
     * @param msg
     *
     * @return Promise<User>
     */
    public static async join( session: Session, msg: TMsg ):Promise<User> {
        const store = Store.getInstance();
        const user = await store.users.get( msg.user );
        return await user.join( session );
    }

    /**
     * Подключает session к
     * @param session
     */
    public async join( session: Session ):Promise<User> {

        // Проверяем возможно текущая сессия уже присоединена к юзера.
        if ( this.sessionsConnected.has(session.connectID) )
            throw Error('You connected in this user');

        // Если сессия имеет подключение, и подключение к другому юзеру,
        // то для начала отключаем эту сессию от того пользователя
        if (session.userConnected && (session.userConnected.id !== this.id ))
            await session.userConnected.leave( session ); // метод этот здесь, фактически user.leave

        const store = Store.getInstance();
        const user:User = await store.users.join( this, session );

        return await session.userConnect( user );
    }

    public static async leave( session:Session, msg:TMsg):Promise<User> {
        const store = Store.getInstance();
        const user = await store.users.get( msg.user );
        return await user.leave( session );
    }

    // Отсоединение session от текущего пользователя
    public async leave( session: Session ):Promise<User> {
        const store = Store.getInstance();
        const user:User = await store.users.leave( this, session );

        return await session.userDisconnect( user );
    }

    public static async kick( session:Session, msg:TMsg ) {
        // Получаем объект пользователя которого хотим кикнуть
        const store = Store.getInstance();
        const user = await store.users.get( msg.user );
        return user.kick( session );
    }

    public async kick( session: Session ) {
        // Функция вычисления пересечения двух массивов
        const intersection = (arrA, arrB) => arrA.filter(x => arrB.includes(x));

        if ( !session.auth )
            throw Error("You not auth");

        // Выбираем сессии пользователя, которые подключеный к пользователю текущей сессии
        const session_ids:Array<number> = intersection(
            Array.from(this.sessions.keys()),
            Array.from(session.user.sessionsConnected.keys())
        );

        // Отключаем все эти сессии от пользователя сессии.
        session_ids.map( session_id => {
            if (session.user.sessionsConnected.has(session_id))
                session.user.leave( session.user.sessionsConnected.get(session_id) );
        });

        return session.send({id: 'user.kick', user: this.toJSON( session )});
    }

    /**
     * Метод блокировки пользователя указанного в msg
     * @param session
     * @param msg
     */
    public static async block( session: Session, msg:TMsg ):Promise<User> {
        const store = Store.getInstance();
        const user:User = await store.users.get( msg.user );
        await store.users.block( user, session );

        return user;
    }

    /**
     * Метод блокировки пользователя указанного в msg
     * @param session
     * @param msg
     */
    public static async unblock( session: Session, msg:TMsg ):Promise<User> {
        const store = Store.getInstance();
        const user:User = await store.users.get( msg.user );
        await store.users.unblock( user, session );

        return user;
    }

    public async toJSON( session: Session, full:boolean = true ):Promise<TUser> {
        let _ret:TUser;

        _ret = ( session.auth ) ?
            (( session.user.id == this.id ) ?
                await this.toJSON_owner(session, full) :
                await this.toJSON_auth(session, full)
            ) : await this.toJSON_other(session, full);

        if (full) {
            _ret['chats'] = await this.chats_toJSON( session );
            _ret['publicChat'] = this.publicChat ? await this.publicChat.toJSON( session ) : null;
            _ret['groupChat'] = this.groupChat ? await this.groupChat.toJSON( session ) : null;
        }

        if ( this.role )
            _ret['role'] = await this.role.toJSON( session );

        _ret['status'] = this.status ? await this.status.toJSON( session ) : null;


        if ( _ret['services'] ) {
            const services = [];
            this.services.map( service => services.push( service.toJSON( session )));
            _ret['services'] = services;
        }

        _ret['sessions'] = [];
        this.sessions.forEach( _session => _ret['sessions'].push( _session.connectID ));

        _ret['session_connected'] = [];
        this.sessionsConnected.forEach( _session => _ret['session_connected'].push( _session.connectID ));

        return _ret;
    }

    private async toJSON_owner( session: Session, full:boolean = true ):Promise<TUser> {

        let ret:TUser = await this.toJSON_other( session, full );
        ret['balance'] = this.balance;

        if (full) {
            ret = Object.assign(ret, {
                settings: this.settings ? await this.settings.toJSON(session) : null,
                services: this.services ? this.services : null,
                broadcast: this.broadcast ? await this.broadcast.toJSON( session ) : null,
            });
        }

        return ret;
    }

    private async toJSON_auth( session: Session, full:boolean = true ):Promise<TUser> {

        let ret:TUser = await this.toJSON_other( session, full );

        if (full) {
            ret = Object.assign(ret, {
                settings: this.settings ? await this.settings.toJSON(session) : null,
                services: this.services ? this.services : null,
                broadcast: this.broadcast ? await this.broadcast.toJSON( session ) : null,
            });
        }

        return ret;
    }

    private async toJSON_other( session: Session, full:boolean = true ):Promise<TUser> {

        let ret:TUser = {
            id: this.id,
            login: this.login,
            uri: this.uri,
        };

        if (full) {
            ret = Object.assign(ret, {
                settings: this.settings ? await this.settings.toJSON(session) : null,
                services: this.services ? this.services : null,
                broadcast: this.broadcast ? await this.broadcast.toJSON( session ) : null,
            });

        }


        return ret;
    }

    private async chats_toJSON( session: Session ):Promise<Array<TChat>> {
        const ret:Array<TChat> = [];

        for (const key of this.chats.keys()) {
            const chat:Chat = this.chats.get(key);
            if ( chat.type.id !== 2 )
                ret.push( await chat.toJSON( session, false ) );
            else if ( session.user && ( session.user.id == this.id ))
                ret.push( await chat.toJSON( session, false ) );
        }

        return ret;
    }

    public async sessionConnect( session: Session ):Promise<User> {
        this.sessionsConnected.set( session.connectID, session );
        return this;
    }

    public async sessionDisconnect( session: Session ):Promise<User> {
        this.sessionsConnected.delete( session.connectID );
        return this;
    }

    public async transferMoney( toUser:User, count:number, category:string):Promise<boolean> {
        // Выполняет транзакция по переводу
//...
        this.balance = this.balance + count;

        toUser.balance = toUser.balance - count;

        // Отправляем всем сессия текущего пользователя, что его баланс изменился
        this.sessions.forEach( session => session.balance( this.balance ) );

        // Отправляем всем сессия пользователя которому перевели его новый баланс
        toUser.sessions.forEach( session => session.balance( toUser.balance ) );

        return true;
    }

}