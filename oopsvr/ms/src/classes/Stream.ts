import { Store } from './Store';

import * as oops from "../types";
import {StreamQuality} from './StreamQuality';
import {StreamSource} from './StreamSource';
import {Session} from "./Session";

import {StreamProjection} from "./StreamProjection";

export class Stream {

    public id:number;

    public source:StreamSource;

    public bitrate:number;

    public quality:StreamQuality;

    public rtmp:string;
    public key:string;
    public qrcode:any;

    public active:boolean;

    public projection:StreamProjection;

    private constructor( stream: oops.TStream ) {
        this.id = stream.id;
        this.active = stream.active;
        this.bitrate = stream.bitrate;
        this.rtmp = stream.rtmp;
        this.key = stream.key;
        this.qrcode = stream.qrcode;
    }

    public static async factory( _stream: oops.TStream ):Promise<Stream> {
        const stream:Stream = new Stream( _stream );
        stream.source = await StreamSource.factory( _stream.source );
        stream.quality = await StreamQuality.factory( _stream.quality );
        stream.projection = await StreamProjection.factory( _stream.projection );

        return stream;
    }

    private toJSON_full(session:Session):oops.TStream {
        return {
            id: this.id,
            source: this.source ? this.source.toJSON(session) : null,
            quality: this.quality ? this.quality.toJSON(session) : null,
            bitrate: this.bitrate,
            rtmp: this.rtmp,
            key: this.key,
            qrcode: this.qrcode,
            projection: this.projection ?  this.projection.toJSON( session ) : null,
            active: this.active
        }
    }

    private toJSON_short(session:Session):oops.TStream {
        return {
            id: this.id,
            source: this.source ? this.source.toJSON(session) : null,
            projection: this.projection ?  this.projection.toJSON( session ) : null,
            active: this.active
        }
    }

    public toJSON( session: Session, full:boolean = true ):oops.TStream {
        return ( full ) ? this.toJSON_full(session) : this.toJSON_short(session);
    }

    public static async add( session: Session, _stream: oops.TStream ):Promise<Stream> {

        const store = Store.getInstance();
        const stream:Stream = await store.streams.add( _stream, session );

        const broadcast = session.user.broadcast;
        broadcast.streams.set( stream.id, stream );

        return stream;

        // await session.send({id: 'stream.add', stream: stream.toJSON( session )});
    }

    public static async del( session: Session, _stream: oops.TStream ):Promise<Stream> {

        const store = Store.getInstance();
        const stream:Stream = await store.streams.delete( _stream, session );

        const broadcast = session.user.broadcast;
        broadcast.streams.delete( stream.id );

        // await session.send({id: 'stream.del', stream: stream.toJSON( session )});
        return stream;
    }

    public static async update( session: Session, _stream: oops.TStream ):Promise<Stream> {
        const store = Store.getInstance();
        return await store.streams.update( _stream, session );
    }

}