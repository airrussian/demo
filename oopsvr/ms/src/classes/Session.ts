import { Store } from './Store';

import * as WebSocket from 'ws';

import {Chat} from './Chat';
import {User} from './User';
import {Message} from './Message';
import {Stream} from './Stream';
import {Broadcast} from "./Broadcast";

import { config } from '../config';

import * as oops from "../types";

import {UserSetting} from "./UserSetting";
import {UserService} from "./UserService";

const store = Store.getInstance();

const sid:number = 1000000000;
const fid:number = 9999999999;

let id:number = sid;

export class Session {

    /**
     * Номер соединения каждое новое соединение новое значение
     */
    public connectID:number;

    /**
     * phpsessid - сессия PHP hash
     */
    public phpsessid:oops.TPHPSessID = null;

    /**
     * WebSocket
     */
    public ws:WebSocket;

    /**
     * Авторизованная сессия
     */
    public auth:boolean = false;

    /**
     * Пользователь
     */
    public user:User;

    /**
     * Содержит объект User к которому подключена текущая сессия
     */
    public userConnected:User;

    public broadcastConnected:Broadcast;

    public chatsConnected:Map<number, Chat>;

    private constructor(ws: WebSocket, phpsessid:oops.TPHPSessID) {
        this.connectID = (id > fid) ? sid : ++id;
        this.ws = ws;
        this.phpsessid = phpsessid;

        this.userConnected = null;
        this.broadcastConnected = null;
        this.chatsConnected = new Map();
    }

    public static async factory(ws:WebSocket, phpsessid:oops.TPHPSessID):Promise<Session> {
        const session:Session = new Session(ws, phpsessid);

        session.ws.on('message',
            async ( message:string ) => await session.dispatch( <oops.TMsg> JSON.parse( message ) ));

        session.ws.on("error",
            async (err:string) => await session.sendError('Error', err));

        session.ws.on("close",
            async () => await session.leave());

        session.ws.on("pong", () => session.ping());

        session.ping();

        return await store.sessions.connect( session );
    }

    private ping() {
        setTimeout(() => this.ws.ping(), config.mr.socket.ping_pong);
    }

    private async dispatch( msg:oops.TMsg = null ): Promise<boolean> {

        const classes = {
            'session'   :Session,
            'user'      :User,
            'chat'      :Chat,
            'message'   :Message,
            'stream'    :Stream,
            'settings'  :UserSetting,
            'services'  :UserService
        }

        try {
            const path:Array<string> = (msg.id).toString().toLowerCase().split(".");

            const className = (path.length > 1) ? path[0] : 'session';
            const methodName = (path.length > 1) ? path[1] : path[0];

            if (classes[className] && (typeof classes[className][methodName] == "function")) {
                const res = await classes[className][methodName](this, msg);
                if ( typeof(res) == "boolean") return;
                msg[className] = await res.toJSON(this);
                return await this.send(msg);
            } else {
                return await this.sendError( 'Not command',  path.join("."));
            }
        } catch (ex) {
            await this.sendError( 'Exception', ex.message);
        }
        return true;
    }

    public static async connect(ws:WebSocket, phpsessid:oops.TPHPSessID):Promise<boolean> {
        const session:Session = await Session.factory(ws, phpsessid);
        return await session.send({id: 'session', session: await session.toJSON( session )});
    }

    public async leave():Promise<Session> {

        const session:Session = await store.sessions.delete( this );

        // Если текущая сессия является пользователем, то
        if ( this.auth && this.user ) {
            // Удаляем текущую сессию
            if ( this.user.sessions.has(this.connectID) )
                this.user.sessions.delete( this.connectID );
            // Если сессий пользователя не осталось, то
            if ( !this.user.sessions.size ) {
                // все подключенные сессии нужно отключить
                // !!! но не в нашем случае, все активные сессии остаются подключены к пользователю
                if ( false && this.user.sessionsConnected.size )
                    this.user.sessionsConnected.forEach( session => this.user.leave( session ) )
                // Выходим из всех чатов
            }
        }

        // Если текущая сессия имеет подключения к пользователю, то отключаем её от него
        if ( this.userConnected )
            await this.userConnected.leave( this );

        if ( this.chatsConnected.size )
            this.chatsConnected.forEach( chat => chat.leave( this ));

        if ( this.broadcastConnected )
            await this.broadcastConnected.leave( this );

        await this.send(<oops.TMsg> {id: 'session', session: null});

        return this;
    }

    public async send( msg: oops.TMsg, _all:boolean = true ): Promise<boolean> {

        const _msg = JSON.stringify(msg);

        if ( this.ws.readyState == 1 )
            if ( this.auth && this.user && _all) {
                if ( this.user.sessions.size )
                    this.user.sessions.forEach( session => session.ws.send(_msg) );
            } else {
                await this.ws.send(_msg)
            }

        // if ( store.admin.size )
        //     store.admin.forEach( adminUser => {
        //         if ( adminUser.sessions.size )
        //             adminUser.sessions.forEach( adminSession => adminSession.ws.send( _msg ));
        //     });

        return true;
    }

    public async sendError(title: string, error: string):Promise<boolean> {
        await this.send(<oops.TMsg>{id: 'error', 'message':{title: title, error: error}});
        return false;
    }

    public async update( _session: oops.TSiteSession ):Promise<Session> {

        if ( _session.user ) {
            this.auth = true;
            this.user = await store.users.get( _session.user );

            this.user.sessions.set( this.connectID, this );
        } else {
            this.auth = false;
            this.user = null;
        }

        return this;
    }


    /**
     * Возвращает информации о текущей сессии.
     * Набор полей зависит от session которая запрашивает.
     *
     * Данная информация как правила передается на Frontend APP
     *
     * @param session
     */
    public async toJSON( session: Session, full:boolean = true ):Promise<oops.TSiteSession> {
        return ( session.connectID == this.connectID ) ?
            await this.toJSON_owner( session, full ) :
            await this.toJSON_other( session, full );
    }

    private async toJSON_owner( session: Session, full:boolean = true ):Promise<oops.TSiteSession> {
        return {
            connectID: this.connectID,
            auth: this.auth,
            user: (this.auth && this.user) ? await this.user.toJSON(session, true) : null,
        }
    }

    private async toJSON_auth( session: Session, full:boolean = true ):Promise<oops.TSiteSession> {
        return await this.toJSON_other(session, full);
    }

    private async toJSON_other( session: Session, full:boolean = true ):Promise<oops.TSiteSession> {
        return {
            connectID: this.connectID,
            auth: this.auth,
            user: (this.auth && this.user) ? await this.user.toJSON(session, false) : null,
        }
    }


    /**
     * Данные методы необходимо будет закрыть от общего досутпа по WS
     */

    /**
     * Закрывает WS соединение для текущей сессии
     * @param session
     * @param msg
     */
    public static async close(session:Session, msg:oops.TMsg) {
        session.ws.close();
    }

    public async userConnect( user: User ):Promise<User> {
        this.userConnected = await user.sessionConnect( this );

        // При подключении сразу подключаем его к публичному чату
        if ( user.publicChat )
            await user.publicChat.join( this );

        // Если статус у пользователя не приват и не группвой то подключаемся к его трансляции
        if (( user.broadcast ) && ( user.status.id == 1 )) {
            await user.broadcast.join( this );
        }

        return this.userConnected;
    }

    public async userDisconnect( user: User ):Promise<User> {
        await user.sessionDisconnect( this );
        this.userConnected = null;

        if ( user.publicChat )
            await user.publicChat.leave( this );

        // Если статус у пользователя не приват и не группвой то подключаемся к его трансляции
        if (( user.broadcast ) && ( user.status.id == 1 ))
            await user.broadcast.leave( this );

        return user;
    }


    /**
     * Возращает состояние приложение, метод сделан для откладки
     * @param session
     * @param msg
     */
    // public static async getstate( session:Session, msg:oops.TMsg) {
    //     const res = {}
    //     res['sessions'] = [];
    //     res['users'] = [];
    //     res['chats'] = [];
    //     store.sessions.forEach( _session => res['sessions'].push(_session.toJSON(session)));
    //     store.users.forEach( _user => res['users'].push( _user.toJSON( session )));
    //     store.chats.forEach( _chat => res['chats'].push( _chat.toJSON( session )));
    //     await session.send({id:'getstate', state: res});
    // }


    public static token(req):oops.TPHPSessID {
        let regexp = /phpsessid=(\w+)/i;
        let result = regexp.exec(req.headers.cookie);
        return result ? result[1] : null;
    };

    public async balance( balance: number ):Promise<boolean> {
        return await this.send({id: 'balance', balance: balance});
    }


}

