import * as oops from "../types";
import {Session} from "./Session";

export class StreamSource {

    public readonly id:number;

    public title:string;

    public constructor( streamSource: oops.TStreamSource ) {
        this.id = streamSource.id;
        this.title = streamSource.title;
    }

    public static async factory( _streamSource: oops.TStreamSource ):Promise<StreamSource> {
        const streamSource:StreamSource = new StreamSource( _streamSource );
        return streamSource;
    }

    public toJSON(session:Session, full:boolean = true):oops.TStreamSource {
        return {
            id: this.id,
            title: this.title
        }
    }

}