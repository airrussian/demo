import * as oops from "../types";
import {Session} from "./Session";

export class StreamProjection {

    public readonly id:number;

    public title:string;

    public name:string;

    public descr:string;

    public constructor( streamProjection: oops.TStreamProjection ) {
        this.id         = streamProjection.id;
        this.title      = streamProjection.title;
        this.name       = streamProjection.name;
        this.descr      = streamProjection.descr;
    }

    public static async factory( _streamProjection: oops.TStreamProjection ):Promise<StreamProjection> {
        const streamProjection:StreamProjection = new StreamProjection( _streamProjection );
        return streamProjection;
    }

    public toJSON(session:Session, full:boolean = true):oops.TStreamProjection {
        return {
            id: this.id,
            title: this.title,
            name: this.name,
            descr: this.descr
        }
    }

}