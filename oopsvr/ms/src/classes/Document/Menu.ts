import * as oops from "../../types";
import { Session } from '../Session';
import { MenuItem } from './MenuItem';

export class Menu {

    private items:Array<MenuItem>;

    public constructor(menu: oops.TMenu = null) {
        this.items = [];
        this.update( menu );
    }

    public update( menu: oops.TMenu ):Menu {
        if ( menu ) {
            this.items = [];
            menu.map( item => this.items.push( new MenuItem( item ) ))
        }

        return this;
    }

    public info( session: Session, full:boolean = true ):oops.TMenu {
        return this.toJSON(session,full);
    }

    public toJSON( session: Session, full:boolean = true ):oops.TMenu {
        let _ret:oops.TMenu = [];
        this.items.forEach( item => {
            const _item:oops.TMenuItem = item.toJSON( session );
            _ret.push( _item );
        })
        return _ret;
    }

}