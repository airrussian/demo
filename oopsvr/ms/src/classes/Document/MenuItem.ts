import * as oops from "../../types";
import { Session } from '../Session';

export class MenuItem {

    public id: number;
    private title: string;
    private full_uri: string;

    public constructor( item: oops.TMenuItem ) {
        this.update( item );
    }

    public update( item: oops.TMenuItem ):MenuItem {
        this.id = item.id || null;
        this.title = item.title || null;
        this.full_uri = item.full_uri || null;
        return this;
    }

    public info( session: Session, full:boolean = true ):oops.TMenuItem {
        return this.toJSON(session,full);
    }

    public toJSON( session: Session, full:boolean = true ):oops.TMenuItem {
        let _ret;
        _ret = {
            id: this.id,
            title: this.title,
            full_uri: this.full_uri
        };
        return _ret;
    }

}