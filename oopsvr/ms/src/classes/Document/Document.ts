import * as oops from "../../types";
import { Session } from '../Session';
import { Header } from './Header';
import { Footer } from './Footer';
import { Menu } from './Menu';

export class Document {

    private header:Header;
    private footer:Footer;
    private langs:Menu;

    public constructor(document: oops.TDocument = null) {
        this.header = new Header();
        this.footer = new Footer();
        this.langs = new Menu();
        this.update( document );
    }

    public update( document: oops.TDocument ):Document {
        if ( document && document.header )
            this.header.update( document.header );

        if ( document && document.footer )
            this.footer.update( document.footer );

        if ( document && document.langs )
            this.langs = new Menu( document.langs );

        return this;
    }

    public info( session: Session, full:boolean = true ):oops.TDocument {
        return this.toJSON(session,full);
    }

    public toJSON( session: Session, full:boolean = true ):oops.TDocument {
        let _ret;
        _ret = {
            header: this.header.toJSON( session ),
            footer: this.footer.toJSON( session ),
            langs: this.langs.toJSON( session )
        };
        return _ret;
    }

}