import * as oops from "../../types";
import { Session } from '../Session';
import { Menu } from './Menu';

export class Footer {

    private menu:Menu;
    private copyright:string;
    private text:string;
    private record:string;


    public constructor( footer: oops.TFooter = null ) {
        this.menu = new Menu();
        this.update( footer );
    }

    public update( footer: oops.TFooter ):Footer {
        if ( footer && footer.menu )
            this.menu.update( footer.menu );

        if ( footer && footer.copyright)
            this.copyright = footer.copyright;

        if ( footer && footer.text )
            this.text = footer.text;

        if ( footer && footer.record )
            this.record = footer.record;

        return this;
    }

    public info( session: Session, full:boolean = true ):oops.TFooter {
        return this.toJSON(session,full);
    }

    public toJSON( session: Session, full:boolean = true ):oops.TFooter {
        let _ret;
        _ret = {
            menu: this.menu.toJSON( session ),
            copyright: this.copyright,
            text: this.text,
            record: this.record
        };
        return _ret;
    }

}