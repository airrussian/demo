import * as oops from "../../types";
import { Session } from '../Session';
import { Menu } from './Menu';

export class Header {

    private usermenu:Menu;

    public constructor( header: oops.THeader = null ) {
        this.usermenu = new Menu();
        this.update( header );
    }

    public update( header: oops.THeader ):Header {
        if ( header && header.usermenu )
            this.usermenu.update( header.usermenu );

        return this;
    }

    public info( session: Session, full:boolean = true ):oops.THeader {
        return this.toJSON(session,full);
    }

    public toJSON( session: Session, full:boolean = true ):oops.THeader {
        let _ret;
        _ret = {
            usermenu: this.usermenu.toJSON( session )
        };
        return _ret;
    }

}