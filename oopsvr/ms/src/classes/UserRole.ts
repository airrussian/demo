import * as oops from "../types";
import {Session} from "./Session";

export class UserRole {

    public readonly id: number;

    public title: string;

    public name: string;

    public constructor( role: oops.TUserRole ) {
        this.id = role.id;
        this.title = role.title;
        this.name = role.name;
    }

    public static async factory( _role: oops.TUserRole ):Promise<UserRole> {
        const role:UserRole = new UserRole( _role );
        return role;
    }

    public toJSON( session: Session, full:boolean = true ):oops.TUserRole {
        return {
            id: this.id,
            title: this.title,
            name: this.name
        }
    }

}