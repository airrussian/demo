import {Store} from "./Store";
import { Message } from './Message';
import { Messages } from './Messages';
import { Session } from './Session';
import { ChatType } from './ChatType';
import { User } from './User';

import * as oops from '../types';

export class Chat {

    public id: number;
    public type: ChatType;
    public owner: number;

    // Содержит все сессии подключенные к чату
    public sessions: Map<number, Session>;

    public messages: Messages;

    private constructor( chat: oops.TChat ) {
        this.id = chat.id || 1;
        this.owner = chat.owner;

        this.sessions = new Map();
    }

    public static async factory( _chat: oops.TChat ):Promise<Chat> {
        const chat:Chat = new Chat( _chat );
        chat.type = await ChatType.factory( _chat.type );
        return chat;
    }

    /**
     * Возвращает существующий чат для текущей сессии
     * @param session
     * @param chat_msg
     */
    public static async get( session: Session, msg: oops.TMsg):Promise<Chat> {
        const store = Store.getInstance();
        const chat:Chat = await store.chats.get( msg.chat );
        return chat;
    }

    /**
     * Создает новый чат с указанными парамерами в chat_msg от текущей сессии
     * @param session
     * @param chat_msg
     */
    public static async create( session: Session, msg: oops.TMsg ):Promise<Chat> {
        const store = Store.getInstance();
        const chat:Chat = await store.chats.create( msg.chat, session );
        return chat;
    }

    public async join( session: Session ):Promise<Chat> {
        const store = Store.getInstance();

        const chat:Chat = await store.chats.join(this, session);

        if ( chat ) {
            await this.sendAllSessions(async _session => {
                await _session.send({
                    id: 'chat.connect',
                    chat: await this.toJSON(_session, false),
                    session: await session.toJSON(_session, false)
                }, false);
            });

            // Добавляем к чату текущую сессию
            this.sessions.set( session.connectID, session );
            // Добавляем к сессии текущий чат
            session.chatsConnected.set( chat.id, chat );
        }

        return this;
    }

    public static async join( session: Session, msg: oops.TMsg ):Promise<Chat> {
        const store = Store.getInstance();
        const chat:Chat = await store.chats.get( msg.chat );
        return await chat.join( session );
    }

    /**
     * session покидает текущий chat.
     * Если сессия яыляется
     * @param session
     */
    public async leave( session: Session ):Promise<Chat> {

        if (!this.sessions.has( session.connectID ))
            throw Error("Session is not joined in chat");

        const store = Store.getInstance();
        if (!await store.chats.leave( this, session ))
            throw Error("???");

        this.sessions.delete( session.connectID );

        await this.sendAllSessions(async _session => {
            await _session.send({
                id: 'chat.disconnect',
                chat: await this.toJSON(_session),
                session: await session.toJSON(_session)
            }, false);
        });

        session.chatsConnected.delete( this.id );

        return this;
    }

    public static async leave( session: Session, msg: oops.TMsg ):Promise<Chat> {
        const store = Store.getInstance();
        let chat:Chat = await store.chats.get( msg.chat.id );
        return await chat.leave( session );
    }

    public static async kick( session: Session, msg: oops.TMsg) {

    }

    /**
     * Приглашения пользователя user в текущий чат от подключения session
     * @param user
     */
    public async invite( user:User ) {
        if ( user.sessions.size ) {
            user.sessions.forEach( session => this.join( session ) );
        }
    }

    public static async read( session: Session, msg: oops.TMsg):Promise<boolean> {
        // const response:oops.TResponse = await store.chats.site.chat.read(msg);
        // if ( !response.success )
        //     return await session.sendError('', '');

        // user.sessions.forEach( _session => _session.send({
        //     id: 'chat.read',
        //     chat: this.toJSON(session)
        // }));

        return true;
    }

    private async private( session: Session ):Promise<boolean> {
        await this.invite( session.userConnected );
        await this.join( session );
        return true;
    }

    //private static getOrCreate( _chat:oops.TChat ):Chat {
        // const chat:Chat = store.chats.has( _chat.id ) ? store.chats.get( _chat.id ) : new Chat( _chat );
        // chat.update( _chat );
        // store.chats.set( chat.id, chat );
        // return chat;
    //}


    private static async _private( session: Session, msg: oops.TMsg ):Promise<Chat> {
        // Не авторизованная сессия не может звать никого в приваты
        if ( !session.auth || !session.user )
            throw new Error('You need auth for private');

        // Если сессии не подключена не к какому пользователю, то не понятно кого она зовет
        if ( !session.userConnected )
            throw new Error('You not connected model');

        const store = Store.getInstance();
        const chat:Chat = await store.chats.private(session);

        if ( chat )
            await chat.private( session );

        return chat;
    }

    public static async privatemessage( session: Session, msg: oops.TMsg ):Promise<boolean> {
        const chat = await Chat._private( session, msg );
        return true;
    }

    /**
     * Метод может быть вызван только пользователем.
     * Метод создает чат для приватных сообщений
     * @param session
     * @param msg
     */
    public static async private( session: Session, msg: oops.TMsg ):Promise<boolean> {
        const chat = await Chat._private( session, msg );
        await Message.send( session, {id: 'message.add', message: {chat: {id: chat.id}, type: 'request', text: 'private'}});
        return true;
    }

    /**
     * Прототип!
     * Метод может быть вызван только моделью, которую пользователь позвал в приват.
     * В msg должна содержаться информация о чате в котором должен начать приватное шоу.
     * @param session
     * @param msg
     */
    public static async start( session: Session, msg: oops.TMsg):Promise<boolean> {

        const store = Store.getInstance();
        const chat:Chat = await store.chats.get( msg.chat );

        // const chat = this.getOrCreate( response.chat );
        // if ( !chat )
        //     throw new Error("Not fount chat #262ts");

        const model = session.user;
        // Выгоняем все сессии из трансляции модели,
        // которые не подключены к текущему чату
        model.sessionsConnected.forEach( _session => {
            if ( !chat.sessions.has( _session.connectID ) ) {
                model.broadcast.leave( _session );
            }
        });

        // Меняем статус у модели только после изгнания сессии с трансляции.
        //model.setStatus(  );

        return true;
    }

    /**
     * Прототип!
     * Метод может быть моделью, если она желает отказаться от запроса в приват в чате
     * В msg должна содердиться информаци о чате, в котором, необходимо отказать в запросе.
     * @param session
     * @param msg
     */
    public static async cancel( session: Session, msg: oops.TMsg):Promise<boolean> {
        return true;
    }

    /**
     * Метод, вызывает модель или пользователь.
     * При вызове метода, шоу в чате приостаналивается.
     * @param session
     * @param msg
     */
    public static async stop( session: Session, msg: oops.TMsg): Promise<boolean> {
        return true;
    }


    public static async group( session: Session, msg: oops.TMsg ):Promise<Chat> {

        if ( !session.auth )
           throw Error('You need auth for private');

        if ( !session.userConnected )
            throw Error('You not connected model');

        const store = Store.getInstance();
        const chat:Chat = await store.chats.group( session );
        return await chat.join( session );
    }

    private async toJSON_owner(session:Session, full:boolean = true):Promise<oops.TChat> {
        return {
            id: this.id,
            type: this.type ? await  this.type.toJSON( session, full ) : null,
            owner: this.owner
        }
    }

    private async toJSON_auth(session:Session, full:boolean = true):Promise<oops.TChat> {
        return {
            id: this.id,
            type: this.type ? this.type.toJSON(session, full) : null,
            owner: this.owner
        }
    }

    private async toJSON_other(session:Session, full:boolean = true):Promise<oops.TChat> {
        return {
            id: this.id,
            type: this.type ? this.type.toJSON(session, full) : null,
            owner: this.owner
        }
    }

    public async toJSON( session:Session, full:boolean = true ):Promise<oops.TChat> {
        let _ret:oops.TChat = ( session.auth ) ?
            ( session.user && ( session.user.id == this.owner) ?
                    await this.toJSON_owner(session, full) :
                    await this.toJSON_auth(session, full)
            ) : await this.toJSON_other(session, full);

        if (session.chatsConnected.has( this.id )) {
            const messages:Messages = await this.getMessages( session, {chat_id: this.id, count: 20}  );
            const _messages:oops.TMessages = await messages.toJSON( session );

            _ret = Object.assign(_ret, {
                members: await this.getMembers( session ),
                guest: await this.getGuest( session ),
                messages: _messages
            });
        }

        return _ret;
    }

    private async getMembers( session: Session ):Promise<oops.TChatMembers> {
        const users:Map<number, User> = new Map();
        this.sessions.forEach( session => {
            if ( session.auth && !users.has( session.user.id ))
                users.set( session.user.id, session.user );
        });
        const users_arr = [];
        for (let key of users.keys()) {
            const member:oops.TChatMember = await users.get(key).toJSON( session, false );
            users_arr.push( member );
        }
        return users_arr;
    }

    private async getGuest( session: Session ):Promise<number> {
        let guest = this.sessions.size;
        this.sessions.forEach( session => !session.auth || guest--);
        return guest;
    }

    /**
     * Получает от сайт сообщения чата
     * @param session
     * @param msg
     */
    public static async messages( session: Session, msg: oops.TMsg):Promise<Messages> {
        console.log("messages");
        const store = Store.getInstance();

        const chat:Chat = await store.chats.get( msg.chat );
        console.log("messages");
        return await chat.getMessages( session, msg.params );
    }

    public async getMessages( session: Session, filter:oops.TMessagesFilter):Promise<Messages> {
        const store = Store.getInstance();
        return await store.messages.get( filter, session );
    }

    public async addMessage( session: Session, _message: oops.TMessage ):Promise<boolean> {
        const store = Store.getInstance();
        const chat:oops.TChat = await this.toJSON(session, false);
        const message:Message = await store.messages.send(chat, _message, session );

        return await this.sendAllSessions(async _session => _session.send({
            id: 'message.add',
            message: await message.toJSON(session, false),
            chat: await this.toJSON(session, false)
        }));
    }

    public async editMessage( session: Session, _message: oops.TMessage ):Promise<boolean> {
        const store = Store.getInstance();
        const chat:oops.TChat = await this.toJSON(session, false);
        const message:Message = await store.messages.edit( chat, _message, session );

        return await this.sendAllSessions(async _session => _session.send({
            id: 'message.edit',
            message: await message.toJSON(session, false),
            chat: await this.toJSON(session, false)
        }));

    }

    public async delMessage( session: Session, _message: oops.TMessage ):Promise<boolean> {
        const store = Store.getInstance();
        const chat:oops.TChat = await this.toJSON(session, false);
        const message:Message = await store.messages.delete( chat, _message, session );

        return await this.sendAllSessions(async _session => _session.send({
            id: 'message.del',
            message: await message.toJSON(session, false),
            chat: await this.toJSON(session, false)
        }));
    }

    public async sendAllSessions( callback: Function ):Promise<boolean> {
        if ( this.sessions.size ) {
            for (let key of this.sessions.keys()) {
                await callback(this.sessions.get(key));
            }
        }
        return true;
    }


}