import {Store} from './index';
import {Session} from "../Session";
import {User} from "../User";

import * as oops from "../../types";

export class StoreUsers {

    private store: Store;

    private items: Map<number, User>;

    private items_login: Map<string, User>;

    constructor( store: Store ) {
        this.store = store;

        this.items = new Map();
        this.items_login = new Map();
    }

    public async get( _user: oops.TUser, session:Session = null):Promise<User> {
        let user:User = null;

        if ( !_user.id && !_user.login)
            throw Error("No user id or login");

        if (this.items.has( _user.id )) {
            user = this.items.get( _user.id );
            this.items_login.set( user.login, user);
            return user;
        }

        if (this.items_login.has( _user.login )) {
            user = this.items_login.get( _user.login );
            this.items.set( user.id, user);
            return user;
        }

        this.store.site.setToken(this.store.admin);
        const response:oops.TResponse = await this.store.site.user.get( { user: _user } );

        if ( !response.success )
            throw Error(response.error);

        user = await User.factory( response.user );

        this.items.set( user.id, user );
        this.items_login.set( user.login, user );
        return user;
    }

    public async block( user: User, session:Session ):Promise<boolean> {
        return true;
    }

    public async unblock( user: User, session: Session ):Promise<boolean> {
        return true;
    }

    public async join( user: User, session: Session):Promise<User> {

        const msg = { user: await user.toJSON( session ) }

        if (session) this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.user.join( msg );

        if ( !response.success )
            throw Error(response.error);

        return user;
    }

    public async leave( user: User, session: Session):Promise<User> {

        const msg = { user: await user.toJSON( session ) }

        const response:oops.TResponse = await this.store.site.user.leave( msg );

        if ( !response.success )
            throw Error(response.error);

        return await user.sessionDisconnect( session );
    }


}