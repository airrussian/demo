import {Store} from './index';
import {Session} from '../Session';
import {Chat} from '../Chat';

import * as oops from '../../types';

export class StoreChats {

    private store:Store;

    private items:Map<number, Chat>;

    constructor( store:Store ) {
        this.store = store;

        this.items = new Map();
    }

    public async get( _chat: oops.TChat ):Promise<Chat> {

        let chat:Chat = null;

        if ( !_chat.id )
            throw Error("No chat id");

        if ( !this.items.has( _chat.id ) ) {
            this.store.site.setToken(this.store.admin);
            const response:oops.TResponse = await this.store.site.chat.get( {chat: _chat} );
            if ( response.success ) {
                chat = await Chat.factory( response.chat );
                this.items.set( chat.id, chat );
            } else {
                chat = null;
            }
        } else {
            chat = this.items.get( _chat.id );
        }

        return chat;

    }

    public async create( _chat:oops.TChat, session:Session = null):Promise<Chat> {
        if (session) this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.chat.create( {chat: _chat} );
        if ( !response.success )
            throw Error( response.error );

        const chat:Chat = await Chat.factory( response.chat );
        this.items.set( chat.id, chat );
        return chat;
    }

    public async private( session: Session ):Promise<Chat> {
        if ( !session )
            throw Error("You not auth");

        if ( !session.user )
            throw Error("You not is user");

        if ( !session.userConnected )
            throw Error("You not connect");

        this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.chat.private({model: session.userConnected.toJSON( session )} );

        if ( !response.success )
            throw Error( response.error );

        if ( this.items.has( response.chat.id ) )
            return this.items.get( response.chat.id );

        const chat:Chat = await Chat.factory( response.chat );
        this.items.set( chat.id, chat );

        return chat;
    }

    public async group( session: Session ):Promise<Chat> {

        //// Проверить
        this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.chat.private({model: session.userConnected.toJSON( session )} );

        if ( !response.success )
            throw Error( response.error );

        const chat:Chat = await Chat.factory( response.chat );
        this.items.set( chat.id, chat );

        return chat;
    }

    public async join( chat:Chat, session: Session ):Promise<Chat> {

        if ( chat.sessions.has( session.connectID ))
            return chat;

        const _chat:oops.TChat = await chat.toJSON(session, false);

        this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.chat.join( {chat:_chat} );

        if ( !response.success )
            throw Error(response.error);

        return chat;
    }

    public async leave( chat:Chat, session:Session ):Promise<boolean> {

        const _chat:oops.TChat = await chat.toJSON(session, false);
        this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.chat.leave( {chat:_chat} );
        if ( !response.success )
            throw Error(response.error);

        return true;
    }

}