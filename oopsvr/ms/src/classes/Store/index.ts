import {Site} from "../Site";
import * as oops from "../../types";
import { config } from '../../config';

import {StoreChats} from "./storeChats";
import {StoreUsers} from "./storeUsers";
import {StoreBroadcasts} from "./storeBroadcasts";
import {StoreSessions} from "./storeSessions";
import {StoreStreams} from "./storeStreams";
import {StoreMessages} from "./StoreMessages";
import {TPHPSessID} from "../../types";

export class Store {

    private static instance:Store;


    public site:Site;

    public admin:TPHPSessID;

    /**
     * Хранит всё чаты всех комнат, всех пользователей
     */
    public chats:StoreChats;

    /**
     * Хранит все сессии активные в MS
     */
    public sessions:StoreSessions;

    public broadcasts:StoreBroadcasts;

    public streams:StoreStreams;

    public users:StoreUsers;

    public transaction:Map<number, Map<number, any>>;

    public messages:StoreMessages;

    public info: {
        memory: {
            rss: 0,
            heapTotal: 0,
            heapUsed: 0,
            external: 0
        }
    }

    private constructor() {
        this.site = new Site();

        this.chats = new StoreChats(this);
        this.sessions = new StoreSessions(this);
        this.broadcasts = new StoreBroadcasts(this);
        this.streams = new StoreStreams(this);
        this.users = new StoreUsers(this);
        this.transaction = new Map();
        this.messages = new StoreMessages(this);

        //setInterval( this.timer, 1000 );
    }

    public static getInstance(): Store {
        if ( !Store.instance )
            Store.instance = new Store();

        return Store.instance;
    }

    public async init():Promise<boolean> {
        const response:oops.TResponse = await this.site.login(config.ms.account.login, config.ms.account.pass);
        if ( !response.success )
            throw Error("I can't authorization on the site, please check account");

        this.admin = <oops.TPHPSessID> response.session.phpsessid;
        console.log("store init... ok", this.admin);

        return true;
    }

    private timer() {
        const broadcasts = this.broadcasts && this.broadcasts.getAll();
        if (broadcasts)
            broadcasts.forEach( async broadcast => await broadcast.tick() );
    }

}