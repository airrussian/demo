import {Store} from './index';
import {Session} from '../Session';
import {Broadcast} from '../Broadcast';

import * as oops from '../../types';

export class StoreBroadcasts {

    private store:Store;

    private items:Map<number, Broadcast>;

    constructor( store:Store ) {
        this.store = store;

        this.items = new Map();
    }

    public getAll():Map<number, Broadcast> {
        return this.items;
    }

    public async get( _broadcast: oops.TBroadcast, session:Session = null):Promise<Broadcast> {

        let broadcast:Broadcast = null;

        if ( !_broadcast.id )
            throw Error("No chat id");

        if ( this.items.has(_broadcast.id))
            return this.items.get( _broadcast.id );

        try {
            broadcast = await Broadcast.factory( _broadcast );
        } catch (ex) {
            if (session) this.store.site.setToken(session.phpsessid);
            const response:oops.TResponse = await this.store.site.broadcast.get( { broadcast: _broadcast });
            if ( !response.success )
                throw Error(response.error);

            broadcast = await Broadcast.factory( _broadcast );
        }

        this.items.set( broadcast.id, broadcast );

        return broadcast;
    }

    public async join( broadcast:Broadcast, session: Session):Promise<boolean> {

        broadcast.sessions.set( session.connectID, session );

        return true;

    }

    public async leave( broadcast:Broadcast, session: Session):Promise<boolean> {

        broadcast.sessions.delete( session.connectID );

        return true;
    }
}