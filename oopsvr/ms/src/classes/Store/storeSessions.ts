import {Store} from './index';
import {Session} from "../Session";
import {User} from "../User";

import * as oops from "../../types";

export class StoreSessions {

    private store: Store;

    private items: Map<number, Session>;

    constructor( store: Store ) {
        this.store = store;

        this.items = new Map();
    }

    public async connect( session: Session ):Promise<Session> {
        this.store.site.setToken( session.phpsessid );
        const response:oops.TResponse = await this.store.site.connect();
        this.items.set( session.connectID, session );
        session = await session.update( response.session );
        return session;
    }

    // public async get( _user: oops.TUser ):Promise<User> {
    //
    //     let user:User = null;
    //
    //     if ( !_user.id && !_user.login)
    //         throw Error("No user id or login");
    //
    //     if ( !this.items.has( _user.id ) ) {
    //         const response:oops.TResponse = await this.store.site.user.get( _user );
    //         user = await User.factory( response.user );
    //         this.items.set( user.id, user );
    //     } else {
    //         user = this.items.get( _user.id );
    //     }
    //
    //     return user;
    //
    // }

    public async delete( session: Session ):Promise<Session> {

        if ( this.items.has( session.connectID ) ) {
            this.items.delete( session.connectID );
        }

        return session;
    }

    public async disconnect( session: Session ):Promise<Session> {
        return session;
    }


}