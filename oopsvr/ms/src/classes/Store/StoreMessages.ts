import {Store} from './index';
import {Session} from "../Session";
import {Messages} from "../Messages";
import {Message} from "../Message";

import * as oops from "../../types";

export class StoreMessages {

    private store: Store;

    constructor( store: Store ) {
        this.store = store;
    }

    public async get( _filter: oops.TMessagesFilter, session:Session ):Promise<Messages> {
        this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.messages.get( _filter );

        if ( !response.success )
            throw Error( response.error );

        return await Messages.factory( response );

    }

    public async send( chat: oops.TChat, message: oops.TMessage, session:Session ):Promise<Message> {
        this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.messages.send({ chat, message });

        if( !response.success )
            throw Error( response.error );

        return await Message.factory( response.message );
    }

    public async edit(chat: oops.TChat, message: oops.TMessage, session:Session):Promise<Message> {
        this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.messages.edit({ chat, message });

        if( !response.success )
            throw Error( response.error );

        return await Message.factory( response.message );
    }

    public async delete( chat: oops.TChat, message: oops.TMessage, session:Session):Promise<Message> {
        this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.messages.delete({ chat, message });

        if( !response.success )
            throw Error( response.error );

        return await Message.factory( response.message );
    }


}