import {Store} from './index';
import {Stream} from "../Stream";
import { Session } from '../Session';

import * as oops from "../../types";

export class StoreStreams {

    private store:Store;

    private items: Map<number, Stream>;

    constructor(store: Store) {
        this.store = store;
        this.items = new Map();
    }

    public async get( _stream:oops.TStream ):Promise<Stream> {

        if ( this.items.has( _stream.id ))
            return this.items.get( _stream.id );

        const response:oops.TResponse = await this.store.site.stream.get( { stream: _stream });
        if ( !response.success )
            throw Error( response.error );

        const stream:Stream = await Stream.factory( response.stream );
        if ( stream ) {
            this.items.set( stream.id, stream );
            return stream;
        }
        return null;
    }

    public async add( _stream:oops.TStream, session: Session):Promise<Stream> {

        if (session) this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.stream.add( { stream: _stream } );

        if ( !response.success )
            throw Error( response.error );

        const stream:Stream = await Stream.factory( response.stream );
        this.items.set( stream.id, stream );
        return stream;
    }

    public async delete( _stream:oops.TStream, session: Session):Promise<Stream> {
        const stream:Stream = await this.get( _stream );

        if (session) this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.stream.del( { stream: _stream } );
        if ( !response.success )
            throw Error( response.error );

        this.items.delete( stream.id );
        return stream;
    }

    public async update( _stream:oops.TStream, session: Session ):Promise<Stream> {
        const stream:Stream = await this.get( _stream );

        if (session) this.store.site.setToken(session.phpsessid);
        const response:oops.TResponse = await this.store.site.stream.update( { stream: _stream } );
        if ( !response.success )
            throw Error( response.error );

        return stream;
    }

}