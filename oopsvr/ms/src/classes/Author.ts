import {Store} from "./Store";
import {Session} from "./Session";
import {User} from "./User";

import * as oops from '../types';

export class Author {

    public nickname:string;
    public user:number;

    private constructor( author: oops.TAuthor ) {
        this.nickname = author.nickname;
        this.user = author.user;
    }

    public static async factory( _author: oops.TAuthor ):Promise<Author> {
        const author = new Author(_author);
        return author;
    }

    public update( author: oops.TAuthor ):Author {
        this.nickname = author.nickname;

        //this.user = User.get(
        //this.user = author.user.id;

        return this;
    }

    public async toJSON( session:Session, full:boolean = true ):Promise<oops.TAuthor> {
        return {
            nickname: this.nickname,
            user: this.user
        }
    }

}
