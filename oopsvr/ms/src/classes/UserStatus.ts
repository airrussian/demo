import * as oops from "../types";
import {Session} from "./Session";

export class UserStatus {
    public id:number;
    public title:string;
    public price:number;
    public rate:string;

    constructor( status:oops.TUserStatus ) {
        this.id = (status && status.id) || 1;
        this.title = (status && status.title) || null;
        this.price = (status && status.price) || null;
        this.rate = (status && status.rate) || null;
    }

    public static async factory( _status:oops.TUserStatus ):Promise<UserStatus> {
        const status:UserStatus = new UserStatus( _status );
        return status;
    }

    public toJSON( session: Session, full:boolean = true ):oops.TUserStatus {
        return {
            id: this.id,
            title: this.title,
            price: this.price,
            rate: this.rate,
        }
    }
}
