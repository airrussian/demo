import {Store} from './Store';

const store = Store.getInstance();

export class BroadcastStatus {

    public id:number;

    public name:string;


    constructor( broadcastStatus: BroadcastStatus ) {
        this.id = broadcastStatus.id;
        this.name = broadcastStatus.name;
    }

}
