import {Store} from "./Store";

import {Session} from "./Session";
import {Chat} from "./Chat";

import {Author} from "./Author";

import * as oops from '../types';

export class Message {

    public id:number;

    public author:Author;

    public text:string;
    public donation:number;
    public dateCreate:string;
    public type:string;

    private constructor( message: oops.TMessage ) {
        this.id = message.id;
        this.text = message.text;
        this.type = message.type;
        this.dateCreate = message.dateCreate;
    }

    public static async factory( _message: oops.TMessage ):Promise<Message> {
        const message = new Message( _message );
        message.author = await Author.factory( _message.author );
        return message;
    }

    public async toJSON( session: Session, full:boolean = true):Promise<oops.TMessage> {
        return session.auth ? (
                ((this.author.user) && (session.user.id == this.author.user)) ?
                    await this.toJSON_owner(session, full) :
                    await this.toJSON_auth( session, full ))
            : await this.toJSON_other( session, full );
    }

    private async toJSON_owner( session: Session, full:boolean = true ):Promise<oops.TMessage> {
        return {
            id: this.id,
            text: this.text,
            donation: this.donation,
            type: this.type,
            author: this.author ? await this.author.toJSON(session, full) : null,
            dateCreate: this.dateCreate,
        }
    }

    private async toJSON_auth( session: Session, full:boolean = true):Promise<oops.TMessage> {
        return  {
            id: this.id,
            text: this.text,
            donation: this.donation,
            type: this.type,
            author: this.author ? await this.author.toJSON(session) : null,
            dateCreate: this.dateCreate,
        }
    }

    private async toJSON_other( session: Session, full:boolean = true):Promise<oops.TMessage> {
        return  {
            id: this.id,
            text: this.text,
            donation: this.donation,
            type: this.type,
            author: this.author ? await this.author.toJSON(session) : null,
            dateCreate: this.dateCreate,
        }
    }

    // public static async get( session: Session, msg: oops.TMsg ):Promise<Message> {
    //     return await store.messages.get( msg.message );
    // }

    public static async send( session: Session, msg: oops.TMsg ):Promise<boolean> {
        const store = Store.getInstance();
        const chat:Chat = await store.chats.get( msg.chat );
        return await chat.addMessage( session, msg.message );
    }

    public static async edit( session: Session, msg: oops.TMsg ):Promise<boolean> {
        const store = Store.getInstance();
        const chat:Chat = await store.chats.get( msg.chat );
        return await chat.editMessage( session, msg.message );
    }

    public static async delete( session: Session, msg: oops.TMsg):Promise<boolean> {
        const store = Store.getInstance();
        const chat:Chat = await store.chats.get( msg.chat );
        return await chat.delMessage( session, msg.message );
    }

}
