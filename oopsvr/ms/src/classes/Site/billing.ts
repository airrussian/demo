import { Site } from './index';
import {TResponse, TTrasferMoney} from "../../types";

export class SiteBilling {

    public site: Site;

    public constructor(site: Site) {
        this.site = site;
    }

    public async transferMoney( msg: TTrasferMoney ): Promise<TResponse> {
        return await this.site.send('/billing/transferMoney/', msg, 'GET');
    }
}
