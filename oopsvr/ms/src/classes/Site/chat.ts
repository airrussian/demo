import { Site } from './index';
import * as oops from "../../types";

export class SiteChat {

    public site: Site;

    public constructor(site: Site) {
        this.site = site;
    }

    public async create( msg ): Promise<oops.TResponse> {
        return await this.site.send('/chat/create', msg, 'POST');
    }

    public async get( msg ): Promise<oops.TResponse> {
        return await this.site.send(`/chat/get/${msg.chat.id}`);
    }

    public async join( msg ): Promise<oops.TResponse> {
        return await this.site.send(`/chat/join/${msg.chat.id}`);
    }

    public async leave( msg ): Promise<oops.TResponse> {
        return await this.site.send(`/chat/leave/${msg.chat.id}`, msg, 'POST');
    }

    public async close( msg ): Promise<oops.TResponse> {
        return await this.site.send('/chat/close', msg, 'POST');
    }

    public async invite( msg ):Promise<oops.TResponse> {
        return await this.site.send('/chat/invite', msg, 'POST');
    }

    public async read( msg ):Promise<oops.TResponse> {
        return await this.site.send('/chat/read', msg, 'PUT');
    }

    public async private( msg ): Promise<oops.TResponse> {
        return await this.site.send('/chat/private', msg, 'POST');
    }

    public async group( msg ): Promise<oops.TResponse> {
        return await this.site.send('/chat/group', msg, 'POST');
    }

    public async start( msg ): Promise<oops.TResponse> {
        return await this.site.send("/chat/start", msg, 'POST');
    }

    public async getmessages( msg ):Promise<oops.TResponse> {
        return await this.site.send("/chat/getmesages", msg, 'GET');
    }

}