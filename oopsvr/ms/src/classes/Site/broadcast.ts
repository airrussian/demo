import { Site } from './index';
import * as oops from "../../types";

export class SiteBroadcast {

    public site: Site;

    public constructor(site: Site) {
        this.site = site;
    }

    public async get( msg ): Promise<oops.TResponse> {
        return await this.site.send('/broadcast/get/' + msg.broadcast.id, msg, 'GET');
    }

    public async join( msg ): Promise<oops.TResponse> {
        return await this.site.send('/broadcast/join/' + msg.broadcast.id, msg, 'GET');
    }

    public async leave( msg ): Promise<oops.TResponse> {
        return await this.site.send('/broadcast/leave/' + msg.broadcast.id, msg, 'POST');
    }
}
