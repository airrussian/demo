import { Site } from './index';
import * as oops from "../../types";

export class SiteUser {

    public site: Site;

    public constructor(site: Site) {
        this.site = site;
    }

    public async get( msg ): Promise<oops.TResponse> {
        return await this.site.send('/live/get/' + msg.user.login, msg, 'GET');
    }

    public async join( msg ): Promise<oops.TResponse> {
        return await this.site.send('/live/join/' + msg.user.login, msg, 'GET');
    }

    public async leave( msg ): Promise<oops.TResponse> {
        return await this.site.send('/live/leave/' + msg.user.login, msg, 'POST');
    }

    public async transaction( msg ): Promise<oops.TResponse> {
        return await this.site.send('/billing/transfer', msg, 'POST');
    }

    public async block( msg ): Promise<oops.TResponse> {
        return await this.site.send('/u/block', msg, 'POST');
    }

    public async unblock( msg ): Promise<oops.TResponse> {
        return await this.site.send('/u/unblock', msg, 'POST');
    }
}
