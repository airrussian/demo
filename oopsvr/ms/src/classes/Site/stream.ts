import { Site } from './index';
import * as oops from "../../types";

export class SiteStream {

    public site: Site;

    public constructor(site: Site) {
        this.site = site;
    }

    public async add( msg ):Promise<oops.TResponse> {
        return await this.site.send(`/stream/add`, msg);
    }

    public async get( msg ):Promise<oops.TResponse> {
        return await this.site.send(`/stream/get`, msg);
    }

    public async del( msg ): Promise<oops.TResponse> {
        return await this.site.send(`/stream/delete`, msg, 'POST');
    }

    public async update( msg ): Promise<oops.TResponse> {
        return await this.site.send(`/stream/update`, msg, 'POST');
    }

}