import * as Request from 'request';
import { config } from '../../config';
import * as oops from "../../types";

import { SiteMessage } from "./message";
import { SiteChat } from './chat';
import { SiteStream } from "./stream";
import { SiteUser } from './user';
import { SiteBroadcast } from './broadcast';

export class Site {

    private phpsessid:string = null;

    public messages: SiteMessage;
    public chat: SiteChat;
    public stream: SiteStream;
    public user: SiteUser;
    public broadcast: SiteBroadcast;

    constructor() {
        this.messages = new SiteMessage(this);
        this.chat = new SiteChat(this);
        this.stream = new SiteStream(this);
        this.user = new SiteUser(this);
        this.broadcast = new SiteBroadcast(this);
    }

    private getHeaders():Request.Headers {
        return {
            'content-type': "application/json",
            'X-Requested-With': 'XMLHttpRequest',
            'OOPS': '1d7af611dfd8a878cc89e9cbccf5ecf7'
        }
    }

    private getCookie(host:string):Request.CookieJar {
        let jar:Request.CookieJar = Request.jar();

        jar.setCookie("builder_locale=en", host);
        if ( this.phpsessid ) {
            jar.setCookie(`PHPSESSID=${this.phpsessid}`, host);
        }

        return jar;
    }

    public async send( URL: oops.TUrl, params: any = {}, method: oops.TMethod = "GET"):Promise<oops.TResponse> {

        const host:string = `${config.siteAPI.schema}://${config.siteAPI.domain}:${config.siteAPI.port}`;
        const cookie = this.getCookie(host);

        console.log(`${host}${URL}`);

        const response:oops.TResponse = await new Promise( (resolve, reject) => {
            Request({
                url: `${host}${URL}`,
                method: method,
                json: params,
                headers: this.getHeaders(),
                jar: cookie,
                //qs: qs,
            }, (error, response, body) => {
                if (error) reject(<oops.TResponse> {success: false, error: error});
                resolve( <oops.TResponse> body );
            });
        });

        if ( !response.success ) console.log( response );

        return response;
    }

    public async login(login:string, password:string):Promise<oops.TResponse> {
        return await this.send('/u/mslogin', {login, password}, 'GET');
    }

    public async init():Promise<oops.TResponse> {
        return await this.send("/ms");
    }

    public async connect(): Promise<oops.TResponse> {
        return await this.send("/ms/connect");
    }

    public async disconnect(): Promise<oops.TResponse> {
        console.log("session.site.disconnect");
        return await this.send("/ms/disconnect");
    }

    public setToken( phpsessid:string ) {
        this.phpsessid = phpsessid;
    }

}