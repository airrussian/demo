import { Site } from './index';
import * as oops from "../../types";

export class SiteMessage {

    public site: Site;

    public constructor(site: Site) {
        this.site = site;
    }

    public async get( msg ): Promise<oops.TResponse> {
        return await this.site.send('/messages', msg, 'GET');
    }

    public async send( msg ): Promise<oops.TResponse> {
        return await this.site.send("/messages", msg, 'POST');
    }

    public async delete( msg ): Promise<oops.TResponse> {
        return await this.site.send('/messages', msg, 'DELETE');
    }

    public async edit( msg ): Promise<oops.TResponse> {
        return await this.site.send('/messages', msg, 'PUT');
    }
}
