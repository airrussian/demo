import * as oops from '../types';
import {Session} from "./Session";

export class ChatType {

    public id:number;
    public title: string;

    private constructor( chattype: oops.TChatType ) {
        this.id = chattype.id;
        this.title = chattype.title;
    }

    public static async factory( chattype: oops.TChatType ):Promise<ChatType> {
        const type:ChatType = new ChatType( chattype );
        return type;
    }

    public toJSON( session: Session, full:boolean = true ):oops.TChatType {
        return {
            id: this.id,
            title: this.title
        }
    }
}
