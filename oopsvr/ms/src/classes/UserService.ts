import * as oops from "../types";
import {Session} from "./Session";
import {User} from "./User";

export class UserService {

    public id: number;

    public title: string;

    public exec: string;

    public rate: string;

    public price: number;

    public constructor( service: oops.TUserService ) {
        this.id     = service.id
        this.title  = service.title;
        this.exec   = service.exec;
        this.rate   = service.rate;
        this.price  = service.price;
    }

    public static async factory( service: oops.TUserService ):Promise<UserService> {
        return new UserService( service );
    }

    public toJSON( session: Session, full:boolean = true ):oops.TUserService {
        return {
            id:     this.id,
            title:  this.title,
            exec:   this.exec,
            rate:   this.rate,
            price:  this.price
        }
    }

    public static async update( session: Session ): Promise<boolean> {
        return false;
    }

}
